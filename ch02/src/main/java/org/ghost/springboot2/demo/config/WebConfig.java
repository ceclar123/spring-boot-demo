package org.ghost.springboot2.demo.config;

import org.ghost.springboot2.demo.common.filter.RequestCrossFilter;
import org.ghost.springboot2.demo.common.filter.RequestUUIDFilter;
import org.ghost.springboot2.demo.common.filter.RequestWrapperFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    /**
     * 配置拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {

    }

    @Bean
    public FilterRegistrationBean getRequestUUIDFilter() {

        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new RequestUUIDFilter());
        registration.addUrlPatterns("/*");
        registration.setName("requestUUIDFilter");

        return registration;
    }

    @Bean
    public FilterRegistrationBean getRequestWrapperFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new RequestWrapperFilter());
        registration.addUrlPatterns("/*");
        registration.setName("requestWrapperFilter");

        return registration;
    }

    @Bean
    @Profile({"dev", "test"})
    public FilterRegistrationBean getRequestCrossFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new RequestCrossFilter());
        registration.addUrlPatterns("/*");
        registration.setName("requestCrossFilter");

        return registration;
    }

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.defaultContentType(MediaType.APPLICATION_JSON).favorPathExtension(true);
    }
}
