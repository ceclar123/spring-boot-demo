package org.ghost.springboot2.demo.component.redis;

import org.springframework.cache.interceptor.KeyGenerator;

import java.lang.reflect.Method;

public class GhostKeyGenerator implements KeyGenerator {
    public static final String REDIS_KEY_PREFIX = "TEST_";

    @Override
    public Object generate(Object target, Method method, Object... params) {
        return REDIS_KEY_PREFIX + method.getName();
    }
}
