package org.ghost.springboot2.demo.service;

import org.ghost.springboot2.demo.dto.RspDTO;

public interface ITestService {
    RspDTO getCacheableUserInfo(Long id);

    RspDTO getCachePutUserInfo(Long id);

    boolean delete(Long id);
}
