package org.ghost.springboot2.demo.component.redis;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Protocol;

import java.io.Serializable;

@Component
@ConfigurationProperties(prefix = "redis-config")
public class RedisProperties implements Serializable {
    private static final long serialVersionUID = 1071156375336358075L;

    private String host = Protocol.DEFAULT_HOST;
    private int port = Protocol.DEFAULT_PORT;
    private String password;
    private int timeout = Protocol.DEFAULT_TIMEOUT;
    private Long expireTime = 604800L;
    private int maxIdle = 8;
    private int maxTotal = 8;
    private long maxWaitMillis = 1000L;
    private Boolean testOnBorrow = true;
    /**
     * key是否需要通过序列化
     */
    private Boolean keySerializer = false;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

    public int getMaxIdle() {
        return maxIdle;
    }

    public void setMaxIdle(int maxIdle) {
        this.maxIdle = maxIdle;
    }

    public int getMaxTotal() {
        return maxTotal;
    }

    public void setMaxTotal(int maxTotal) {
        this.maxTotal = maxTotal;
    }

    public long getMaxWaitMillis() {
        return maxWaitMillis;
    }

    public void setMaxWaitMillis(long maxWaitMillis) {
        this.maxWaitMillis = maxWaitMillis;
    }

    public Boolean getTestOnBorrow() {
        return testOnBorrow;
    }

    public void setTestOnBorrow(Boolean testOnBorrow) {
        this.testOnBorrow = testOnBorrow;
    }

    public Boolean getKeySerializer() {
        return keySerializer;
    }

    public void setKeySerializer(Boolean keySerializer) {
        this.keySerializer = keySerializer;
    }

    @Override
    public String toString() {
        return "RedisProperties{" +
                "host='" + host + '\'' +
                ", port=" + port +
                ", password='" + password + '\'' +
                ", timeout=" + timeout +
                ", expireTime=" + expireTime +
                ", maxIdle=" + maxIdle +
                ", maxTotal=" + maxTotal +
                ", maxWaitMillis=" + maxWaitMillis +
                ", testOnBorrow=" + testOnBorrow +
                ", keySerializer=" + keySerializer +
                '}';
    }
}
