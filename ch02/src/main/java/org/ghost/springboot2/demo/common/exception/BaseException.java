package org.ghost.springboot2.demo.common.exception;

import org.ghost.springboot2.demo.common.constant.IErrorCodeEnum;
import org.ghost.springboot2.demo.common.constant.SystemErrorCodeEnum;
import org.slf4j.Logger;

import java.io.Serializable;

public class BaseException extends RuntimeException implements Serializable {

    /**
     * 错误码
     */
    private String code;

    /**
     * 错误信息
     */
    private String message;

    /**
     * 使用错误信息抛出异常
     *
     * @param message 错误信息
     */
    public BaseException(String message) {
        this(SystemErrorCodeEnum.SYSTEM_ERROR.getCode(), message);
    }

    public BaseException(String methodName, IErrorCodeEnum errorCodeEnum, Logger logger) {
        this(methodName, errorCodeEnum != null ? errorCodeEnum.getCode() : SystemErrorCodeEnum.SYSTEM_ERROR.getCode()
                , errorCodeEnum != null ? errorCodeEnum.getMessage() : SystemErrorCodeEnum.SYSTEM_ERROR.getMessage(),
                logger);
    }

    public BaseException(String methodName, String code, String message, Logger logger) {
        if (logger.isInfoEnabled()) {
            logger.warn("{} 业务异常: {}, {}", methodName, code, message);
        }

        this.code = code;
        this.message = message;
    }

    /**
     * 使用错误码和错误信息抛出异常
     *
     * @param code    错误码
     * @param message 错误信息
     */
    protected BaseException(String code, String message) {
        this.code = code;
        this.message = message;
    }


    /**
     * 获取状态码
     *
     * @return 状态码
     */
    public String getCode() {
        return code;
    }

    /**
     * 获取错误信息
     *
     * @return 错误信息
     */
    @Override
    public String getMessage() {
        return message;
    }
}
