package org.ghost.springboot2.demo.controller;

import org.ghost.springboot2.demo.dto.RspDTO;
import org.ghost.springboot2.demo.service.IDemoService;
import org.ghost.springboot2.demo.service.ITestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "demo2")
public class Demo2Controller extends BaseController {
    private final static Logger logger = LoggerFactory.getLogger(Demo2Controller.class);

    @Autowired
    private ITestService testService;

    @Autowired
    private IDemoService demoService;

    @RequestMapping(value = "cacheable/id/{id}", method = RequestMethod.GET)
    public RspDTO getCacheableUserInfo(@PathVariable Long id) {
        return testService.getCacheableUserInfo(id);
    }

    @RequestMapping(value = "cachePut/id/{id}", method = RequestMethod.GET)
    public RspDTO getCachePutUserInfo(@PathVariable Long id) {
        return testService.getCachePutUserInfo(id);
    }

    @RequestMapping(value = "del/id/{id}", method = RequestMethod.DELETE)
    public RspDTO delete(@PathVariable Long id) {
        return new RspDTO(testService.delete(id));
    }

    @RequestMapping(value = "info/id/{id}", method = RequestMethod.GET)
    public RspDTO getUserInfo(@PathVariable Long id) {
        return demoService.getUserInfo(id);
    }

}
