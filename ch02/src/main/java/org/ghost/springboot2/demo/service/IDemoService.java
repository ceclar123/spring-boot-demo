package org.ghost.springboot2.demo.service;

import org.ghost.springboot2.demo.dto.RspDTO;

public interface IDemoService {
    RspDTO getUserInfo(Long id);
}
