package org.ghost.springboot2.demo.dto;


import org.ghost.springboot2.demo.common.constant.SystemErrorCodeEnum;

import java.io.Serializable;


public class RspDTO implements Serializable {
    /**
     * 是否成功
     */
    private Boolean success;

    /**
     * 错误码
     */
    private String code;

    /**
     * 错误信息
     */
    private String message;

    /**
     * 返回数据
     */
    private Object data;

    /**
     * 使用错误码和错误信息构造返回DTO
     *
     * @param data 返回数据
     */
    public RspDTO(Object data) {
        this(SystemErrorCodeEnum.SUCCESS.getCode(), SystemErrorCodeEnum.SUCCESS.getMessage(), data);
    }

    /**
     * 使用错误码和错误信息构造返回DTO
     *
     * @param code    错误码
     * @param message 错误信息
     */
    public RspDTO(String code, String message) {
        this(code, message, null);
    }

    /**
     * @param code    错误码
     * @param message 错误信息
     * @param data    数据
     */
    public RspDTO(String code, String message, Object data) {
        this.code = code != null ? code : SystemErrorCodeEnum.SYSTEM_ERROR.getMessage();
        this.message = message != null ? message : "";
        this.data = data;
        this.success = SystemErrorCodeEnum.SUCCESS.getCode().equals(this.code);
    }

    /**
     * 是否成功
     *
     * @return Boolean
     */
    public Boolean isSuccess() {
        return success;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    /**
     * 获取错误码
     *
     * @return 错误码
     */
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取错误信息
     *
     * @return 错误信息
     */
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 获取数据
     *
     * @return T
     */
    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RspDTO{" +
                "success=" + success +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
