package org.ghost.springboot2.demo.service.impl;

import org.ghost.springboot2.demo.dto.RspDTO;
import org.ghost.springboot2.demo.service.IDemoService;
import org.springframework.cache.annotation.*;
import org.springframework.stereotype.Service;

@CacheConfig(cacheNames = "DEMO_")
@Service
public class DemoServiceImpl implements IDemoService {
    @Caching(
            cacheable = {@Cacheable(key = "#id + ''", unless = "#result == null")},
            put = {@CachePut(key = "#id + ''", unless = "#result == null")},
            evict = {@CacheEvict(key = "#id + ''")}
    )
    @Override
    public RspDTO getUserInfo(Long id) {
        return new RspDTO(id);
    }
}
