package org.ghost.springboot2.demo.component.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.util.Pool;

@Configuration
public class RedisCacheConfig {
    @Autowired
    private RedisProperties redisProperties;

    @Bean
    public JedisPoolConfig getJedisPoolConfig() {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxIdle(this.redisProperties.getMaxIdle());
        jedisPoolConfig.setMaxTotal(this.redisProperties.getMaxTotal());
        jedisPoolConfig.setMaxWaitMillis(this.redisProperties.getMaxWaitMillis());
        jedisPoolConfig.setTestOnBorrow(this.redisProperties.getTestOnBorrow());
        return jedisPoolConfig;
    }

    @Bean
    public Pool<Jedis> getJedisPool(JedisPoolConfig jedisPoolConfig) {
        return new JedisPool(jedisPoolConfig, this.redisProperties.getHost(), this.redisProperties.getPort(), this.redisProperties.getTimeout(), this.redisProperties.getPassword());
    }

    @Bean
    public GhostKeyGenerator getGhostKeyGenerator() {
        return new GhostKeyGenerator();
    }
}
