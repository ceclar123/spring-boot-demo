package org.ghost.springboot2.demo.util;

import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;


public class NumberUtil {
    private NumberUtil() {
    }

    private static final Long EMPTY_LONG = 0L;
    private static final Long LONG_DEFAULT_VALUE = 0L;
    private static final Integer EMPTY_INTEGER = 0;
    private static final Integer INTEGER_DEFAULT_VALUE = 0;

    public static Boolean isEmpty(final Long longInteger) {
        return longInteger == null || EMPTY_LONG.equals(longInteger);
    }

    /**
     * 如果值为NULL，则默认值为0
     *
     * @param longInteger 值
     * @return Long
     */
    public static Long getDefaultValue(Long longInteger) {
        return getDefaultValue(longInteger, LONG_DEFAULT_VALUE);
    }

    /**
     * 如果值为NULL，则返回默认值
     *
     * @param longInteger  值
     * @param defaultValue 默认值
     * @return Long
     */
    public static Long getDefaultValue(Long longInteger, Long defaultValue) {
        return longInteger != null ? longInteger : defaultValue;
    }


    public static Boolean isEmpty(final Integer integer) {
        return integer == null || EMPTY_INTEGER.equals(integer);
    }

    public static Integer getDefaultValue(final Integer integer) {
        return getDefaultValue(integer, INTEGER_DEFAULT_VALUE);
    }

    public static Integer getDefaultValue(final Integer integer, Integer defaultValue) {
        return integer != null ? integer : defaultValue;
    }

    public static Integer tryParse(String val, Integer defaultValue) {
        try {
            return Integer.parseInt(val);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static long toLong(String input, long defaultVal) {
        try {
            return Long.parseLong(input);
        } catch (Exception e) {
            return defaultVal;
        }
    }

    public static int toInt(String input, int defaultVal) {
        try {
            return Integer.parseInt(input);
        } catch (Exception e) {
            return defaultVal;
        }
    }

    public static BigDecimal toBigDecimal(String input, BigDecimal defaultVal) {
        try {
            return NumberUtils.createBigDecimal(input);
        } catch (Exception e) {
            return defaultVal;
        }
    }
}
