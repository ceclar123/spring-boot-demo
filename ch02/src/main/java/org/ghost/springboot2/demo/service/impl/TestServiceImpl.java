package org.ghost.springboot2.demo.service.impl;

import org.ghost.springboot2.demo.dto.RspDTO;
import org.ghost.springboot2.demo.service.ITestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements ITestService {
    private final static Logger logger = LoggerFactory.getLogger(TestServiceImpl.class);

    @Cacheable(cacheNames = "TEST_", key = "#id + ''", unless = "#result == null")
    @Override
    public RspDTO getCacheableUserInfo(Long id) {
        logger.info("*****TestServiceImpl.getCacheableUserInfo参数id={}", id);
        return new RspDTO(id);
    }

    @CachePut(cacheNames = "TEST_", key = "#id + ''", unless = "#result == null")
    @Override
    public RspDTO getCachePutUserInfo(Long id) {
        logger.info("*****TestServiceImpl.getCachePutUserInfo参数id={}", id);
        return new RspDTO(id);
    }

    @CacheEvict(cacheNames = "TEST_", key = "#id + ''")
    @Override
    public boolean delete(Long id) {
        logger.info("*****TestServiceImpl.delete参数id={}", id);
        return true;
    }
}
