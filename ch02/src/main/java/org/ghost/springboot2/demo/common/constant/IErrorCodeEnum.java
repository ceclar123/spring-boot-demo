package org.ghost.springboot2.demo.common.constant;

public interface IErrorCodeEnum {
    String getCode();

    String getMessage();
}
