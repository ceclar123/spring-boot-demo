package org.ghost.springboot2.demo.util;

import org.springframework.util.Assert;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class GenericMapParaSource<K, V> {
    private final Map<K, V> values = new HashMap<K, V>();

    public GenericMapParaSource() {
    }

    public GenericMapParaSource(K paramName, V value) {
        addValue(paramName, value);
    }

    public GenericMapParaSource<K, V> addValue(K paramName, V value) {
        Assert.notNull(paramName, "Parameter name must not be null");
        this.values.put(paramName, value);
        return this;
    }

    public Map<K, V> getValues() {
        return Collections.unmodifiableMap(this.values);
    }

    public boolean hasValue(K paramName) {
        return this.values.containsKey(paramName);
    }
}
