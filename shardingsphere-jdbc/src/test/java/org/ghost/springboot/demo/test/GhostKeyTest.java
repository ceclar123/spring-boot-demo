package org.ghost.springboot.demo.test;

import org.ghost.springboot.demo.component.sharding.GhostGenerator;
import org.ghost.springboot.demo.component.sharding.GhostKeyParser;
import org.ghost.springboot.demo.component.sharding.UserGenerator;
import org.ghost.springboot.demo.util.DateUtil;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class GhostKeyTest {
    @Test
    public void test1() {
        GhostKeyParser ghostKeyParser = new GhostKeyParser(41, UserGenerator.EPOCH);
        System.out.println(ghostKeyParser.getDateTime(259119379244355585L));

        System.out.println(LocalDateTime.of(1970, 1, 1, 8, 0, 0).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        System.out.println(LocalDateTime.of(2017, 1, 1, 8, 0, 0).atZone(ZoneId.of("Asia/Shanghai")).toInstant().toEpochMilli());


        Calendar calendar = Calendar.getInstance();
        calendar.set(1970, Calendar.JANUARY, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        System.out.println(calendar.getTimeInMillis());

        System.out.println(GhostGenerator.EPOCH);

        System.out.println(DateUtil.dateToStrYMD_HMS(new Date(1288834974657L)));
    }
}
