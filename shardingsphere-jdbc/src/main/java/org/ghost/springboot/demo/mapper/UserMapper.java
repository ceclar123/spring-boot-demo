package org.ghost.springboot.demo.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.ghost.springboot.demo.entity.UserEntity;

public interface UserMapper extends BaseMapper<UserEntity> {
}
