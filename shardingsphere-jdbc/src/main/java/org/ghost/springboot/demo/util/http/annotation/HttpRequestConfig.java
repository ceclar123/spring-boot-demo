package org.ghost.springboot.demo.util.http.annotation;

import org.springframework.core.annotation.AliasFor;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface HttpRequestConfig {
    @AliasFor("name")
    String value() default "";

    /**
     * 接口名a/b/c
     *
     * @return
     */
    String name() default "";

    /**
     * 请求方法
     *
     * @return
     */
    HttpMethod method() default HttpMethod.GET;

    /**
     * contentType
     *
     * @return
     */
    String contentType() default MediaType.APPLICATION_JSON_UTF8_VALUE;

    /**
     * 请求头a=b
     *
     * @return
     */
    String[] headers() default {};

    /**
     * 扩展参数a=b
     *
     * @return
     */
    String[] params() default {};
}
