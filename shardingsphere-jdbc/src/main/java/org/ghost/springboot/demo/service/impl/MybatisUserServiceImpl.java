package org.ghost.springboot.demo.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.ghost.springboot.demo.entity.UserEntity;
import org.ghost.springboot.demo.mapper.UserMapper;
import org.ghost.springboot.demo.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "mybatisUserServiceImpl")
public class MybatisUserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements IUserService {
    private final Logger logger = LoggerFactory.getLogger(MybatisUserServiceImpl.class);

    @Override
    public boolean add(List<UserEntity> userEntityList) {
        if (CollectionUtils.isNotEmpty(userEntityList)) {
            return this.insertBatch(userEntityList);
        }
        return false;
    }

    @Override
    public boolean add(UserEntity userEntity) {
        if (userEntity != null) {
            return this.insert(userEntity);
        }
        return false;
    }

    @Override
    public boolean updateById(UserEntity userEntity, Long id) {
        if (userEntity != null && id != null) {
            userEntity.setId(id);
            return this.updateById(userEntity);
        }
        return false;
    }

    @Override
    public boolean updateByClientSn(UserEntity userEntity, Long clientSn) {
        if (userEntity != null && clientSn != null) {
            userEntity.setClientSn(clientSn);
            return this.update(userEntity, new EntityWrapper<UserEntity>().eq("client_sn", clientSn));
        }
        return false;
    }

    @Override
    public boolean deleteById(Long id) {
        if (id != null) {
            return baseMapper.deleteById(id) > 0;
        }
        return false;
    }

    @Override
    public boolean deleteByClientSn(Long clientSn) {
        if (clientSn != null) {
            return this.delete(new EntityWrapper<UserEntity>().eq("client_sn", clientSn));
        }
        return false;
    }

    @Override
    public UserEntity selectById(Long id) {
        if (id != null) {
            return baseMapper.selectById(id);
        }
        return null;
    }

    @Override
    public UserEntity selectByClientSn(Long clientSn) {
        if (clientSn != null) {
            return this.selectOne(new EntityWrapper<UserEntity>().eq("client_sn", clientSn));
        }
        return null;
    }
}
