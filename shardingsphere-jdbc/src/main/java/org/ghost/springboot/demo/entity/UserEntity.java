package org.ghost.springboot.demo.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

@TableName("t_user")
public class UserEntity extends Model<UserEntity> implements Serializable {
    private static final long serialVersionUID = 6986425202309558172L;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    @TableField("username")
    private String userName;
    private Integer age;
    private String sex;
    @TableField("client_sn")
    private Long clientSn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Long getClientSn() {
        return clientSn;
    }

    public void setClientSn(Long clientSn) {
        this.clientSn = clientSn;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", clientSn=" + clientSn +
                '}';
    }
}
