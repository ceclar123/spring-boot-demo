package org.ghost.springboot.demo.common.function;

public interface Action3<A, B, C> {
    void execute(A a, B b, C c);
}
