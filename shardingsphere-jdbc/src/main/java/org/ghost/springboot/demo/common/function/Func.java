package org.ghost.springboot.demo.common.function;

public interface Func<R> {
    R execute();
}
