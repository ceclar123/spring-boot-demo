package org.ghost.springboot.demo.common.exception;

import org.ghost.springboot.demo.common.constant.SystemErrorCodeEnum;


public class RepositoryUpstreamServiceErrorException extends BaseException {
    public RepositoryUpstreamServiceErrorException(String message) {
        super(SystemErrorCodeEnum.UPSTREAM_SERVICE_ERROR.getCode(), message);
    }

    public RepositoryUpstreamServiceErrorException(String code, String message) {
        super(code, message);
    }
}
