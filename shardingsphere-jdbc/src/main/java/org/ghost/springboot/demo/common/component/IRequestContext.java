package org.ghost.springboot.demo.common.component;


public interface IRequestContext {
    String getRemoteIp();
    Integer getSourceChannel();
}
