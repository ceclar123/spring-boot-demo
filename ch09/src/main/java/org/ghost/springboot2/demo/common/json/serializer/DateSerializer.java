package org.ghost.springboot2.demo.common.json.serializer;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.ghost.springboot2.demo.util.DateUtil;

import java.io.IOException;
import java.util.Date;

public class DateSerializer extends JsonSerializer<Date> {
    @Override
    public void serialize(Date value, JsonGenerator gen, SerializerProvider serializers) throws IOException,
            JsonProcessingException {
        String formattedDate = DateUtil.dateToStrYMD_HMS(value);
        gen.writeString(formattedDate);
    }
}