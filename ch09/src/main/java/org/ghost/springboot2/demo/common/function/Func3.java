package org.ghost.springboot2.demo.common.function;

public interface Func3<A, B, C, R> {
    R execute(A a, B b, C c);
}

