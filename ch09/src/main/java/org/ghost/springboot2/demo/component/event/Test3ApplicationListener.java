package org.ghost.springboot2.demo.component.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class Test3ApplicationListener implements ApplicationListener<Test3ApplicationEvent> {
    private final static Logger logger = LoggerFactory.getLogger(AnnotationRegisterListener.class);

    @Override
    public void onApplicationEvent(Test3ApplicationEvent event) {
        logger.info("*****AnnotationRegisterListener.test3监听->" + event.getName());
    }
}
