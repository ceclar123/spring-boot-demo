package org.ghost.springboot2.demo.component.event;

import org.ghost.springboot2.demo.service.impl.TestServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.SmartApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class Test4SmartApplicationListener implements SmartApplicationListener {
    private final static Logger logger = LoggerFactory.getLogger(AnnotationRegisterListener.class);

    @Override
    public boolean supportsEventType(Class<? extends ApplicationEvent> eventType) {
        return eventType == Test4ApplicationEvent.class;
    }

    @Override
    public boolean supportsSourceType(Class<?> sourceType) {
        return sourceType == TestServiceImpl.class;
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        Test4ApplicationEvent test4ApplicationEvent = (Test4ApplicationEvent) event;
        logger.info("*****AnnotationRegisterListener.test4监听->" + test4ApplicationEvent.getName());

    }

    @Override
    public int getOrder() {
        return 1;
    }
}
