package org.ghost.springboot2.demo.common.function;

public interface Func<R> {
    R execute();
}
