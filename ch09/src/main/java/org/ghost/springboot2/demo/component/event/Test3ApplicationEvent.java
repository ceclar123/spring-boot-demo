package org.ghost.springboot2.demo.component.event;

import org.springframework.context.ApplicationEvent;

public class Test3ApplicationEvent extends ApplicationEvent {
    private String name;

    public Test3ApplicationEvent(Object source, String name) {
        super(source);
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
