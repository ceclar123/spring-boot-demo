package org.ghost.springboot2.demo.service;

public interface ITestService {
    String test1();

    String test2();

    String test3();

    String test4();
}
