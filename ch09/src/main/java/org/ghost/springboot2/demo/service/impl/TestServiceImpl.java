package org.ghost.springboot2.demo.service.impl;

import org.ghost.springboot2.demo.component.event.Test2ApplicationEvent;
import org.ghost.springboot2.demo.component.event.Test3ApplicationEvent;
import org.ghost.springboot2.demo.component.event.Test4ApplicationEvent;
import org.ghost.springboot2.demo.component.util.SpringUtil;
import org.ghost.springboot2.demo.config.TaskExecutorConfig;
import org.ghost.springboot2.demo.service.ITestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements ITestService {
    private final static Logger logger = LoggerFactory.getLogger(TestServiceImpl.class);

    @Autowired
    private TaskExecutorConfig taskExecutorConfig;

    @Override
    public String test1() {
        taskExecutorConfig.getAsyncExecutor().execute(() -> {
            logger.info("*****第一种方式*****");
        });
        return "test1";
    }

    @Override
    public String test2() {
        SpringUtil.getApplicationContext().publishEvent(new Test2ApplicationEvent(this, "test2"));
        return "test2";
    }

    @Override
    public String test3() {
        SpringUtil.getApplicationContext().publishEvent(new Test3ApplicationEvent(this, "test3"));
        return "test3";
    }

    @Override
    public String test4() {
        SpringUtil.getApplicationContext().publishEvent(new Test4ApplicationEvent(this, "test4"));
        return "test4";
    }
}
