package org.ghost.springboot2.demo.common.component;


public interface IRequestContext {
    String getRemoteIp();

    Integer getSourceChannel();
}
