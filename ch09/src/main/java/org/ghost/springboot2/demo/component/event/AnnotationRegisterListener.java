package org.ghost.springboot2.demo.component.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class AnnotationRegisterListener {
    private final static Logger logger = LoggerFactory.getLogger(AnnotationRegisterListener.class);

    @EventListener
    public void test2(Test2ApplicationEvent event) {
        logger.info("*****AnnotationRegisterListener.test2监听->" + event.getName());
    }
}
