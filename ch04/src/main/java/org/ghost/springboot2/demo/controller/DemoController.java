package org.ghost.springboot2.demo.controller;

import org.ghost.springboot2.demo.dto.RspDTO;
import org.ghost.springboot2.demo.service.ITestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping(value = "demo")
public class DemoController extends BaseController {
    private final static Logger logger = LoggerFactory.getLogger(DemoController.class);

    @Autowired
    private ITestService testService;

    @RequestMapping(value = "test1", method = RequestMethod.GET)
    public RspDTO get1() throws ExecutionException, InterruptedException {
        return this.render(testService.getAsync1().get());
    }

    @RequestMapping(value = "test2", method = RequestMethod.GET)
    public RspDTO get2() throws ExecutionException, InterruptedException {
        return this.render(testService.getAsync2().get());
    }

    @RequestMapping(value = "test3", method = RequestMethod.GET)
    public RspDTO get3() {
        testService.process1();
        return this.render();
    }

    @RequestMapping(value = "test4", method = RequestMethod.GET)
    public RspDTO get4() {
        testService.process2();
        return this.render();
    }
}

