package org.ghost.springboot2.demo.util;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;


public class MimeTypeUtil {
    private static final String DEFAULT_TYPE = "application/octet-stream";
    private static final Map<String, String> DATA = new HashMap<String, String>();

    static {
        //按字母排序,参考http://www.w3school.com.cn/media/media_mimeref.asp
        DATA.put("acx", "application/internet-property-stream");
        DATA.put("ai", "application/postscript");
        DATA.put("aif", "audio/x-aiff");
        DATA.put("aifc", "audio/x-aiff");
        DATA.put("aiff", "audio/x-aiff");
        DATA.put("asf", "video/x-ms-asf");
        DATA.put("asr", "video/x-ms-asf");
        DATA.put("asx", "video/x-ms-asf");
        DATA.put("au", "audio/basic");
        DATA.put("avi", "video/x-msvideo");
        DATA.put("axs", "application/olescript");
        DATA.put("bas", "text/plain");
        DATA.put("bcpio", "application/x-bcpio");
        DATA.put("bin", "application/octet-stream");
        DATA.put("bmp", "image/bmp");
        DATA.put("c", "text/plain");
        DATA.put("cat", "application/vnd.ms-pkiseccat");
        DATA.put("cdf", "application/x-cdf");
        DATA.put("cer", "application/x-x509-ca-cert");
        DATA.put("class", "application/octet-stream");
        DATA.put("clp", "application/x-msclip");
        DATA.put("cmx", "image/x-cmx");
        DATA.put("cod", "image/cis-cod");
        DATA.put("cpio", "application/x-cpio");
        DATA.put("crd", "application/x-mscardfile");
        DATA.put("crl", "application/pkix-crl");
        DATA.put("crt", "application/x-x509-ca-cert");
        DATA.put("csh", "application/x-csh");
        DATA.put("css", "text/css");
        DATA.put("dcr", "application/x-director");
        DATA.put("der", "application/x-x509-ca-cert");
        DATA.put("dir", "application/x-director");
        DATA.put("dll", "application/x-msdownload");
        DATA.put("dms", "application/octet-stream");
        DATA.put("doc", "application/msword");
        DATA.put("dot", "application/msword");
        DATA.put("dvi", "application/x-dvi");
        DATA.put("dxr", "application/x-director");
        DATA.put("eps", "application/postscript");
        DATA.put("etx", "text/x-setext");
        DATA.put("evy", "application/envoy");
        DATA.put("exe", "application/octet-stream");
        DATA.put("fif", "application/fractals");
        DATA.put("flr", "x-world/x-vrml");
        DATA.put("gif", "image/gif");
        DATA.put("gtar", "application/x-gtar");
        DATA.put("gz", "application/x-gzip");
        DATA.put("h", "text/plain");
        DATA.put("hdf", "application/x-hdf");
        DATA.put("hlp", "application/winhlp");
        DATA.put("hqx", "application/mac-binhex40");
        DATA.put("hta", "application/hta");
        DATA.put("htc", "text/x-component");
        DATA.put("htm", "text/html");
        DATA.put("html", "text/html");
        DATA.put("htt", "text/webviewhtml");
        DATA.put("ico", "image/x-icon");
        DATA.put("ief", "image/ief");
        DATA.put("iii", "application/x-iphone");
        DATA.put("ins", "application/x-internet-signup");
        DATA.put("isp", "application/x-internet-signup");
        DATA.put("jfif", "image/pipeg");
        DATA.put("jpe", "image/jpeg");
        DATA.put("jpeg", "image/jpeg");
        DATA.put("jpg", "image/jpeg");
        DATA.put("js", "application/x-javascript");
        DATA.put("latex", "application/x-latex");
        DATA.put("lha", "application/octet-stream");
        DATA.put("lsf", "video/x-la-asf");
        DATA.put("lsx", "video/x-la-asf");
        DATA.put("lzh", "application/octet-stream");
        DATA.put("m13", "application/x-msmediaview");
        DATA.put("m14", "application/x-msmediaview");
        DATA.put("m3u", "audio/x-mpegurl");
        DATA.put("man", "application/x-troff-man");
        DATA.put("mdb", "application/x-msaccess");
        DATA.put("me", "application/x-troff-me");
        DATA.put("mht", "message/rfc822");
        DATA.put("mhtml", "message/rfc822");
        DATA.put("mid", "audio/mid");
        DATA.put("mny", "application/x-msmoney");
        DATA.put("mov", "video/quicktime");
        DATA.put("movie", "video/x-sgi-movie");
        DATA.put("mp2", "video/mpeg");
        DATA.put("mp3", "audio/mpeg");
        DATA.put("mpa", "video/mpeg");
        DATA.put("mpe", "video/mpeg");
        DATA.put("mpeg", "video/mpeg");
        DATA.put("mpg", "video/mpeg");
        DATA.put("mpp", "application/vnd.ms-project");
        DATA.put("mpv2", "video/mpeg");
        DATA.put("ms", "application/x-troff-ms");
        DATA.put("mvb", "application/x-msmediaview");
        DATA.put("nws", "message/rfc822");
        DATA.put("oda", "application/oda");
        DATA.put("p10", "application/pkcs10");
        DATA.put("p12", "application/x-pkcs12");
        DATA.put("p7b", "application/x-pkcs7-certificates");
        DATA.put("p7c", "application/x-pkcs7-mime");
        DATA.put("p7m", "application/x-pkcs7-mime");
        DATA.put("p7r", "application/x-pkcs7-certreqresp");
        DATA.put("p7s", "application/x-pkcs7-signature");
        DATA.put("pbm", "image/x-portable-bitmap");
        DATA.put("pdf", "application/pdf");
        DATA.put("pfx", "application/x-pkcs12");
        DATA.put("pgm", "image/x-portable-graymap");
        DATA.put("pko", "application/ynd.ms-pkipko");
        DATA.put("pma", "application/x-perfmon");
        DATA.put("pmc", "application/x-perfmon");
        DATA.put("pml", "application/x-perfmon");
        DATA.put("pmr", "application/x-perfmon");
        DATA.put("pmw", "application/x-perfmon");
        DATA.put("pnm", "image/x-portable-anymap");
        DATA.put("pot,", "application/vnd.ms-powerpoint");
        DATA.put("ppm", "image/x-portable-pixmap");
        DATA.put("pps", "application/vnd.ms-powerpoint");
        DATA.put("ppt", "application/vnd.ms-powerpoint");
        DATA.put("prf", "application/pics-rules");
        DATA.put("ps", "application/postscript");
        DATA.put("pub", "application/x-mspublisher");
        DATA.put("qt", "video/quicktime");
        DATA.put("ra", "audio/x-pn-realaudio");
        DATA.put("ram", "audio/x-pn-realaudio");
        DATA.put("ras", "image/x-cmu-raster");
        DATA.put("rgb", "image/x-rgb");
        DATA.put("rmi", "audio/mid");
        DATA.put("roff", "application/x-troff");
        DATA.put("rtf", "application/rtf");
        DATA.put("rtx", "text/richtext");
        DATA.put("scd", "application/x-msschedule");
        DATA.put("sct", "text/scriptlet");
        DATA.put("setpay", "application/set-payment-initiation");
        DATA.put("setreg", "application/set-registration-initiation");
        DATA.put("sh", "application/x-sh");
        DATA.put("shar", "application/x-shar");
        DATA.put("sit", "application/x-stuffit");
        DATA.put("snd", "audio/basic");
        DATA.put("spc", "application/x-pkcs7-certificates");
        DATA.put("spl", "application/futuresplash");
        DATA.put("src", "application/x-wais-source");
        DATA.put("sst", "application/vnd.ms-pkicertstore");
        DATA.put("stl", "application/vnd.ms-pkistl");
        DATA.put("stm", "text/html");
        DATA.put("svg", "image/svg+xml");
        DATA.put("sv4cpio", "application/x-sv4cpio");
        DATA.put("sv4crc", "application/x-sv4crc");
        DATA.put("swf", "application/x-shockwave-flash");
        DATA.put("t", "application/x-troff");
        DATA.put("tar", "application/x-tar");
        DATA.put("tcl", "application/x-tcl");
        DATA.put("tex", "application/x-tex");
        DATA.put("texi", "application/x-texinfo");
        DATA.put("texinfo", "application/x-texinfo");
        DATA.put("tgz", "application/x-compressed");
        DATA.put("tif", "image/tiff");
        DATA.put("tiff", "image/tiff");
        DATA.put("tr", "application/x-troff");
        DATA.put("trm", "application/x-msterminal");
        DATA.put("tsv", "text/tab-separated-values");
        DATA.put("txt", "text/plain");
        DATA.put("uls", "text/iuls");
        DATA.put("ustar", "application/x-ustar");
        DATA.put("vcf", "text/x-vcard");
        DATA.put("vrml", "x-world/x-vrml");
        DATA.put("wav", "audio/x-wav");
        DATA.put("wcm", "application/vnd.ms-works");
        DATA.put("wdb", "application/vnd.ms-works");
        DATA.put("wks", "application/vnd.ms-works");
        DATA.put("wmf", "application/x-msmetafile");
        DATA.put("wps", "application/vnd.ms-works");
        DATA.put("wri", "application/x-mswrite");
        DATA.put("wrl", "x-world/x-vrml");
        DATA.put("wrz", "x-world/x-vrml");
        DATA.put("xaf", "x-world/x-vrml");
        DATA.put("xbm", "image/x-xbitmap");
        DATA.put("xla", "application/vnd.ms-excel");
        DATA.put("xlc", "application/vnd.ms-excel");
        DATA.put("xlm", "application/vnd.ms-excel");
        DATA.put("xls", "application/vnd.ms-excel");
        DATA.put("xlt", "application/vnd.ms-excel");
        DATA.put("xlw", "application/vnd.ms-excel");
        DATA.put("xof", "x-world/x-vrml");
        DATA.put("xpm", "image/x-xpixmap");
        DATA.put("xwd", "image/x-xwindowdump");
        DATA.put("z", "application/x-compress");
        DATA.put("zip", "application/zip");
    }

    private MimeTypeUtil() {

    }

    public static String getMimeType(String suffixName) {
        if (StringUtils.isNotBlank(suffixName)) {
            String value = DATA.get(suffixName.toLowerCase());
            if (StringUtils.isNotBlank(value)) {
                return value;
            }

            return DEFAULT_TYPE;
        }

        return "";
    }
}
