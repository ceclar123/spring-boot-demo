package org.ghost.springboot2.demo.util;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Objects;

public class ListUtil {
    private ListUtil() {
    }

    /**
     * 重写toString方法的时候，List对象转成String类型
     *
     * @param source
     * @param <T>
     * @return
     */
    public static <T> String toString(List<T> source) {
        if (CollectionUtils.isNotEmpty(source)) {
            return source.stream().filter(Objects::nonNull).map(Object::toString).reduce("", (a, b) -> StringUtils.isBlank(a) ? b : a + "," + b);
        }

        return "";
    }
}