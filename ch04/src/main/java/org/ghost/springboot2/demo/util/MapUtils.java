package org.ghost.springboot2.demo.util;

import java.util.HashMap;

public class MapUtils {
    public static <K, V> HashMap<K, V> newHashMap(K key, V val) {
        HashMap<K, V> hashMap = new HashMap<K, V>();
        if (key != null) {
            hashMap.put(key, val);
        }
        return hashMap;
    }
}
