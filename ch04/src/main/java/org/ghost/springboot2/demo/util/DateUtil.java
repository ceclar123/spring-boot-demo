package org.ghost.springboot2.demo.util;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

final public class DateUtil {
    /**
     * yyyy/M/d
     */
    public static final String DATE_FORMAT1 = "yyyy/M/d";
    /**
     * yyyy-M-d
     */
    public static final String DATE_FORMAT2 = "yyyy-M-d";
    /**
     *
     G 年代标志符
     y 年
     M 月
     d 日
     h 时 在上午或下午 (1~12)
     H 时 在一天中 (0~23)
     m 分
     s 秒
     S 毫秒
     E 星期
     D 一年中的第几天
     F 一月中第几个星期几
     w 一年中第几个星期
     W 一月中第几个星期
     a 上午 / 下午 标记符
     k 时 在一天中 (1~24)
     K 时 在上午或下午 (0~11)
     z 时区
     */
    /**
     * yyyy.M.d
     */
    public static final String DATE_FORMAT3 = "yyyy.M.d";
    /**
     * yyyy-MM-dd
     */
    public static final String DATE_FORMAT4 = "yyyy-MM-dd";
    /**
     * yyyy/MM/dd
     */
    public static final String DATE_FORMAT5 = "yyyy/MM/dd";
    /**
     * yyyy.MM.dd
     */
    public static final String DATE_FORMAT6 = "yyyy.MM.dd";
    /**
     * yy-M-d
     */
    public static final String DATE_FORMAT7 = "yy-M-d";
    /**
     * yy/M/d
     */
    public static final String DATE_FORMAT8 = "yy/M/d";
    /**
     * yy.M.d
     */
    public static final String DATE_FORMAT9 = "yy.M.d";
    /**
     * yy/MM/dd
     */
    public static final String DATE_FORMA10 = "yy/MM/dd";
    /**
     * MM/dd
     */
    public static final String DATE_FORMA11 = "MM/dd";
    /**
     * HH:mm:ss
     */
    public static final String TIME_FORMAT1 = "HH:mm:ss";
    /**
     * HH:mm
     */
    public static final String TIME_FORMAT2 = "HH:mm";
    /**
     * HH:mm:ss tt
     */
    public static final String TIME_FORMAT3 = "HH:mm:ss a";
    /**
     * HH:mm tt
     */
    public static final String TIME_FORMAT4 = "HH:mm a";
    private static final Logger logger = LoggerFactory.getLogger(DateUtil.class);
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    private static final long EMPTY_LONG = 0L;

    /**
     * 是否为空
     *
     * @param date 日期
     * @return 是否为空
     */
    public static Boolean isEmpty(final Date date) {
        return date == null || EMPTY_LONG == date.getTime();
    }

    /**
     * 如果值为null，获取为当前时间的默认值
     *
     * @param date 值
     * @return Date
     */
    public static Date getDefaultValue(final Date date) {
        return getDefaultValue(date, new Date());
    }

    /**
     * 如果值为null，获取默认值
     *
     * @param date         值
     * @param defaultValue 默认值
     * @return Date
     */
    public static Date getDefaultValue(final Date date, Date defaultValue) {
        return date != null ? date : defaultValue;
    }

    /**
     * 获取当前时间
     *
     * @return 当前时间
     */
    public static Date now() {
        return new Date();
    }

    /**
     * 获取明天
     *
     * @return 明天
     */
    public static Date tomorrow() {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, 1);
        return cal.getTime();
    }

    /**
     * 获取当前时间
     *
     * @return 当前时间
     */
    public static String nowStr() {
        return DateFormatUtils.ISO_8601_EXTENDED_DATE_FORMAT
                .format(new Date());
    }

    /**
     * 获取明天
     *
     * @return 明天
     */
    public static String tomorrowStr() {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, 1);
        return DateFormatUtils.ISO_8601_EXTENDED_DATE_FORMAT.format(cal.getTime());
    }

    /**
     * 日期字符串转换为Date
     *
     * @param dateStr 日期字符串
     * @return Date
     */
    public static Date toDate(String dateStr) {
        return toDate(dateStr, DEFAULT_DATE_FORMAT);
    }


    public static Date toDate(String dateStr, String format) {
        Date date = null;
        try {
            date = DateUtils.parseDate(dateStr, format);
        } catch (ParseException | IllegalArgumentException e) {
            if (logger.isWarnEnabled()) {
                logger.warn("日期时间解析失败: {}, {}", e.getMessage(), e);
            }
        }

        return date;
    }

    public static Date getWeekStartTime() {
        Calendar cal = Calendar.getInstance();
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (dayOfWeek == 0) {
            dayOfWeek = 7;
        }
        cal.add(Calendar.DATE, -dayOfWeek + 1);
        return cal.getTime();
    }

    public static Date getWeekEndTime() {
        Calendar cal = Calendar.getInstance();
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (dayOfWeek == 0) {
            dayOfWeek = 7;
        }
        cal.add(Calendar.DATE, -dayOfWeek + 7);
        return cal.getTime();
    }

    public static String dateToString(Date date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }

    public static String dateToYMD(Date date) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT4);
        return format.format(date);
    }

    public static String dateToYMDHMS(Date date) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT4 + " " + TIME_FORMAT1);
        return format.format(date);
    }

    public static String dateToYMD(LocalDateTime dateTime) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern(DATE_FORMAT4);
        return df.format(dateTime);
    }

    public static String dateToYMD(LocalDate date) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern(DATE_FORMAT4);
        return df.format(date);
    }

    public static String dateToYMDHMS(LocalDateTime dateTime) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern(DATE_FORMAT4 + " " + TIME_FORMAT1);
        return df.format(dateTime);
    }

    public static DayOfWeek dateToWeekDay(Date date) {
        LocalDateTime dateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        return dateTime.getDayOfWeek();
    }

    public static String dateToWeekDayName(Date date) {
        DayOfWeek dayOfWeek = dateToWeekDay(date);
        String result = "";
        switch (dayOfWeek) {
            case MONDAY:
                result = "周一";
                break;
            case TUESDAY:
                result = "周二";
                break;
            case WEDNESDAY:
                result = "周三";
                break;
            case THURSDAY:
                result = "周四";
                break;
            case FRIDAY:
                result = "周五";
                break;
            case SATURDAY:
                result = "周六";
                break;
            case SUNDAY:
                result = "周日";
                break;
            default:
                result = "未知";
                break;
        }

        return result;
    }

    public static String getTodayYMD() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(currentTime);
        return dateString;
    }

    public static String dateToStrYMD(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(date);
    }

    public static String dateToStrYMD_HMS(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.format(date);
    }

    public static String dateToStr(Date date, String pattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        return formatter.format(date);
    }

    public static Date strToDateYMD(String str) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        try {
            return formatter.parse(str);
        } catch (ParseException var3) {
            logger.error("鏃ユ湡杞\ue101崲寮傚父锛歞ate = {},exception = {}", str, var3);
            return null;
        }
    }

    public static Date strToDateYMD_HMS(String str) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            return formatter.parse(str);
        } catch (ParseException var3) {
            logger.error("鏃ユ湡杞\ue101崲寮傚父锛歞ate = {},exception = {}", str, var3);
            return null;
        }
    }

    public static Date strToDate(String str, String pattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);

        try {
            return formatter.parse(str);
        } catch (ParseException var4) {
            logger.error("鏃ユ湡杞\ue101崲寮傚父锛歞ate = {},exception = {}", str, var4);
            return null;
        }
    }

    public static String getTodayYMD_HMS() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);
        return dateString;
    }

}

