package org.ghost.springboot2.demo.service.impl;

import org.ghost.springboot2.demo.config.TaskExecutorConfig;
import org.ghost.springboot2.demo.service.ITestService;
import org.ghost.springboot2.demo.util.ChineseNameUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

@Service
public class TestServiceImpl implements ITestService {
    private final static Logger logger = LoggerFactory.getLogger(TestServiceImpl.class);

    @Autowired
    private TaskExecutorConfig taskExecutorConfig;

    @Async("asyncTaskExecutor")
    @Override
    public Future<String> getAsync1() {
        return new AsyncResult<String>(ChineseNameUtil.getChineseName());
    }

    @Override
    public Future<String> getAsync2() {
        return taskExecutorConfig.getAsyncTaskExecutor().submit(ChineseNameUtil::getChineseName);
    }

    @Async("asyncTaskExecutor")
    @Override
    public void process1() {
        ChineseNameUtil.getChineseName();
    }

    @Override
    public void process2() {
        taskExecutorConfig.getAsyncExecutor().execute(() -> System.out.println(ChineseNameUtil.getChineseName()));
    }
}
