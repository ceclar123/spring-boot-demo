package org.ghost.springboot2.demo.service;

import java.util.concurrent.Future;

public interface ITestService {
    Future<String> getAsync1();

    Future<String> getAsync2();

    void process1();

    void process2();
}
