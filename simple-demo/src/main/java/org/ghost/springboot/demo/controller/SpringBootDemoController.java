package org.ghost.springboot.demo.controller;

import io.swagger.annotations.ApiOperation;
import org.ghost.springboot.demo.dto.RspDTO;
import org.ghost.springboot.demo.service.IWeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "demo")
public class SpringBootDemoController extends BaseController {
    @Autowired
    private IWeatherService weatherService;

    @RequestMapping(value = "hello", method = RequestMethod.GET)
    public RspDTO helloWorld() {
        try {
            Thread.sleep(3000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this.render("Hello World");
    }

    @ApiOperation(value = "查询指定城市天气预报")
    @RequestMapping(value = "weather", method = RequestMethod.GET)
    public RspDTO getWeather(@RequestParam String city) {
        return this.render(weatherService.getWeatherForecastInfo(city));
    }
}

