package org.ghost.springboot.demo.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.ghost.springboot.demo.dto.weather.WeatherForecastRspDTO;
import org.ghost.springboot.demo.service.IWeatherService;
import org.ghost.springboot.demo.util.http.HttpClientHelper;
import org.ghost.springboot.demo.util.http.HttpRequestBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;

@Service
public class WeatherServiceImpl implements IWeatherService {
    private final Logger logger = LoggerFactory.getLogger(WeatherServiceImpl.class);

    @Override
    public WeatherForecastRspDTO getWeatherForecastInfo(String city) {
        if (StringUtils.isNotBlank(city)) {
            String url = MessageFormat.format("{0}?city={1}", "https://www.sojson.com/open/api/weather/json.shtml", city);

            HttpRequestBuilder httpRequestBuilder = new HttpRequestBuilder()
                    .build(HttpMethod.GET, url)
                    .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);

            return HttpClientHelper.invoke(httpRequestBuilder, WeatherForecastRspDTO.class);
        }

        return null;
    }
}
