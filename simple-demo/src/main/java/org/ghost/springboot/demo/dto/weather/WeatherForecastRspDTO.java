package org.ghost.springboot.demo.dto.weather;

import java.io.Serializable;

public class WeatherForecastRspDTO implements Serializable {
    private String date;
    private String message;
    private Integer status;
    private String city;
    private Integer count;
    private WeatherForecastDTO data;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public WeatherForecastDTO getData() {
        return data;
    }

    public void setData(WeatherForecastDTO data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "WeatherForecastRspDTO{" +
                "date='" + date + '\'' +
                ", message='" + message + '\'' +
                ", status=" + status +
                ", city='" + city + '\'' +
                ", count=" + count +
                ", data=" + data +
                '}';
    }
}
