package org.ghost.springboot.demo.util.http.annotation;

import org.ghost.springboot.demo.util.http.HttpRequestBuilder;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;

@Target(TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface HttpClientConfig {
    @AliasFor("baseUrl")
    String value() default "";

    /**
     * http://a.b.c.com
     *
     * @return
     */
    String baseUrl() default "";

    /**
     * http请求数据组装
     *
     * @return
     */
    Class<HttpRequestBuilder> config() default HttpRequestBuilder.class;
}
