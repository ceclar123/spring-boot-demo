package org.ghost.springboot.demo.common.function;

public interface Func1<A, R> {
    R execute(A a);
}
