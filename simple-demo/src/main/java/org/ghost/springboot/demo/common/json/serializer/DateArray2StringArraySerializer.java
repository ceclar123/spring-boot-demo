package org.ghost.springboot.demo.common.json.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.apache.commons.lang3.ArrayUtils;
import org.ghost.springboot.demo.util.DateUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class DateArray2StringArraySerializer extends JsonSerializer<Date[]> {
    @Override
    public void serialize(Date[] value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        if (ArrayUtils.isNotEmpty(value)) {
            List<String> list = Arrays.stream(value).filter(Objects::nonNull).map(DateUtil::dateToStrYMD_HMS).collect(Collectors.toList());
            gen.writeObject(list);
        }
    }
}