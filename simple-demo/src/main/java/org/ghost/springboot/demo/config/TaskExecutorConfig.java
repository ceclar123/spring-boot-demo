package org.ghost.springboot.demo.config;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
@EnableAsync
public class TaskExecutorConfig implements AsyncConfigurer {
    private static enum Singleton {
        INSTANCE;

        private ThreadPoolTaskExecutor taskExecutor = null;

        private Singleton() {
            taskExecutor = new ThreadPoolTaskExecutor();
            taskExecutor.setCorePoolSize(5);
            taskExecutor.setMaxPoolSize(Integer.MAX_VALUE);
            taskExecutor.setQueueCapacity(25);
            taskExecutor.initialize();
        }

        public ThreadPoolTaskExecutor getInstance() {
            return taskExecutor;
        }
    }


    public AsyncTaskExecutor getAsyncTaskExecutor() {
        return Singleton.INSTANCE.getInstance();
    }


    @Override
    public Executor getAsyncExecutor() {
        return Singleton.INSTANCE.getInstance();
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return null;
    }
}
