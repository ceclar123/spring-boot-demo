package org.ghost.springboot.demo.service;

import org.ghost.springboot.demo.dto.weather.WeatherForecastRspDTO;

public interface IWeatherService {
    WeatherForecastRspDTO getWeatherForecastInfo(String city);
}
