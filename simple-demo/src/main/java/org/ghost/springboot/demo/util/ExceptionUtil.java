package org.ghost.springboot.demo.util;

import org.apache.commons.lang3.ArrayUtils;
import org.ghost.springboot.demo.common.exception.*;
import org.slf4j.Logger;

/**
 * 异常工具
 */
final public class ExceptionUtil {
    private static final String MESSAGE_NOT_EXIST = "不存在";
    private static final String MESSAGE_INVALID = "非法";
    private static final String MESSAGE_FIELD = " 字段";


    public static void throwBusinessErrorException(String methodName, String code, String message, Logger logger)
            throws RuntimeException {
        if (logger.isInfoEnabled()) {
            logger.warn("{} 业务异常: {}, {}", methodName, code, message);
        }

        throw new BusinessErrorException(code, message);
    }

    public static void throwBusinessErrorException(String methodName, String message, Logger logger) {
        if (logger.isInfoEnabled()) {
            logger.warn("{} 业务异常: {}", methodName, message);
        }

        throw new BusinessErrorException(message);
    }

    public static void throwParamErrorException(String methodName, Logger logger) {
        if (logger.isWarnEnabled()) {
            logger.warn("{} {}: {}", methodName, MESSAGE_NOT_EXIST);
        }
        throw new ParamErrorException(methodName);
    }


    public static void throwParamFieldErrorException(String methodName, String paramField, Logger logger) {
        if (logger.isWarnEnabled()) {
            logger.warn("参数字段: {} {}", paramField, MESSAGE_NOT_EXIST);
        }

        throw new ParamFieldInvalidErrorException(String.format("%s %s: %s", methodName, MESSAGE_NOT_EXIST,
                paramField));
    }

    public static void throwParamFieldInvalidErrorException(String methodName, String paramField, Logger logger) {
        if (logger.isWarnEnabled()) {
            logger.warn("参数字段: {} {}", paramField, MESSAGE_INVALID);
        }

        throw new ParamFieldInvalidErrorException(String.format("%s %s: %s", methodName, MESSAGE_INVALID, paramField));
    }

    /**
     * 抛出应用层参数错误异常
     *
     * @param methodName 方法名称
     * @param logger     日志处理器
     * @deprecated
     */
    public static void throwAppServiceParamErrorException(String methodName, Logger logger) {
        if (logger.isWarnEnabled()) {
            logger.warn("参数{}", MESSAGE_NOT_EXIST);
        }
        throw new ParamErrorException(methodName);
    }

    /**
     * 抛出应用层参数字段异常
     *
     * @param methodName 方法名称
     * @param paramField 字段
     * @param logger     日志处理器
     * @deprecated
     */
    public static void throwAppServiceParamFieldErrorException(String methodName, String paramField, Logger logger) {
        if (logger.isWarnEnabled()) {
            logger.warn("参数: {} {}", paramField, MESSAGE_NOT_EXIST);
        }
        throw new ParamFieldErrorException(String.format("%s %s: %s", methodName, MESSAGE_FIELD, paramField));
    }

    /**
     * 跑出应用程序参数字段非法异常
     *
     * @param methodName 方法名称
     * @param paramField 字段
     * @param logger     日志处理器
     * @deprecated
     */
    public static void throwAppServiceParamFieldInvalidException(String methodName, String paramField, Logger logger) {
        if (logger.isWarnEnabled()) {
            logger.warn("参数: {} {}", paramField, MESSAGE_INVALID);
        }

        throw new ParamFieldInvalidErrorException(String.format("%s %s: %s", methodName, MESSAGE_INVALID, paramField));
    }


    /**
     * 抛出service Client 参数异常
     *
     * @param message 异常信息
     * @param logger  日志处理器
     */
    public static void throwServiceClientParamException(String message, Logger logger) {
        if (logger.isWarnEnabled()) {
            logger.warn(message);
        }
        throw new ParamErrorException(message + "参数错误");
    }

    /**
     * 抛出应用层错误
     *
     * @param message 错误信息
     * @param logger  日志处理器
     * @deprecated
     */
    public static void throwAppServiceException(String message, Logger logger) {
        if (logger.isWarnEnabled()) {
            logger.warn(message);
        }
        throw new BaseException(message);
    }

    /**
     * 抛出服务客户端解析返回数据格式失败异常
     *
     * @param message 错误信息
     * @param logger  日志处理器
     */
    public static void throwServiceClientResponseParamException(String message, Logger logger) {
        if (logger.isWarnEnabled()) {
            logger.warn("解析返回数据失败: {}", message);
        }
        throw new ParamFieldErrorException(message);
    }

    public static void throwServiceClientIOException(String message, Exception e, Logger logger) {
        if (logger.isWarnEnabled()) {
            logger.warn("HTTP 网络错误: {}", message);
        }

        try {
            throw new ServiceClientIOException(message).initCause(e);
        } catch (Throwable throwable) {
            if (logger.isWarnEnabled()) {
                logger.warn("抛出ServiceClientIoException失败: {}, {}", throwable.getMessage(), throwable);
            }
        }
    }

    /**
     * 抛出存储层上游服务错误
     *
     * @param message 错误信息
     * @param logger  日志处理器
     */
    public static void throwRepositoryServiceErrorException(String message, Logger logger) {
        if (logger.isWarnEnabled()) {
            logger.warn("上游服务错误: {}", message);
        }

        throw new RepositoryUpstreamServiceErrorException(message);
    }

    /**
     * 抛出存储层上游服务错误
     *
     * @param code    错误码
     * @param message 错误信息
     * @param logger  日志处理器
     */
    public static void throwRepositoryServiceErrorException(String code, String message, Logger logger) {
        if (logger.isWarnEnabled()) {
            logger.warn("上游服务错误: 错误码: {} {}", code, message);
        }

        throw new RepositoryUpstreamServiceErrorException(code, message);
    }

    public static String getExceptionStack(Throwable e) {
        StringBuilder builder = new StringBuilder(e.toString() + e.getMessage() + "\n");
        StackTraceElement[] stackTraceElements = e.getStackTrace();
        if (ArrayUtils.isNotEmpty(stackTraceElements)) {
            for (int index = stackTraceElements.length - 1; index >= 0; --index) {
                builder.append("at [" + stackTraceElements[index].getClassName() + ",");
                builder.append(stackTraceElements[index].getFileName() + ",");
                builder.append(stackTraceElements[index].getMethodName() + ",");
                builder.append(stackTraceElements[index].getLineNumber() + "]\n");
            }
        }
        return builder.toString();
    }
}
