package org.ghost.springboot.demo.common.component;

public class RequestThread {

    private static ThreadLocal<String> uuidThreadLocal = new ThreadLocal<>();

    public static String getUUID() {
        return uuidThreadLocal.get();
    }

    public static void setUUID(String uuid) {
        uuidThreadLocal.set(uuid);
    }

    public static void removeUUID() {
        uuidThreadLocal.remove();
    }

}
