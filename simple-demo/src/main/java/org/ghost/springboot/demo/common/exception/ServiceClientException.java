package org.ghost.springboot.demo.common.exception;

import org.ghost.springboot.demo.common.constant.SystemErrorCodeEnum;

import java.io.Serializable;

public class ServiceClientException extends BaseException implements Serializable {
    public ServiceClientException(String message) {
        super(SystemErrorCodeEnum.UPSTREAM_SERVICE_ERROR.getCode(), message);
    }

    public ServiceClientException(String code, String message) {
        super(code, message);
    }
}
