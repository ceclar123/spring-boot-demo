package org.ghost.springboot.demo.test;

import org.ghost.springboot.demo.Application;
import org.ghost.springboot.demo.config.TaskExecutorConfig;
import org.ghost.springboot.demo.dto.HttpRspDTO;
import org.ghost.springboot.demo.util.*;
import org.ghost.springboot.demo.util.http.HttpAsyncClientHelper;
import org.ghost.springboot.demo.util.http.HttpClientHelper;
import org.ghost.springboot.demo.util.http.HttpRequestBuilder;
import org.ghost.springboot.demo.util.http.HttpResultCallback;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class HttpAsyncClientHelperTest {
    @Autowired
    private TaskExecutorConfig taskExecutorConfig;

    @Test
    public void test1() {
        long begin = System.currentTimeMillis();
        HttpRspDTO<String[]> req = new HttpRspDTO<String[]>();
        req.setSuccess(true);
        req.setMessage("张三三");
        req.setData(new String[]{"123", "456", "哈哈"});

        HttpRequestBuilder httpRequestBuilder = new HttpRequestBuilder()
                .build(HttpMethod.GET, "http://localhost:8391/demo/hello")
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
                .addUrlPara(req);

        HttpRspDTO<String> rspDTO = HttpAsyncClientHelper.invoke(httpRequestBuilder, HttpRspDTO.class, null, String.class);

        long end = System.currentTimeMillis();
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
        System.out.println(rspDTO);
    }

    /**
     * HttpAsyncClient批量异步回调
     */
    @Test
    public void test2() {
        long begin = System.currentTimeMillis();
        List<HttpRequestBuilder> requestBuilderList = new ArrayList<HttpRequestBuilder>();
        for (int i = 0; i < 100; i++) {
            HttpRequestBuilder httpRequestBuilder = new HttpRequestBuilder()
                    .build(HttpMethod.GET, "http://localhost:8391/demo/hello")
                    .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
            requestBuilderList.add(httpRequestBuilder);
        }

        List<HttpRspDTO<String>> resultList = new ArrayList<HttpRspDTO<String>>();
        HttpAsyncClientHelper.invoke(requestBuilderList, HttpRspDTO.class, null, new Type[]{String.class}, new HttpResultCallback<HttpRspDTO<String>>() {
            @Override
            public void handle(HttpRspDTO<String> result) {
                System.out.println("#####返回结果:" + result);
                if (result != null) {
                    resultList.add(result);
                }
            }
        });

        long end = System.currentTimeMillis();
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
        System.out.println(ListUtil.toString(resultList));
    }

    /**
     * 自定义线程池 + HttpAsyncClient
     */
    @Test
    public void test3() {
        long begin = System.currentTimeMillis();
        List<Future<HttpRspDTO<String>>> futureList = new ArrayList<Future<HttpRspDTO<String>>>();
        for (int i = 0; i < 100; i++) {
            HttpRequestBuilder httpRequestBuilder = new HttpRequestBuilder()
                    .build(HttpMethod.GET, "http://localhost:8391/demo/hello")
                    .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);

            Future<HttpRspDTO<String>> future = taskExecutorConfig.getAsyncTaskExecutor().submit(new Callable<HttpRspDTO<String>>() {
                @Override
                public HttpRspDTO<String> call() throws Exception {
                    return HttpAsyncClientHelper.invoke(httpRequestBuilder, HttpRspDTO.class, null, String.class);
                }
            });
            futureList.add(future);
        }

        List<HttpRspDTO<String>> resultList = new ArrayList<HttpRspDTO<String>>();
        for (Future<HttpRspDTO<String>> future : futureList) {
            try {
                HttpRspDTO<String> rtn = future.get();
                if (rtn != null) {
                    resultList.add(rtn);
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        long end = System.currentTimeMillis();
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
        System.out.println(ListUtil.toString(resultList));
    }

    /**
     * 自定义线程池 + HttpClient
     */
    @Test
    public void test4() {
        long begin = System.currentTimeMillis();
        List<Future<HttpRspDTO<String>>> futureList = new ArrayList<Future<HttpRspDTO<String>>>();
        for (int i = 0; i < 100; i++) {
            HttpRequestBuilder httpRequestBuilder = new HttpRequestBuilder()
                    .build(HttpMethod.GET, "http://localhost:8391/demo/hello")
                    .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);

            Future<HttpRspDTO<String>> future = taskExecutorConfig.getAsyncTaskExecutor().submit(new Callable<HttpRspDTO<String>>() {
                @Override
                public HttpRspDTO<String> call() throws Exception {
                    return HttpClientHelper.invoke(httpRequestBuilder, HttpRspDTO.class, null, String.class);
                }
            });
            futureList.add(future);
        }

        List<HttpRspDTO<String>> resultList = new ArrayList<HttpRspDTO<String>>();
        for (Future<HttpRspDTO<String>> future : futureList) {
            try {
                HttpRspDTO<String> rtn = future.get();
                if (rtn != null) {
                    resultList.add(rtn);
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        long end = System.currentTimeMillis();
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
        System.out.println(ListUtil.toString(resultList));
    }
}
