package org.ghost.springboot.demo.test;

import org.ghost.springboot.demo.dto.HttpRspDTO;
import org.ghost.springboot.demo.util.FastJsonUtil;
import org.ghost.springboot.demo.util.http.HttpAsyncClientHelper;
import org.ghost.springboot.demo.util.http.HttpRequestBuilder;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import java.lang.reflect.Type;

public class FastJsonUtilTest {
    @Test
    public void test1() {
        long begin = System.currentTimeMillis();
        HttpRequestBuilder httpRequestBuilder = new HttpRequestBuilder()
                .build(HttpMethod.GET, "http://localhost:8391/demo/hello")
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);

        HttpRspDTO<String> rspDTO = HttpAsyncClientHelper.invoke(httpRequestBuilder, HttpRspDTO.class, null, String.class);

        long end = System.currentTimeMillis();
        System.out.println("http请求结果:" + rspDTO);
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);

        begin = System.currentTimeMillis();
        String result = FastJsonUtil.toJson(rspDTO);
        end = System.currentTimeMillis();
        System.out.println("序列化结果:" + result);
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);

        begin = System.currentTimeMillis();
        Type type = ParameterizedTypeImpl.make(HttpRspDTO.class, new Type[]{String.class}, null);
        rspDTO = FastJsonUtil.parseObject(result, type);
        end = System.currentTimeMillis();
        System.out.println("反序列化结果:" + rspDTO);
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
    }
}
