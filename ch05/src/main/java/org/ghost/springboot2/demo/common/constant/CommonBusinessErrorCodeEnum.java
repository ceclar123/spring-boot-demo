package org.ghost.springboot2.demo.common.constant;

public enum CommonBusinessErrorCodeEnum implements IErrorCodeEnum {
    BUSINESS_ERROR("E400500", "业务错误");


    /**
     * 错误码
     */
    private String code;

    /**
     * 错误信息
     */
    private String message;


    /**
     * 使用错误码和错误信息构造枚举
     *
     * @param code    错误码
     * @param message 错误信息
     */
    CommonBusinessErrorCodeEnum(String code, String message) {
        this.code = code;
        this.message = message != null ? message : "";
    }

    /**
     * 获取错误码
     *
     * @return String
     */
    @Override
    public String getCode() {
        return code;
    }

    /**
     * 获取错误信息
     *
     * @return String
     */
    @Override
    public String getMessage() {
        return message;
    }
}
