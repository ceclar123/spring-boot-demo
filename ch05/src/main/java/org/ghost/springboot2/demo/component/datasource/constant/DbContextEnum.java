package org.ghost.springboot2.demo.component.datasource.constant;

import java.util.Objects;

public enum DbContextEnum {
    /**
     * 主库
     */
    MASTER(1, "MASTER", "主库"),
    /**
     * 从库
     */
    SLAVE(2, "SLAVE", "从库");

    private int code;
    private String name;
    private String desc;

    DbContextEnum(int code, String name, String desc) {
        this.code = code;
        this.name = name;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public static DbContextEnum getItemByCode(Integer code) {
        if (code != null) {
            for (DbContextEnum item : DbContextEnum.values()) {
                if (Objects.equals(item.getCode(), code)) {
                    return item;
                }
            }
        }
        return null;
    }
}
