package org.ghost.springboot2.demo.common.function;

public interface Func1<A, R> {
    R execute(A a);
}
