package org.ghost.springboot2.demo.common.json.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.common.collect.Lists;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public class LongArray2DateArrayDeserializer extends JsonDeserializer<Date[]> {
    @Override
    public Date[] deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        JsonToken currentToken = p.getCurrentToken();
        if (currentToken == JsonToken.START_ARRAY) {
            List<Long> dates = Lists.newArrayList();
            while (p.nextLongValue(0L) > 0L) {
                dates.add(p.getLongValue());
            }
            if (dates.size() > 0) {
                Date[] result = new Date[dates.size()];
                for (int i = 0; i < dates.size(); i++) {
                    result[i] = new Date(dates.get(i));
                }
                return result;
            }
        }

        return null;
    }
}