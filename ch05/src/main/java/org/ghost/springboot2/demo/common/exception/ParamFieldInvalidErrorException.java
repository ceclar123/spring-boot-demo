package org.ghost.springboot2.demo.common.exception;

import org.slf4j.Logger;

import java.io.Serializable;

public class ParamFieldInvalidErrorException extends ParamErrorException implements Serializable {
    private static final String DEFAULT_SUFFIX = "非法";

    public ParamFieldInvalidErrorException(String message) {
        super(message, DEFAULT_SUFFIX);
    }

    public ParamFieldInvalidErrorException(String methodName, String paramField, Logger logger) {
        super(String.format("%s %s: %s", methodName, DEFAULT_SUFFIX, paramField), logger);
    }
}
