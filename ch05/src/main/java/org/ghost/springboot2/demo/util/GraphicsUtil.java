package org.ghost.springboot2.demo.util;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;

public final class GraphicsUtil {
    private GraphicsUtil() {
        //
    }

    /**
     * 长字符串自动换行
     *
     * @param g
     * @param text          字符串
     * @param contentWidth  内容宽度
     * @param contentHeight 内容高度
     * @param x             x坐标
     * @param y             y坐标
     * @param font          字体
     * @param color         颜色
     */
    public static void drawStringMultiLine(final Graphics2D g, final String text, final int contentWidth, final int contentHeight, final int x, final int y, final Font font, final Color color) {
        g.setFont(font);
        g.setColor(color);
        FontMetrics fontMetrics = g.getFontMetrics();
        if (fontMetrics.stringWidth(text) < contentWidth) {
            g.drawString(text, x, y);
        } else {
            int fontHeight = fontMetrics.getAscent() + fontMetrics.getDescent();
            char[] chars = text.toCharArray();

            //拆成多行
            java.util.List<String> lines = new ArrayList<String>();
            int fromIndex = 0;
            int strWidth = 0;
            int strLength = chars.length;
            //超过宽度,自动换行
            for (int i = 0; i < strLength; i++) {
                strWidth += fontMetrics.charWidth(chars[i]);
                if (strWidth > contentWidth) {
                    lines.add(text.substring(fromIndex, i));
                    strWidth = 0;
                    fromIndex = i;
                }
            }

            // 加上最后一行
            if (fromIndex < text.length()) {
                lines.add(text.substring(fromIndex, text.length()));
            }
            int newY = y;
            for (String line : lines) {
                //超过高度直接跳过
                if (y + fontMetrics.getAscent() > contentHeight) {
                    break;
                }
                g.drawString(line, x, newY + fontMetrics.getAscent());
                newY += fontHeight;
            }
        }
    }

    /**
     * 长字符串自动换行居中
     *
     * @param g
     * @param text   字符串
     * @param width  总宽度
     * @param height 总长度
     * @param x      x坐标(最小边距)
     * @param y      y坐标
     * @param font   字体
     * @param color  颜色
     */
    public static Point2D.Float drawStringMultiLineCenter(final Graphics2D g, final String text, final float width, final float height, final float x, float y, final Font font, final Color color) {
        final float maxContentWidth = width - 2 * x;
        g.setFont(font);
        g.setColor(color);
        FontMetrics fontMetrics = g.getFontMetrics();
        if (fontMetrics.stringWidth(text) < maxContentWidth) {
            float newX = getHCenter(width, fontMetrics, text);
            g.drawString(text, newX, y);
        } else {
            int fontHeight = fontMetrics.getAscent() + fontMetrics.getDescent();
            char[] chars = text.toCharArray();

            //拆成多行
            java.util.List<String> lines = new ArrayList<String>();
            int fromIndex = 0;
            int strWidth = 0;
            int strLength = chars.length;
            //超过宽度,自动换行
            for (int i = 0; i < strLength; i++) {
                strWidth += fontMetrics.charWidth(chars[i]);
                if (strWidth > maxContentWidth) {
                    lines.add(text.substring(fromIndex, i));
                    strWidth = 0;
                    fromIndex = i;
                }
            }

            // 加上最后一行
            if (fromIndex < text.length()) {
                lines.add(text.substring(fromIndex, text.length()));
            }
            for (String line : lines) {
                //超过高度直接跳过
                if (y + fontMetrics.getAscent() > height) {
                    break;
                }
                float newX = getHCenter(width, fontMetrics, line);
                g.drawString(line, newX, y + fontMetrics.getAscent());
                y += fontHeight;
            }
        }

        return new Point2D.Float(x, y);
    }

    private static float getHCenter(float lineWidth, FontMetrics fontMetrics, String text) {
        int strWidth = fontMetrics.stringWidth(text);
        return (lineWidth - strWidth) / 2;
    }
}
