package org.ghost.springboot2.demo.component.datasource.properties;

import java.io.Serializable;

public class DataSourceConfigEntity implements Serializable {
    private static final long serialVersionUID = 3877261814895232151L;

    private String url;
    private String username;
    private String password;
    private Integer maxPoolSize = 10;
    private Integer minIdle = 1;
    private String driverClassName;
    private Boolean master = false;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(Integer maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public Integer getMinIdle() {
        return minIdle;
    }

    public void setMinIdle(Integer minIdle) {
        this.minIdle = minIdle;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public Boolean getMaster() {
        return master;
    }

    public void setMaster(Boolean master) {
        this.master = master;
    }

    @Override
    public String toString() {
        return "DataSourceConfigEntity{" +
                "url='" + url + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", maxPoolSize=" + maxPoolSize +
                ", minIdle=" + minIdle +
                ", driverClassName='" + driverClassName + '\'' +
                ", master=" + master +
                '}';
    }
}
