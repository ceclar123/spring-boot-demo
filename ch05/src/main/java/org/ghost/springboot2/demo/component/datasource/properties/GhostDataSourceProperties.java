package org.ghost.springboot2.demo.component.datasource.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@ConfigurationProperties(
        prefix = "ghost.datasources"
)
@Component
public class GhostDataSourceProperties implements Serializable {
    private static final long serialVersionUID = 1834827130124783132L;

    private String validationQuery = "SELECT 1";
    private Integer idleTimeout = 86400000;
    private Integer maxLifeTime = 86400000;

    private DataSourceConfigEntity master;
    private DataSourceConfigEntity slave;

    public String getValidationQuery() {
        return validationQuery;
    }

    public void setValidationQuery(String validationQuery) {
        this.validationQuery = validationQuery;
    }

    public Integer getIdleTimeout() {
        return idleTimeout;
    }

    public void setIdleTimeout(Integer idleTimeout) {
        this.idleTimeout = idleTimeout;
    }

    public Integer getMaxLifeTime() {
        return maxLifeTime;
    }

    public void setMaxLifeTime(Integer maxLifeTime) {
        this.maxLifeTime = maxLifeTime;
    }

    public DataSourceConfigEntity getMaster() {
        return master;
    }

    public void setMaster(DataSourceConfigEntity master) {
        this.master = master;
    }

    public DataSourceConfigEntity getSlave() {
        return slave;
    }

    public void setSlave(DataSourceConfigEntity slave) {
        this.slave = slave;
    }

    @Override
    public String toString() {
        return "GhostDataSourceProperties{" +
                "validationQuery='" + validationQuery + '\'' +
                ", idleTimeout=" + idleTimeout +
                ", maxLifeTime=" + maxLifeTime +
                ", master=" + master +
                ", slave=" + slave +
                '}';
    }
}
