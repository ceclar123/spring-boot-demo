package org.ghost.springboot2.demo.service;

import java.util.Map;

public interface IDemoService {
    Map<String, Object> getUserInfo(Long id);
}
