package org.ghost.springboot2.demo.service.impl;

import org.ghost.springboot2.demo.service.IDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class DemoServiceImpl implements IDemoService {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public Map<String, Object> getUserInfo(Long id) {
        String sql = "SELECT * FROM t_user WHERE id = :id";
        return jdbcTemplate.queryForMap(sql, new MapSqlParameterSource("id", id));
    }
}
