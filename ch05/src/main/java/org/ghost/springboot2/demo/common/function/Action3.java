package org.ghost.springboot2.demo.common.function;

public interface Action3<A, B, C> {
    void execute(A a, B b, C c);
}
