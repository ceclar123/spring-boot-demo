package org.ghost.springboot2.demo.service;

import java.util.Map;

public interface ITestService {
    Map<String, Object> getUserInfo(Long id);

    int updateNameById(String name, Long id);

    /**
     * 相同不管,不同就更新(测试写中有图)
     *
     * @param name
     * @param age
     * @param id
     * @return
     */
    int updateInfoById(String name, Integer age, Long id);
}
