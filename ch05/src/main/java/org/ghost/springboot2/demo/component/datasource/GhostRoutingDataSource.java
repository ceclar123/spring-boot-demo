package org.ghost.springboot2.demo.component.datasource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

public class GhostRoutingDataSource extends AbstractRoutingDataSource {
    @Override
    protected Object determineCurrentLookupKey() {
        return DbContext.getDbContext();
    }
}
