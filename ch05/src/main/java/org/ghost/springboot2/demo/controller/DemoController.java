package org.ghost.springboot2.demo.controller;

import org.ghost.springboot2.demo.dto.RspDTO;
import org.ghost.springboot2.demo.service.ITestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "demo")
public class DemoController extends BaseController {
    private final static Logger logger = LoggerFactory.getLogger(DemoController.class);

    @Autowired
    private ITestService testService;

    @RequestMapping(value = "userInfo/id/{id}", method = RequestMethod.GET)
    public RspDTO get1(@PathVariable Long id) {
        return this.render(testService.getUserInfo(id));
    }

    @RequestMapping(value = "userInfo/id/{id}", method = RequestMethod.PUT)
    public RspDTO get2(@PathVariable Long id, @RequestParam String name) {
        return this.render(testService.updateNameById(name, id));
    }

    @RequestMapping(value = "userInfo/id/{id}", method = RequestMethod.PATCH)
    public RspDTO get2(@PathVariable Long id, @RequestParam String name, @RequestParam Integer age) {
        return this.render(testService.updateInfoById(name, age, id));
    }

}

