package org.ghost.springboot2.demo.component.datasource;

import org.ghost.springboot2.demo.component.datasource.constant.DbContextEnum;

public class DbContext {
    private static final ThreadLocal<DbContextEnum> CONTEXT = new ThreadLocal<DbContextEnum>();

    public static void setDbContext(DbContextEnum dbContextEnum) {
        CONTEXT.set(dbContextEnum);
    }

    public static DbContextEnum getDbContext() {
        return CONTEXT.get();
    }

    public static void switchToMaster() {
        setDbContext(DbContextEnum.MASTER);
    }

    public static void switchToSlave() {
        setDbContext(DbContextEnum.SLAVE);
    }

    public static void remove() {
        CONTEXT.remove();
    }
}
