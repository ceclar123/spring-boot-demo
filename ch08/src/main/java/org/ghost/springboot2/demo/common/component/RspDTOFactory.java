package org.ghost.springboot2.demo.common.component;

import org.ghost.springboot2.demo.common.constant.SystemErrorCodeEnum;
import org.ghost.springboot2.demo.dto.RspDTO;

public class RspDTOFactory {
    private static final RspDTO APP_NOT_EXIST_RSP_DTO;
    private static final RspDTO NOT_PERMISSION_RSP_DTO;
    private static final RspDTO PARAM_ERROR_RSP_DTO;

    static {
        APP_NOT_EXIST_RSP_DTO = new RspDTO(SystemErrorCodeEnum.APP_KEY_NOT_EXIST_ERROR.getCode(), SystemErrorCodeEnum
                .APP_KEY_NOT_EXIST_ERROR.getMessage());
        NOT_PERMISSION_RSP_DTO = new RspDTO(SystemErrorCodeEnum.NO_PERMISSION_ERROR.getCode(), SystemErrorCodeEnum
                .NO_PERMISSION_ERROR.getMessage());
        PARAM_ERROR_RSP_DTO = new RspDTO(SystemErrorCodeEnum.PARAM_ERROR.getCode(), SystemErrorCodeEnum.PARAM_ERROR
                .getMessage());
    }

    /**
     * 获取AppKey 不存在DTO
     *
     * @return RspDTO
     */
    public static RspDTO getAppKeyNotExistRspDTO() {
        return APP_NOT_EXIST_RSP_DTO;
    }

    /**
     * 没有访问权限
     *
     * @return RspDTO
     */
    public static RspDTO getNoPermissionRspErrorDTO() {
        return NOT_PERMISSION_RSP_DTO;
    }

    /**
     * 参数错误
     *
     * @return RspDTO
     */
    public static RspDTO getParamErrorRspDTO() {
        return PARAM_ERROR_RSP_DTO;
    }
}
