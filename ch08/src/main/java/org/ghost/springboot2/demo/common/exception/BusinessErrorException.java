package org.ghost.springboot2.demo.common.exception;

import org.ghost.springboot2.demo.common.constant.CommonBusinessErrorCodeEnum;
import org.ghost.springboot2.demo.common.constant.IErrorCodeEnum;
import org.slf4j.Logger;

import java.io.Serializable;


public class BusinessErrorException extends BaseException implements Serializable {
    public BusinessErrorException(String message) {
        this(CommonBusinessErrorCodeEnum.BUSINESS_ERROR.getCode(), message);
    }

    public BusinessErrorException(String methodName, IErrorCodeEnum errorCodeEnum, Logger logger) {
        super(methodName, errorCodeEnum, logger);
    }

    public BusinessErrorException(String methodName, String message, Logger logger) {
        super(methodName, CommonBusinessErrorCodeEnum.BUSINESS_ERROR.getCode(), message, logger);
    }

    public BusinessErrorException(String code, String message) {
        super(code, message);
    }
}
