package org.ghost.springboot2.demo.service.impl;

import org.ghost.springboot2.demo.config.TaskExecutorConfig;
import org.ghost.springboot2.demo.service.ITestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

@Service
public class TestServiceImpl implements ITestService {
    private final static Logger logger = LoggerFactory.getLogger(TestServiceImpl.class);

    @Autowired
    private TaskExecutorConfig taskExecutorConfig;

    @Async("asyncTaskExecutor")
    @Override
    public Future<String> getAsync1() {
        logger.debug("debug日志getAsync1");
        logger.info("info日志getAsync1");
        logger.warn("warn日志getAsync1");
        logger.error("error日志getAsync1");
        return new AsyncResult<String>("Hello张三");
    }

    @Override
    public Future<String> getAsync2() {
        logger.debug("debug日志getAsync2");
        logger.info("info日志getAsync2");
        logger.warn("warn日志getAsync2");
        logger.error("error日志getAsync2");
        return taskExecutorConfig.getAsyncTaskExecutor().submit(() -> "Hello张三");
    }

    @Async("asyncTaskExecutor")
    @Override
    public void process1() {
        logger.debug("debug日志process1");
        logger.info("info日志process1");
        logger.warn("warn日志process1");
        logger.error("error日志process1");
    }

    @Override
    public void process2() {
        logger.debug("debug日志process2");
        logger.info("info日志process2");
        logger.warn("warn日志process2");
        logger.error("error日志process2");
        taskExecutorConfig.getAsyncExecutor().execute(() -> System.out.println("Hello张三"));
    }
}
