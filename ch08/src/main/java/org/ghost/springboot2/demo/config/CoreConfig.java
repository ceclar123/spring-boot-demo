package org.ghost.springboot2.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;

@Configuration
public class CoreConfig {
    @Bean(value = "asyncTaskExecutor")
    public TaskExecutor getTaskExecutor(TaskExecutorConfig taskExecutorConfig) {
        return taskExecutorConfig.getAsyncTaskExecutor();
    }
}
