package org.ghost.springboot2.demo.common.converter;

import org.apache.commons.lang3.StringUtils;
import org.ghost.springboot2.demo.util.DateUtil;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class String2ListDateConverter implements Converter<String, List<Date>> {
    @Override
    public List<Date> convert(String source) {
        if (StringUtils.isNotBlank(source)) {
            String[] data = source.split("\\,");
            if (data != null && data.length > 0) {
                List<Date> list = new ArrayList<>();
                for (String item : data) {
                    try {
                        //先判断是否时间戳
                        list.add(new Date(Long.valueOf(source)));
                    } catch (Exception e) {
                        list.add(DateUtil.strToDateYMD_HMS(source));
                    }
                }

                return list;
            }
        }

        return null;
    }
}