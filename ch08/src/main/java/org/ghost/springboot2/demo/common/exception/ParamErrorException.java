package org.ghost.springboot2.demo.common.exception;

import org.ghost.springboot2.demo.common.constant.SystemErrorCodeEnum;
import org.slf4j.Logger;

import java.io.Serializable;


public class ParamErrorException extends BaseException implements Serializable {
    private static final String DEFAULT_SUFFIX = "参数不存在";

    public ParamErrorException(String message) {
        this(message, DEFAULT_SUFFIX);
    }

    public ParamErrorException(String methodName, Logger logger) {
        this(methodName, "", logger);
    }

    public ParamErrorException(String methodName, String param, Logger logger) {
        super(methodName, SystemErrorCodeEnum.PARAM_ERROR.getCode(), String.format("%s %s%s", methodName, param,
                DEFAULT_SUFFIX), logger);
    }

    public ParamErrorException(String message, String suffix) {
        this(SystemErrorCodeEnum.PARAM_ERROR.getCode(), message, suffix);
    }

    public ParamErrorException(String code, String message, String suffix) {
        super(code, message + suffix);
    }
}
