# spring-boot-demo

## 介绍日志配置文件的使用
### 1、logback-spring.xml配置
### 2、启动参数
- -Dspring.profiles.active=dev -Dspring.config.location=classpath:/env/dev/
- -Dspring.profiles.active=test -Dspring.config.location=classpath:/env/test/
- -Dspring.profiles.active=prod -Dspring.config.location=classpath:/env/prod/
>主要是使不同环境的配置放在不同的目录

### property文件加载
<pre>
@Component
@ConfigurationProperties(prefix = "person")
@PropertySource(value = {"${spring.config.location}person.properties"}, encoding = "UTF-8")
public class PersonProperties implements Serializable {
    //
}
</pre>