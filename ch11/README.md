# spring-boot-demo
## RabbitMQ使用
### 1、exchange类型
- 1、fanout
    - (1)、把所有发送到该Exchange的消息投递到所有与它绑定的队列中，无须对消息的routingkey进行匹配操作
- 2、direct
    - (1)、把消息投递到那些bindingKey与routingKey完全匹配的队列中。  
    - (2)、发送消息需要exchangeName与routingKey
    ```
      RabbitTemplate.convertAndSend(String exchange, String routingKey, final Object object); 
    ```
    - (3)、exchange与queue的绑定
    ```
    @RabbitListener(
    	bindings = @QueueBinding(
    		exchange = @Exchange(value = "exchangeName", type = ExchangeTypes.TOPIC,
    			durable = "true", autoDelete = "false"),
    		value = @Queue(value = "queueName", durable = "true",
    			autoDelete = "false"),
    		key = "routingKey"
    	)
    )
    ```
- 3、topic
    - (1)、与direct类似，将消息路由到bindingKey与routingKey模式匹配的队列中。
    - (2)、其中'*'表示匹配一个单词， '#'则表示匹配没有或者多个单词
- 4、header(听说性能不是很好,用得不多)
    - (1)、此类型的exchange和以上三个都不一样，其路由的规则是根据header来判断
    - (2)、绑定增加参数举例:
    ```
    @RabbitListener(
    	bindings = @QueueBinding(
    		exchange = @Exchange(value = "exchangeName", type = ExchangeTypes.TOPIC,
    			durable = "true", autoDelete = "false"),
    		value = @Queue(value = "queueName", durable = "true",
    			autoDelete = "false"),
    		key = "routingKey",
    		arguments = {@Argument(name = "x-match", value = "any"), @Argument(name = "helloKey", value = "helloValue", type = "java.lang.String")}
    	)
    )
    ```
    - (3)、x-match为特殊参数，all表示要匹配所有的header，any表示只要匹配其中的一个header即可
    ```
    RabbitTemplate.convertAndSend(String exchange, String routingKey, final Object object
    	, new MessagePostProcessor() {
                        public Message postProcessMessage(Message message) throws AmqpException {
                            if (message != null) {
                                message.getMessageProperties().setHeader("helloKey", "helloValue");
                            }
                            return message;
                        }
    
                        public Message postProcessMessage(Message message, Correlation correlation) {
                            if (message != null) {
                                message.getMessageProperties().setHeader("helloKey", "helloValue");
                            }
                            return message;
                        }
                    }); 
    ```