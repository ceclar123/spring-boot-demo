package org.ghost.springboot2.demo.config.annotation;

import org.ghost.springboot2.demo.common.constant.RabbitMqConstant;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Administrator
 */
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@RabbitListener(
        bindings = @QueueBinding(
                value = @Queue,
                exchange = @Exchange(value = RabbitMqConstant.DEFAULT_EXCHANGE, type = ExchangeTypes.TOPIC,
                        durable = RabbitMqConstant.FALSE_CONSTANT, autoDelete = RabbitMqConstant.TRUE_CONSTANT),
                key = RabbitMqConstant.DEFAULT_KEY,
                ignoreDeclarationExceptions = "true",
                arguments = @Argument(name = "x-message-ttl", value = "10000", type = "java.lang.Integer")
        )
)
public @interface MyListener {

}
