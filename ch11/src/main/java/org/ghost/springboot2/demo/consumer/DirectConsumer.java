package org.ghost.springboot2.demo.consumer;

import org.ghost.springboot2.demo.common.constant.RabbitMqConstant;
import org.ghost.springboot2.demo.common.constant.SpringProfileConstant;
import org.ghost.springboot2.demo.dto.OrderMessageDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Profile(SpringProfileConstant.PROFILE_DIRECT_LISTENER)
public class DirectConsumer {

    @RabbitListener(
            bindings = @QueueBinding(
                    exchange = @Exchange(value = RabbitMqConstant.DIRECT_EXCHANGE, type = ExchangeTypes.TOPIC,
                            durable = RabbitMqConstant.FALSE_CONSTANT, autoDelete = RabbitMqConstant.TRUE_CONSTANT),
                    value = @Queue(value = RabbitMqConstant.DIRECT_QUEUE, durable = RabbitMqConstant.FALSE_CONSTANT,
                            autoDelete = RabbitMqConstant.TRUE_CONSTANT),
                    key = RabbitMqConstant.DIRECT_KEY
            ),
            containerFactory = "directRabbitListenerContainerFactory"
    )
    public void process(OrderMessageDTO event) {
        log.info("direct container receive message:{} ", event);
    }
}
