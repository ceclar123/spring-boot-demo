package org.ghost.springboot2.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Administrator
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderMessageDTO {
    private String orderId;
    private String buyer;
    private Integer num;
}
