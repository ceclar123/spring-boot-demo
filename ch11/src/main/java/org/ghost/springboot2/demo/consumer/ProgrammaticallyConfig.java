package org.ghost.springboot2.demo.consumer;

import org.ghost.springboot2.demo.common.constant.RabbitMqConstant;
import org.ghost.springboot2.demo.common.constant.SpringProfileConstant;
import org.ghost.springboot2.demo.dto.OrderMessageDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;


/**
 * @author Administrator
 */
@Slf4j
@Configuration
@Profile(SpringProfileConstant.PROFILE_PROGRAMMATICALLY_CONFIG)
public class ProgrammaticallyConfig {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Bean
    @Qualifier(RabbitMqConstant.PROGRAMMATICALLY_QUEUE)
    public Queue queue() {
        return new Queue(RabbitMqConstant.PROGRAMMATICALLY_QUEUE, false, false, true);
    }

    /**
     * 消费者和生产者都可以声明，交换器这种一般经常创建，可以手动创建。需要注意对于没有路由到队列的消息会被丢弃。
     *
     * @return
     */
    @Bean
    @Qualifier(RabbitMqConstant.PROGRAMMATICALLY_EXCHANGE)
    public TopicExchange exchange() {
        return new TopicExchange(RabbitMqConstant.PROGRAMMATICALLY_EXCHANGE, false, true);
    }

    @Bean
    public Binding binding(@Qualifier(RabbitMqConstant.PROGRAMMATICALLY_EXCHANGE) TopicExchange exchange,
                           @Qualifier(RabbitMqConstant.PROGRAMMATICALLY_QUEUE) Queue queue) {
        return BindingBuilder.bind(queue).to(exchange).with(RabbitMqConstant.PROGRAMMATICALLY_KEY);
    }

    /**
     * 声明简单的消费者，接收到的都是原始的{@link Message}
     *
     * @param connectionFactory
     * @return
     */
    @Bean
    public SimpleMessageListenerContainer simpleContainer(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setMessageListener(message -> log.info("simple receiver,message:{}", message));
        container.setQueueNames(RabbitMqConstant.PROGRAMMATICALLY_QUEUE);
        return container;
    }

    /**
     * 声明带Channel的消费者,比如要手动确认消息时就会用到这个
     *
     * @param connectionFactory
     * @return
     */
    @Bean
    public SimpleMessageListenerContainer simpleContainer2(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setMessageListener((ChannelAwareMessageListener) (message, channel) -> {
            log.info("channel receiver,message:{}", message);
        });
        container.setQueueNames(RabbitMqConstant.PROGRAMMATICALLY_QUEUE);
        return container;
    }

    /**
     * 声明采用MessageListenerAdapter的消费者，Convert在MessageListenerAdapter中设置
     *
     * @param connectionFactory
     * @return
     */
    @Bean
    public SimpleMessageListenerContainer adaptContainer(ConnectionFactory connectionFactory, MessageListenerAdapter messageConverter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setMessageListener(messageConverter);
        container.setQueueNames(RabbitMqConstant.PROGRAMMATICALLY_QUEUE);
        return container;
    }

    @Bean
    public MessageListenerAdapter listenerAdapter(MessageConverter messageConverter, Receiver receiver) {
        final MessageListenerAdapter process = new MessageListenerAdapter(receiver, "process");
        //注意显示声明时，MessageConvert不是在Container中声明的
        process.setMessageConverter(messageConverter);
        return process;
    }

    @Component
    public static class Receiver {

        public void process(OrderMessageDTO message) {
            log.info("adapter receive param:{message = [" + message + "]} info:");
        }
    }
}
