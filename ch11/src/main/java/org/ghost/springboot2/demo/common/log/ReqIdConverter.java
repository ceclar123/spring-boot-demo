package org.ghost.springboot2.demo.common.log;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import org.ghost.springboot2.demo.common.component.RequestContext;

/**
 * @author Administrator
 */
public class ReqIdConverter extends ClassicConverter {
    @Override
    public String convert(ILoggingEvent event) {
        String id = RequestContext.getContext().getId();
        return id == null ? "---" : id;
    }
}
