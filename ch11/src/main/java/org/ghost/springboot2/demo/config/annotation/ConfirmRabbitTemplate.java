package org.ghost.springboot2.demo.config.annotation;

import org.springframework.beans.factory.annotation.Qualifier;

import java.lang.annotation.*;


/**
 * @author Administrator
 */
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Qualifier
public @interface ConfirmRabbitTemplate {

}
