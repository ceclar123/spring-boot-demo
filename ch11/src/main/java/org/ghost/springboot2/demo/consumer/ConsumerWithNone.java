package org.ghost.springboot2.demo.consumer;

import org.ghost.springboot2.demo.common.constant.RabbitMqConstant;
import org.ghost.springboot2.demo.common.constant.SpringProfileConstant;
import org.ghost.springboot2.demo.dto.OrderMessageDTO;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * @author Administrator
 */
@Component
@Slf4j
@Profile(SpringProfileConstant.PROFILE_CONSUMER_NONE)
public class ConsumerWithNone {

    @RabbitListener(
            bindings = @QueueBinding(
                    exchange = @Exchange(value = RabbitMqConstant.NONE_EXCHANGE, type = ExchangeTypes.TOPIC),
                    value = @Queue(value = RabbitMqConstant.NONE_QUEUE, durable = RabbitMqConstant.TRUE_CONSTANT),
                    key = RabbitMqConstant.NONE_KEY
            ),
            containerFactory = "containerWithNone"
    )
    @SneakyThrows
    public void process(@Headers Map<String, Object> headers, @Payload OrderMessageDTO msg) {
        Thread.sleep(1 * 1000);
        log.info("none consumer receive message:{headers = [" + headers + "], msg = [" + msg + "]}");
    }
}
