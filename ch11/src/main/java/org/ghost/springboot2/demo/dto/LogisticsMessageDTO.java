package org.ghost.springboot2.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author Administrator
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogisticsMessageDTO {
    private String orderId;
    private String logisticsNum;
}
