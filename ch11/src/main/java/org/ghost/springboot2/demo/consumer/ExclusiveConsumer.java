package org.ghost.springboot2.demo.consumer;

import org.ghost.springboot2.demo.common.constant.SpringProfileConstant;
import org.ghost.springboot2.demo.config.annotation.MyListener;
import org.ghost.springboot2.demo.dto.OrderMessageDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * @Author: chenfangzhi
 * @Description: 测试私有队列的功能, 可以在RabbitMq管理平台看到spring.gen开头的队列，
 * 绑定到了{@link RabbitMqConstant#DEFAULT_EXCHANGE}，详见{@link MyListener}
 * @Date: 2018/9/22-23:13
 * @ModifiedBy:
 */
@Slf4j
@Component
@Profile(SpringProfileConstant.PROFILE_CONSUMER_EXCLUSIVE)
public class ExclusiveConsumer {

    @MyListener
    public void process(OrderMessageDTO message) {
        log.info("exclusive queue message:{}", message);
    }

    @MyListener
    public void process2(OrderMessageDTO message) {
        log.info("exclusive queue2 receive message:{}", message);
    }
}
