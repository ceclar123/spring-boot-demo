package org.ghost.springboot2.demo.consumer;

import org.ghost.springboot2.demo.common.constant.RabbitMqConstant;
import org.ghost.springboot2.demo.common.constant.SpringProfileConstant;
import org.ghost.springboot2.demo.dto.LogisticsMessageDTO;
import org.ghost.springboot2.demo.dto.OrderMessageDTO;
import org.ghost.springboot2.demo.dto.StorageMessageDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RabbitListener(
        bindings = @QueueBinding(
                exchange = @Exchange(value = RabbitMqConstant.MULTIPART_HANDLE_EXCHANGE, type = ExchangeTypes.TOPIC,
                        durable = RabbitMqConstant.FALSE_CONSTANT, autoDelete = RabbitMqConstant.TRUE_CONSTANT),
                value = @Queue(value = RabbitMqConstant.MULTIPART_HANDLE_QUEUE, durable = RabbitMqConstant.FALSE_CONSTANT,
                        autoDelete = RabbitMqConstant.TRUE_CONSTANT),
                key = RabbitMqConstant.MULTIPART_HANDLE_KEY
        )
)
@Profile(SpringProfileConstant.PROFILE_MULTIPART)
public class MultipartConsumer {

    /**
     * RabbitHandler用于有多个方法时但是参数类型不能一样，否则会报错
     *
     * @param msg
     */
    @RabbitHandler
    public void process(OrderMessageDTO msg) {
        log.info("*****param msg1:{}", msg);
    }

    @RabbitHandler
    public void process(StorageMessageDTO msg) {
        log.info("*****param msg2:{}", msg);
    }

    /**
     * 下面的多个消费者，消费的类型不一样没事，不会被调用，但是如果缺了相应消息的处理Handler则会报错
     *
     * @param msg
     */
    @RabbitHandler
    public void process(LogisticsMessageDTO msg) {
        log.info("*****param msg3:{}", msg);
    }

    @RabbitHandler
    public void process(Object msg) {
        log.info("*****param msg4:{}", msg);
    }


}
