package org.ghost.springboot2.demo.common.component;


/**
 * @author Administrator
 */
public interface IRequestContext {
    String getRemoteIp();
}
