package org.ghost.springboot2.demo.common.constant;

/**
 * @author Administrator
 */
public interface SpringProfileConstant {
    /**
     * 手动设置的方式
     */
    String PROFILE_OLD_CONFIG = "old.config";
    /**
     * 注册channel监听器的方式
     */
    String PROFILE_CHANNEL_LISTENER = "channel.listener";
    /**
     * 发送者确认模式
     */
    String PROFILE_PUBLISHER_CONFIRM = "publisher.confirm";
    /**
     * 消费者确认模式
     */
    String PROFILE_CONSUMER_CONFIRM = "consumer.confirm";

    /**
     * 监听消费者错误事件
     */
    String PROFILE_CONSUMER_LISTENER = "consumer.error";
    /**
     * 手动注册队列等等
     */
    String PROFILE_PROGRAMMATICALLY_CONFIG = "programmatically.config";
    /**
     * direct监听容器
     */
    String PROFILE_DIRECT_LISTENER = "direct.listener";
    /**
     * 多种消息类型
     */
    String PROFILE_MULTIPART = "multipart";

    String PROFILE_REPLY = "reply.proflle";
    /**
     * 自动确认的消费者
     */
    String PROFILE_CONSUMER_NONE = "consumer.none";
    /**
     * 消息者异常时处理
     */
    String PROFILE_CONSUMER_REQUEUE = "consumer.requeue";
    /**
     * 排他消费者
     */
    String PROFILE_CONSUMER_EXCLUSIVE = "consumer.exclusive";
    /**
     * 带重试机制的Consumer
     */
    String PROFILE_CONSUMER_RETRY = "consumer.retry";
    /**
     * 死信队列
     */
    String PROFILE_DEAD_LETTER = "dead.letter";
}
