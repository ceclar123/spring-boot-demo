package org.ghost.springboot2.demo.consumer;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.ghost.springboot2.demo.common.constant.RabbitMqConstant;
import org.ghost.springboot2.demo.common.constant.RabbitMqDefaultConstant;
import org.ghost.springboot2.demo.common.constant.SpringProfileConstant;
import org.ghost.springboot2.demo.dto.OrderMessageDTO;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * @author Administrator
 */
@Component
@Profile(SpringProfileConstant.PROFILE_DEAD_LETTER)
@Slf4j
public class DeadLetterConsumer {

    /**
     * 下面的在声明了一个队列，给了{@linkplain RabbitMqConstant#DEAD_LETTER_EXCHANGE 死信队列的交换器},并且给了一个
     * 消息的{@linkplain RabbitMqConstant#DEAD_LETTER_KEY RouteKey}.key默认是监听队列的名字，可以不提供，如果成功，
     * 可以在web控制台看到queue的Features有DLX(死信队列交换器)和DLK(死信队列绑定)。
     * 下面的消费者失败三次后发送nack，消息转入死信队列。
     * 这里要说一个我踩的坑，{@link Queue}中有个参数{@link Queue#arguments}，
     * 我当时把死信队列的参数设置到了{@link QueueBinding#arguments()}，所以导致绑定死信队列不成功。
     * 最后通过在web控制台观察发现的。
     *
     * @param headers
     * @param msg
     */
    @RabbitListener(
            bindings = @QueueBinding(
                    exchange = @Exchange(value = RabbitMqConstant.DEFAULT_EXCHANGE, type = ExchangeTypes.TOPIC,
                            durable = RabbitMqConstant.FALSE_CONSTANT, autoDelete = RabbitMqConstant.TRUE_CONSTANT),
                    value = @Queue(value = RabbitMqConstant.DEFAULT_QUEUE, durable = RabbitMqConstant.FALSE_CONSTANT,
                            autoDelete = RabbitMqConstant.TRUE_CONSTANT, arguments = {
                            @Argument(name = RabbitMqDefaultConstant.DEAD_LETTER_EXCHANGE, value = RabbitMqConstant.DEAD_EXCHANGE),
                            @Argument(name = RabbitMqDefaultConstant.DEAD_LETTER_KEY, value = RabbitMqConstant.DEAD_KEY)
                    }),
                    key = RabbitMqConstant.DEFAULT_KEY
            ))
    @SneakyThrows
    public void process(@Headers Map<String, Object> headers, @Payload OrderMessageDTO msg) {
        log.info("retry consumer receive message:{headers = [" + headers + "], msg = [" + msg + "]}");
        throw new RuntimeException();
    }

    /**
     * 声明死信队列并监听,key采用了#，表示监听所有消息
     *
     * @param headers
     * @param msg
     */
    @RabbitListener(
            bindings = @QueueBinding(
                    exchange = @Exchange(value = RabbitMqConstant.DEAD_EXCHANGE),
                    value = @Queue(value = RabbitMqConstant.DEAD_QUEUE, durable = RabbitMqConstant.TRUE_CONSTANT),
                    key = "#"
            )
    )
    public void deadListener(@Headers Map<String, Object> headers, @Payload OrderMessageDTO msg) {
        log.info("dead consumer receive message:{headers = [" + headers + "], msg = [" + msg + "]}");
    }
}
