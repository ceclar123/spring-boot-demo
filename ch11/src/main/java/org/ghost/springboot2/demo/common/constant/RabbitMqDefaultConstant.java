package org.ghost.springboot2.demo.common.constant;

/**
 * @author Administrator
 */
public interface RabbitMqDefaultConstant {
    /**
     * 死信队列参数
     */
    String DEAD_LETTER_EXCHANGE = "x-dead-letter-exchange";
    String DEAD_LETTER_KEY = "x-dead-letter-routing-key";
}
