package org.ghost.springboot2.demo.consumer;

/**
 * @Author: chenfangzhi
 * @Description:
 * @Date: 2018/9/18-21:07
 * @ModifiedBy:
 */

import lombok.extern.slf4j.Slf4j;
import org.ghost.springboot2.demo.common.constant.RabbitMqConstant;
import org.ghost.springboot2.demo.dto.OrderMessageDTO;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Slf4j
@ConditionalOnMissingBean(value = {RequeueConsumer.class, ConsumerWithRetry.class, ExclusiveConsumer.class, DeadLetterConsumer.class})
public class BasicConsumer {
//    @RabbitListener(
//            bindings = @QueueBinding(
//                    exchange = @Exchange(value = RabbitMqConstant.DEFAULT_EXCHANGE, type = ExchangeTypes.TOPIC,
//                            durable = RabbitMqConstant.FALSE_CONSTANT, autoDelete = RabbitMqConstant.TRUE_CONSTANT),
//                    value = @Queue(value = RabbitMqConstant.DEFAULT_QUEUE, durable = RabbitMqConstant.FALSE_CONSTANT,
//                            autoDelete = RabbitMqConstant.TRUE_CONSTANT),
//                    key = RabbitMqConstant.DEFAULT_KEY
//            ),
//            containerFactory = RabbitMqConstant.CONTAINER_DEFAULT,
//            //指定消费者的线程数量,一个线程会打开一个Channel，一个队列上的消息只会被消费一次（不考虑消息重新入队列的情况）
//            concurrency = "1-2"
//    )
//    public void process1(@Headers Map<String, Object> headers, @Payload OrderMessageDTO msg) {
//        log.info("*****BasicConsumer-TOPIC receive message:{headers = {}, msg = {}}", headers, msg);
//    }
//
//    @RabbitListener(
//            bindings = @QueueBinding(
//                    exchange = @Exchange(value = RabbitMqConstant.DEFAULT_EXCHANGE, type = ExchangeTypes.DIRECT,
//                            durable = RabbitMqConstant.FALSE_CONSTANT, autoDelete = RabbitMqConstant.TRUE_CONSTANT),
//                    value = @Queue(value = RabbitMqConstant.DEFAULT_QUEUE, durable = RabbitMqConstant.FALSE_CONSTANT,
//                            autoDelete = RabbitMqConstant.TRUE_CONSTANT),
//                    key = RabbitMqConstant.DEFAULT_KEY
//            ),
//            containerFactory = RabbitMqConstant.CONTAINER_DEFAULT,
//            //指定消费者的线程数量,一个线程会打开一个Channel，一个队列上的消息只会被消费一次（不考虑消息重新入队列的情况）
//            concurrency = "1-2"
//    )
//    public void process2(@Headers Map<String, Object> headers, @Payload OrderMessageDTO msg) {
//        log.info("*****BasicConsumer-DIRECT receive message:{headers = {}, msg = {}}", headers, msg);
//    }
//
//    @RabbitListener(
//            bindings = @QueueBinding(
//                    exchange = @Exchange(value = RabbitMqConstant.DEFAULT_EXCHANGE, type = ExchangeTypes.FANOUT,
//                            durable = RabbitMqConstant.FALSE_CONSTANT, autoDelete = RabbitMqConstant.TRUE_CONSTANT),
//                    value = @Queue(value = RabbitMqConstant.DEFAULT_QUEUE, durable = RabbitMqConstant.FALSE_CONSTANT,
//                            autoDelete = RabbitMqConstant.TRUE_CONSTANT)
//            ),
//            containerFactory = RabbitMqConstant.CONTAINER_DEFAULT,
//            //指定消费者的线程数量,一个线程会打开一个Channel，一个队列上的消息只会被消费一次（不考虑消息重新入队列的情况）
//            concurrency = "1-2"
//    )
//    public void process3(@Headers Map<String, Object> headers, @Payload OrderMessageDTO msg) {
//        log.info("*****BasicConsumer-FANOUT receive message:{headers = {}, msg = {}}", headers, msg);
//    }

    @RabbitListener(
            bindings = @QueueBinding(
                    exchange = @Exchange(value = RabbitMqConstant.DEFAULT_EXCHANGE, type = ExchangeTypes.HEADERS,
                            durable = RabbitMqConstant.FALSE_CONSTANT, autoDelete = RabbitMqConstant.TRUE_CONSTANT
                    ),
                    value = @Queue(value = RabbitMqConstant.DEFAULT_QUEUE, durable = RabbitMqConstant.FALSE_CONSTANT,
                            autoDelete = RabbitMqConstant.TRUE_CONSTANT),
                    arguments = {@Argument(name = "x-match", value = "any"),
                            @Argument(name = "userName", value = "hello"),
                            @Argument(name = "password", value = "123456789")
                    }
            ),
            containerFactory = RabbitMqConstant.CONTAINER_DEFAULT,
            //指定消费者的线程数量,一个线程会打开一个Channel，一个队列上的消息只会被消费一次（不考虑消息重新入队列的情况）
            concurrency = "1-2"
    )
    public void process4(@Headers Map<String, Object> headers, @Payload OrderMessageDTO msg) {
        log.info("*****BasicConsumer-HEADERS receive message:{headers = {}, msg = {}}", headers, msg);
    }
}
