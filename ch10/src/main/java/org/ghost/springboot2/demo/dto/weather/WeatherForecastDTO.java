package org.ghost.springboot2.demo.dto.weather;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class WeatherForecastDTO implements Serializable {
    /**
     * 湿度
     */
    private String shiDu;
    /**
     * pm25
     */
    private Integer pm25;
    /**
     * pm10
     */
    private Integer pm10;
    /**
     * 质量
     */
    private String quality;
    /**
     * 温度
     */
    private String wenDu;
    /**
     * 备注
     */
    private String ganMao;
    private WeatherDTO yesterday;
    private List<WeatherDTO> forecast;

    @JsonProperty(value = "shiDu")
    public String getShiDu() {
        return shiDu;
    }

    @JsonProperty(value = "shidu")
    public void setShiDu(String shiDu) {
        this.shiDu = shiDu;
    }

    public Integer getPm25() {
        return pm25;
    }

    public void setPm25(Integer pm25) {
        this.pm25 = pm25;
    }

    public Integer getPm10() {
        return pm10;
    }

    public void setPm10(Integer pm10) {
        this.pm10 = pm10;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    @JsonProperty(value = "wenDu")
    public String getWenDu() {
        return wenDu;
    }

    @JsonProperty(value = "wendu")
    public void setWenDu(String wenDu) {
        this.wenDu = wenDu;
    }

    @JsonProperty(value = "ganMao")
    public String getGanMao() {
        return ganMao;
    }

    @JsonProperty(value = "ganmao")
    public void setGanMao(String ganMao) {
        this.ganMao = ganMao;
    }

    public WeatherDTO getYesterday() {
        return yesterday;
    }

    public void setYesterday(WeatherDTO yesterday) {
        this.yesterday = yesterday;
    }

    public List<WeatherDTO> getForecast() {
        return forecast;
    }

    public void setForecast(List<WeatherDTO> forecast) {
        this.forecast = forecast;
    }

    @Override
    public String toString() {
        return "WeatherForecastDTO{" +
                "shiDu='" + shiDu + '\'' +
                ", pm25=" + pm25 +
                ", pm10=" + pm10 +
                ", quality='" + quality + '\'' +
                ", wenDu='" + wenDu + '\'' +
                ", ganMao='" + ganMao + '\'' +
                ", yesterday=" + yesterday +
                ", forecast=" + forecast +
                '}';
    }
}
