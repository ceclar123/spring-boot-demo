package org.ghost.springboot2.demo.common.json.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.apache.commons.collections.CollectionUtils;
import org.ghost.springboot2.demo.util.DateUtil;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class DateList2StringArraySerializer extends JsonSerializer<List<Date>> {
    @Override
    public void serialize(List<Date> value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        if (CollectionUtils.isNotEmpty(value)) {
            List<String> list = value.stream().filter(Objects::nonNull).map(DateUtil::dateToStrYMD_HMS).collect(Collectors.toList());
            gen.writeObject(list);
        }
    }
}