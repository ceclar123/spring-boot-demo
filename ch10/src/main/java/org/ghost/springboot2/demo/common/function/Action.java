package org.ghost.springboot2.demo.common.function;

public interface Action {
    void execute();
}
