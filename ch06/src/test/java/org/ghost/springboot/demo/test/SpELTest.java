package org.ghost.springboot.demo.test;

import org.ghost.springboot.demo.util.MapParaSource;
import org.junit.Test;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;


public class SpELTest {
    @Test
    public void test1() {
        ExpressionParser expressionParser = new SpelExpressionParser();
        Expression expression = expressionParser.parseExpression("#id");
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("id", "100");

        System.out.println(expression.getValue(context));
    }

    @Test
    public void test2() {
        ExpressionParser expressionParser = new SpelExpressionParser();
        Expression expression = expressionParser.parseExpression("#para['id']");
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("para", new MapParaSource("id", 10).addValue("name", "张三").getValues());

        System.out.println(expression.getValue(context));
    }

    @Test
    public void test3() {
        ExpressionParser expressionParser = new SpelExpressionParser();
        Expression expression = expressionParser.parseExpression("#para[0].id");
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("para", new TestBean[]{new TestBean(1, "张三"), new TestBean(2, "李四")});

        System.out.println(expression.getValue(context));
    }

    @Test
    public void test4() {
        ExpressionParser expressionParser = new SpelExpressionParser();
        Expression expression = expressionParser.parseExpression("#para[0].id");
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("para", new TestBean[]{new TestBean(1, "张三"), new TestBean(2, "李四")});

        System.out.println(expression.getValue(context));
    }

    @Test
    public void test5() {
        ExpressionParser expressionParser = new SpelExpressionParser();
        Expression expression = expressionParser.parseExpression("#para.id");
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("para", new TestBean(1, "张三"));

        System.out.println(expression.getValue(context));
    }


    class TestBean {
        private Integer id;
        private String name;

        public TestBean() {
        }

        public TestBean(Integer id, String name) {
            this.id = id;
            this.name = name;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "TestBean{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}
