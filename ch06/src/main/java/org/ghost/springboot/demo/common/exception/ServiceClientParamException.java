package org.ghost.springboot.demo.common.exception;

import org.ghost.springboot.demo.common.constant.SystemErrorCodeEnum;

import java.io.Serializable;

public class ServiceClientParamException extends ServiceClientException implements Serializable {
    public ServiceClientParamException(String message) {
        super(SystemErrorCodeEnum.SERVICE_CLIENT_PARAM_ERROR.getCode(), message);
    }
}
