package org.ghost.springboot.demo.common.log;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import org.ghost.springboot.demo.common.component.RequestContext;

public class ReqIdConverter extends ClassicConverter {

    @Override
    public String convert(ILoggingEvent event) {
        String id = RequestContext.getContext().getId();
        return id == null ? "---" : id;
    }
}
