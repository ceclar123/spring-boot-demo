package org.ghost.springboot.demo.util.http;

public interface HttpResultCallback<T> {
    void handle(T result);
}
