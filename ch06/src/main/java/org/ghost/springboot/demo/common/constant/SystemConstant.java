package org.ghost.springboot.demo.common.constant;

public class SystemConstant {
    /**
     * 表名
     */
    public static final String LIMIT_TABLE_NAME = "limit_table_{0}";

    /**
     * 数据库最大查询数据limit 10000
     */
    public static final long DB_QUERY_MAX_ROW = 1000L;
}
