package org.ghost.springboot.demo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

public class UserEntity implements Serializable {
    private static final long serialVersionUID = 6986425202309558172L;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    private String userName;

    private Integer age;

    private String sex;

    private Long clientSn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Long getClientSn() {
        return clientSn;
    }

    public void setClientSn(Long clientSn) {
        this.clientSn = clientSn;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", clientSn=" + clientSn +
                '}';
    }
}
