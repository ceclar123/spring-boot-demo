package org.ghost.springboot.demo;

import com.test.sharding.annotation.EnableShardingTable;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;


@EnableShardingTable
@SpringBootApplication(scanBasePackages = "org.ghost.springboot.demo")
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableAsync
@MapperScan(basePackages = "org.ghost.springboot.demo")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
