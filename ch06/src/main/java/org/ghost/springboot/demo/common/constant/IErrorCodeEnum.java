package org.ghost.springboot.demo.common.constant;

public interface IErrorCodeEnum {
    String getCode();

    String getMessage();
}
