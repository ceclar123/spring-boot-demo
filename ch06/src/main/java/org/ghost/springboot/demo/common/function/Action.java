package org.ghost.springboot.demo.common.function;

public interface Action {
    void execute();
}
