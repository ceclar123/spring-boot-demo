package org.ghost.springboot.demo.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.util.List;

public final class FastJsonUtil {
    private static Logger logger = LoggerFactory.getLogger(FastJsonUtil.class);

    /**
     * 把类对象转化成json string
     *
     * @param obj
     * @return
     */
    public static String toJson(Object obj) {
        try {
            return JSONObject.toJSONString(obj);
        } catch (Exception e) {
            logger.error("*****FastJsonUtil数据转换出错:{}", e);
        }
        return "";
    }

    /**
     * 把json string 转化成类对象
     *
     * @param str
     * @param t
     * @return
     */
    public static <T> T parseObject(String str, Class<T> t) {
        try {
            if (str != null && !"".equals(str.trim())) {
                return JSONArray.parseObject(str.trim(), t);
            }
        } catch (Exception e) {
            logger.error("*****FastJsonUtil数据转换出错:{}", e);
        }
        return null;
    }

    /**
     * 把json string 转化成类对象
     *
     * @param str
     * @param type
     * @return
     */
    public static <T> T parseObject(String str, Type type) {
        try {
            if (str != null && !"".equals(str.trim())) {
                return JSONArray.parseObject(str.trim(), type);
            }
        } catch (Exception e) {
            logger.error("*****FastJsonUtil数据转换出错:{}", e);
        }
        return null;
    }

    /**
     * 把json string 转化成类对象
     *
     * @param str
     * @param type
     * @return
     */
    public static <T> T parseObject(String str, TypeReference<T> type) {
        try {
            if (str != null && !"".equals(str.trim())) {
                return JSONArray.parseObject(str.trim(), type);
            }
        } catch (Exception e) {
            logger.error("*****FastJsonUtil数据转换出错:{}", e);
        }
        return null;
    }

    /**
     * 把json string 转化成类对象
     *
     * @param str
     * @param t
     * @return
     */
    public static <T> List<T> parseArray(String str, Class<T> t) {
        try {
            if (str != null && !"".equals(str.trim())) {
                return JSONArray.parseArray(str.trim(), t);
            }
        } catch (Exception e) {
            logger.error("*****FastJsonUtil数据转换出错:{}", e);
        }
        return null;
    }


}
