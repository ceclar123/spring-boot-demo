package org.ghost.springboot.demo.common.constant;

public enum SystemErrorCodeEnum implements IErrorCodeEnum {
    SUCCESS("S200000", "操作成功"),
    SYSTEM_ERROR("E400000", "系统错误"),
    APP_KEY_NOT_EXIST_ERROR("E400001", "token 不存在 !"),
    NO_PERMISSION_ERROR("E400002", "没有访问权限"),
    UPSTREAM_SERVICE_ERROR("E400003", "上游服务错误"),
    SERVICE_CLIENT_PARAM_ERROR("E400004", "服务Client 参数错误"),
    SERVICE_CLIENT_IO_ERROR("E400005", "服务Client IO异常"),
    SERVICE_CLIENT_RESPONSE_PARSE_ERROR("E400006", "服务返回数据解析异常"),
    PARAM_ERROR("E400400", "参数错误");

    //公共业务错误码从E400500-E401000

    /**
     * 错误码
     */
    private String code;

    /**
     * 错误信息
     */
    private String message;


    /**
     * 使用错误码和错误信息构造枚举
     *
     * @param code    错误码
     * @param message 错误信息
     */
    SystemErrorCodeEnum(String code, String message) {
        this.code = code;
        this.message = message != null ? message : "";
    }

    /**
     * 获取错误码
     *
     * @return String
     */
    @Override
    public String getCode() {
        return code;
    }

    /**
     * 获取错误信息
     *
     * @return String
     */
    @Override
    public String getMessage() {
        return message;
    }
}
