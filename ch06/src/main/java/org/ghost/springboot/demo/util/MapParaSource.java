package org.ghost.springboot.demo.util;

import org.springframework.util.Assert;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MapParaSource {
    private final Map<String, Object> values = new HashMap<String, Object>();

    public MapParaSource() {
    }

    public MapParaSource(String paramName, Object value) {
        addValue(paramName, value);
    }

    public MapParaSource addValue(String paramName, Object value) {
        Assert.notNull(paramName, "Parameter name must not be null");
        this.values.put(paramName, value);
        return this;
    }

    public Map<String, Object> getValues() {
        return Collections.unmodifiableMap(this.values);
    }

    public boolean hasValue(String paramName) {
        return this.values.containsKey(paramName);
    }
}
