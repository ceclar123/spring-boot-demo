package org.ghost.springboot.demo.common.function;

public interface Action2<A, B> {
    void execute(A a, B b);
}
