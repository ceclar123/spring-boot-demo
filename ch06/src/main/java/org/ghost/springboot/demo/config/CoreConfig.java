package org.ghost.springboot.demo.config;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CoreConfig {

    @Pointcut("execution(* org.ghost.springboot.demo.controller.*.*Controller.*(..))")
    public void excudeApiController() {
        System.out.println("is pointcut");
    }
//    private final Logger logger = LoggerFactory.getLogger(CoreConfig.class);

//    @Pointcut("execution(* org.ghost.springboot.demo.controller.web.*Controller.*(..))")
//    public void excudeApiController(){
//        System.out.println("is pointcut");
//    }
//
//    /**
//     * 拦截器具体实现
//     * @param pjp 拦截点
//     * @return JsonResult（被拦截方法的执行结果，或需要登录的错误提示。）
//     */
//    @Around("excudeApiController()") //指定拦截器规则；也可以直接把“execution(* com.xjj.........)”写进这里
//    public Object Interceptor(ProceedingJoinPoint pjp){
//        Object result;
//        try {
//            result = pjp.proceed();
//        } catch(BaseException pe){
//            result = new RspDTO(pe.getCode(), pe.getMessage());
//        }catch(Throwable t){
//            if (logger.isErrorEnabled()) {
//                logger.error("系统错误: {}, {}", t.getMessage(), t);
//            }
//            result = new RspDTO(SystemErrorCodeEnum.SYSTEM_ERROR.getCode(), SystemErrorCodeEnum.SYSTEM_ERROR
// .getMessage());
//        }
//
//        return result;
//    }

}
