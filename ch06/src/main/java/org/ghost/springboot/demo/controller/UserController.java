package org.ghost.springboot.demo.controller;

import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.RandomUtils;
import org.ghost.springboot.demo.dto.RspDTO;
import org.ghost.springboot.demo.entity.UserEntity;
import org.ghost.springboot.demo.service.IUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RestController
@RequestMapping(value = "user")
public class UserController extends BaseController {
    @Autowired
    private IUserService userService;

    @RequestMapping(value = "hello", method = RequestMethod.GET)
    public RspDTO helloWorld() {
        return this.render("Hello World");
    }

    @ApiOperation(value = "批量添加用户")
    @RequestMapping(value = "batch", method = RequestMethod.POST)
    public RspDTO batchAddUser(@RequestBody UserEntity userEntity) {
        List<UserEntity> userEntityList = IntStream.range(1, 10).mapToObj(it -> {
            UserEntity entity = new UserEntity();
            BeanUtils.copyProperties(userEntity, entity);
            entity.setClientSn(RandomUtils.nextLong(1, Integer.MAX_VALUE));
            entity.setAge(RandomUtils.nextInt(1, 100));
            entity.setSex(RandomUtils.nextInt(0, 2) + "");

            return entity;
        }).collect(Collectors.toList());

        return this.render(userService.add(userEntityList));
    }

    @ApiOperation(value = "添加用户")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public RspDTO addUser(@RequestBody UserEntity userEntity) {
        return this.render(userService.add(userEntity));
    }

    @ApiOperation(value = "根据ID修改用户")
    @RequestMapping(value = "id/{id}", method = RequestMethod.PUT)
    public RspDTO updateUserById(@PathVariable Long id, @RequestBody UserEntity userEntity) {
        return this.render(userService.updateById(userEntity, id));
    }

    @ApiOperation(value = "根据clientSn修改用户")
    @RequestMapping(value = "clientSn/{clientSn}", method = RequestMethod.PUT)
    public RspDTO updateUserByClientSn(@PathVariable Long clientSn, @RequestBody UserEntity userEntity) {
        return this.render(userService.updateByClientSn(userEntity, clientSn));
    }

    @ApiOperation(value = "根据ID删除用户")
    @RequestMapping(value = "id/{id}", method = RequestMethod.DELETE)
    public RspDTO deleteUserById(@PathVariable Long id) {
        return this.render(userService.deleteById(id));
    }

    @ApiOperation(value = "根据clientSn删除用户")
    @RequestMapping(value = "clientSn/{clientSn}", method = RequestMethod.DELETE)
    public RspDTO deleteUserByClientSn(@PathVariable Long clientSn) {
        return this.render(userService.deleteByClientSn(clientSn));
    }

    @ApiOperation(value = "根据主键ID查询用户信息")
    @RequestMapping(value = "id/{id}", method = RequestMethod.GET)
    public RspDTO getUserById(@PathVariable Long id) {
        return this.render(userService.selectById(id));
    }

    @ApiOperation(value = "根据clientSn查询用户信息")
    @RequestMapping(value = "clientSn/{clientSn}", method = RequestMethod.GET)
    public RspDTO getUserByClientSn(@PathVariable Long clientSn) {
        return this.render(userService.selectByClientSn(clientSn));
    }
}

