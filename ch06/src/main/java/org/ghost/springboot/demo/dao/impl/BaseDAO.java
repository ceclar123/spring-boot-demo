package org.ghost.springboot.demo.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public abstract class BaseDAO {
    @Autowired
    protected NamedParameterJdbcTemplate jdbcTemplate;
}
