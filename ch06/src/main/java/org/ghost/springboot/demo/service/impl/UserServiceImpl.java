package org.ghost.springboot.demo.service.impl;

import org.apache.commons.collections4.CollectionUtils;
import org.ghost.springboot.demo.dao.IUserDAO;
import org.ghost.springboot.demo.entity.UserEntity;
import org.ghost.springboot.demo.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements IUserService {
    private final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private IUserDAO userDAO;

    @Override
    public boolean add(List<UserEntity> userEntityList) {
        if (CollectionUtils.isNotEmpty(userEntityList)) {
            return userDAO.add(userEntityList);
        }
        return false;
    }

    @Override
    public boolean add(UserEntity userEntity) {
        if (userEntity != null) {
            return userDAO.add(userEntity);
        }
        return false;
    }

    @Override
    public boolean updateById(UserEntity userEntity, Long id) {
        if (userEntity != null && id != null) {
            return userDAO.updateById(userEntity, id);
        }
        return false;
    }

    @Override
    public boolean updateByClientSn(UserEntity userEntity, Long clientSn) {
        if (userEntity != null && clientSn != null) {
            return userDAO.updateByClientSn(userEntity, clientSn);
        }
        return false;
    }

    @Override
    public boolean deleteById(Long id) {
        if (id != null) {
            return userDAO.deleteById(id);
        }
        return false;
    }

    @Override
    public boolean deleteByClientSn(Long clientSn) {
        if (clientSn != null) {
            return userDAO.deleteByClientSn(clientSn);
        }
        return false;
    }

    @Override
    public UserEntity selectById(Long id) {
        if (id != null) {
            return userDAO.selectById(id);
        }
        return null;
    }

    @Override
    public UserEntity selectByClientSn(Long clientSn) {
        if (clientSn != null) {
            return userDAO.selectByClientSn(clientSn);
        }
        return null;
    }
}
