package org.ghost.springboot.demo.common.exception;

import org.ghost.springboot.demo.common.constant.SystemErrorCodeEnum;

import java.io.Serializable;


public class ServiceClientResponseParseException extends ServiceClientException implements Serializable {
    public ServiceClientResponseParseException(String message) {
        super(SystemErrorCodeEnum.SERVICE_CLIENT_RESPONSE_PARSE_ERROR.getCode(), message);
    }
}
