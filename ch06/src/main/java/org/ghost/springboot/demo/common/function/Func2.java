package org.ghost.springboot.demo.common.function;

public interface Func2<A, B, R> {
    R execute(A a, B b);
}
