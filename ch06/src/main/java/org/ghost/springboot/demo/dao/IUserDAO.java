package org.ghost.springboot.demo.dao;

import org.ghost.springboot.demo.entity.UserEntity;

import java.util.List;

public interface IUserDAO {
    boolean add(List<UserEntity> userEntityList);

    boolean add(UserEntity userEntity);

    boolean updateById(UserEntity userEntity, Long id);

    boolean updateByClientSn(UserEntity userEntity, Long clientSn);

    boolean deleteById(Long id);

    boolean deleteByClientSn(Long clientSn);

    UserEntity selectById(Long id);

    UserEntity selectByClientSn(Long clientSn);
}
