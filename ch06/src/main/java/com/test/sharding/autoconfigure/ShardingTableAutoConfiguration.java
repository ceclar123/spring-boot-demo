package com.test.sharding.autoconfigure;

import com.test.sharding.properties.ShardingTableProperties;
import com.test.sharding.spring.ApplicationContextUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ShardingTableAutoConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(ShardingTableAutoConfiguration.class);

    @Bean
    public ShardingTableProperties getShardingTableProperties() {
        return new ShardingTableProperties();
    }


    @Bean
    public ApplicationContextUtil getApplicationContextUtil() {
        return new ApplicationContextUtil();
    }

//    @Bean(value = "shardingMethodConfig")
//    public Map<String, String> getHello(MybatisSqlSessionFactoryBean mybatisSqlSessionFactoryBean) throws Exception {
//        //插件放到第一个,最后一个拦截
////        List<Interceptor> interceptorList = mybatisSqlSessionFactoryBean.getObject().getConfiguration().getInterceptors();
////        Interceptor[] interceptors = new Interceptor[interceptorList != null ? interceptorList.size() + 1 : 1];
////        interceptors[0] = new ShardingTableInterceptor();
////        if (CollectionUtils.isNotEmpty(interceptorList)) {
////            int i = 1;
////            for (Interceptor item : interceptorList) {
////                interceptors[i++] = item;
////            }
////        }
////        mybatisSqlSessionFactoryBean.setPlugins(interceptors);
////        mybatisSqlSessionFactoryBean.afterPropertiesSet();
//        mybatisSqlSessionFactoryBean.getObject().getConfiguration().addInterceptor(new ShardingTableInterceptor());
//
//        //获取注解配置
//        Collection<MappedStatement> mappedStatements = mybatisSqlSessionFactoryBean.getObject().getConfiguration().getMappedStatements();
//        if (CollectionUtils.isNotEmpty(mappedStatements)) {
//            //从a.b.c..TestMapper.deleteUser中提取出a.b.c..TestMapper,获取类上面的ShardingTable注解
//            List<Tuple2<Class, String>> classAnnotationList = mappedStatements.stream().filter(Objects::nonNull)
//                    .map(MappedStatement::getId)
//                    .filter(StringUtils::isNotBlank)
//                    .map(it -> it.substring(0, it.lastIndexOf(".")))
//                    .distinct()
//                    .map(it -> {
//                        try {
//                            Class clz = Class.forName(it);
//                            Annotation annotation = clz.getAnnotation(ShardingTable.class);
//                            if (annotation != null) {
//                                return new Tuple2<Class, String>(clz, ((ShardingTable) annotation).value());
//                            } else {
//                                return new Tuple2<Class, String>(clz, "");
//                            }
//                        } catch (ClassNotFoundException e) {
//                            logger.error("*****ShardingTableAutoConfiguration出现错误:" + e.getMessage(), e);
//                            return null;
//                        }
//                    })
//                    .filter(Objects::nonNull)
//                    .collect(Collectors.toList());
//
//            //method上的ShardingTable注解可以替换类上面的ShardingTable注解
//            Map<String, String> result = classAnnotationList.stream()
//                    .filter(Objects::nonNull)
//                    .flatMap(it -> Stream.of(it.getFirst().getMethods())
//                                    .filter(Objects::nonNull)
//                                    .map(f -> {
//                                        String name = it.getFirst().getName() + "." + f.getName();
//                                        String target = it.getSecond();
//                                        Annotation annotation = f.getAnnotation(ShardingTable.class);
//                                        if (annotation != null) {
//                                            String tmpVal = ((ShardingTable) annotation).value();
//                                            if (StringUtils.isNotBlank(tmpVal)) {
//                                                target = tmpVal;
//                                            }
//                                        }
//                                        if (StringUtils.isNotBlank(target)) {
//                                            return new Tuple2<String, String>(name, target);
//                                        }
//                                        return null;
//                                    }).filter(Objects::nonNull)
//                            //.filter(f -> mappedStatements.stream().filter(Objects::nonNull).anyMatch(fi -> Objects.equals(f.getFirst(), fi.getId())))
//                    ).collect(Collectors.toMap(Tuple2::getFirst, Tuple2::getSecond));
//
//            result.entrySet().forEach(it -> System.out.println(it.getKey() + "->" + it.getValue()));
//            return result;
//        }
//
//        return null;
//    }
}