package com.test.sharding.aspect;

import com.test.sharding.properties.ShardingTableProperties;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;

@Aspect
@Component
@ConditionalOnBean(ShardingTableProperties.class)
public class ShardingTableAspect {
    public ShardingTableAspect() {
        System.out.println("hello");
    }

    @Around(value = "@annotation(com.test.sharding.annotation.ShardingTable)")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        return joinPoint.proceed(joinPoint.getArgs());
    }
}
