package com.test.sharding.annotation;

import com.test.sharding.constant.OperatorEnum;

import javax.validation.constraints.NotNull;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE, ElementType.METHOD})
public @interface ShardingTable {
    /**
     * 分表表名
     *
     * @return
     */
    @NotNull
    String value();

    /**
     * 分表字段的操作
     *
     * @return
     */
    OperatorEnum operator() default OperatorEnum.EQ;

    /**
     * 分表字段SpEL表达式
     *
     * @return
     */
    String column1() default "#id";

    /**
     * 分表字段SpEL表达式(between and需要第二个字段)
     *
     * @return
     */
    String column2() default "";
}
