package com.test.sharding.properties;

import java.io.Serializable;
import java.util.List;

public class ShardingTableItemProperties implements Serializable {
    private static final long serialVersionUID = -3918923420001101036L;

    private String logicTable;
    private Integer size;
    private List<String> shardingColumnList;
    private String shardingClass;
    private String keyGeneratorColumn;
    private String keyGeneratorClass;

    public String getLogicTable() {
        return logicTable;
    }

    public void setLogicTable(String logicTable) {
        this.logicTable = logicTable;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public List<String> getShardingColumnList() {
        return shardingColumnList;
    }

    public void setShardingColumnList(List<String> shardingColumnList) {
        this.shardingColumnList = shardingColumnList;
    }

    public String getShardingClass() {
        return shardingClass;
    }

    public void setShardingClass(String shardingClass) {
        this.shardingClass = shardingClass;
    }

    public String getKeyGeneratorColumn() {
        return keyGeneratorColumn;
    }

    public void setKeyGeneratorColumn(String keyGeneratorColumn) {
        this.keyGeneratorColumn = keyGeneratorColumn;
    }

    public String getKeyGeneratorClass() {
        return keyGeneratorClass;
    }

    public void setKeyGeneratorClass(String keyGeneratorClass) {
        this.keyGeneratorClass = keyGeneratorClass;
    }

    @Override
    public String toString() {
        return "ShardingTableItemProperties{" +
                "logicTable='" + logicTable + '\'' +
                ", size=" + size +
                ", shardingColumnList=" + shardingColumnList +
                ", shardingClass='" + shardingClass + '\'' +
                ", keyGeneratorColumn='" + keyGeneratorColumn + '\'' +
                ", keyGeneratorClass='" + keyGeneratorClass + '\'' +
                '}';
    }
}
