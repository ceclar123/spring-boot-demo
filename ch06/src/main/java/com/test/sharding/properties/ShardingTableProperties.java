package com.test.sharding.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.Serializable;
import java.util.Map;

@ConfigurationProperties(prefix = "sharding")
public class ShardingTableProperties implements Serializable {
    private static final long serialVersionUID = -7271493165523442754L;

    private Map<String, ShardingTableItemProperties> tables;

    public Map<String, ShardingTableItemProperties> getTables() {
        return tables;
    }

    public void setTables(Map<String, ShardingTableItemProperties> tables) {
        this.tables = tables;
    }

    @Override
    public String toString() {
        return "ShardingTableProperties{" +
                "tables=" + tables +
                '}';
    }
}
