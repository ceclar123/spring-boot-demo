package com.test.sharding.constant;

public enum OperatorEnum {
    EQ(1, "="),
    NOT_EQ(2, "!="),
    GREATER_EQ(3, ">="),
    LESS_EQ(4, "<="),
    IN(5, "IN"),
    NOT_IN(6, "NOT IN"),
    BETWEEN_AND(7, "BETWEEN AND");

    private int code;
    private String name;

    OperatorEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }


}
