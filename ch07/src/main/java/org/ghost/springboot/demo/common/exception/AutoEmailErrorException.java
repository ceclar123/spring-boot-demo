package org.ghost.springboot.demo.common.exception;

import org.ghost.springboot.demo.common.constant.CommonBusinessErrorCodeEnum;
import org.ghost.springboot.demo.common.constant.IErrorCodeEnum;
import org.slf4j.Logger;

import java.io.Serializable;


public class AutoEmailErrorException extends BusinessErrorException implements Serializable {
    private static final long serialVersionUID = 2075704566724746960L;

    public AutoEmailErrorException(String message) {
        this(CommonBusinessErrorCodeEnum.BUSINESS_ERROR.getCode(), message);
    }

    public AutoEmailErrorException(String methodName, IErrorCodeEnum errorCodeEnum, Logger logger) {
        super(methodName, errorCodeEnum, logger);
    }

    public AutoEmailErrorException(String code, String message) {
        super(code, message);
    }
}
