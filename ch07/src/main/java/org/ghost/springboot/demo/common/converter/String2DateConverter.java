package org.ghost.springboot.demo.common.converter;

import org.ghost.springboot.demo.util.DateUtil;
import org.springframework.core.convert.converter.Converter;

import java.util.Date;

//@Component
public class String2DateConverter implements Converter<String, Date> {
    @Override
    public Date convert(String source) {
        try {
            //先判断是否时间戳
            return new Date(Long.valueOf(source));
        } catch (Exception e) {
            return DateUtil.toDate(source);
        }
    }
}