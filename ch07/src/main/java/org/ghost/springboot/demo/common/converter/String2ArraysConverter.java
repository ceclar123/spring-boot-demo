package org.ghost.springboot.demo.common.converter;

import org.apache.commons.lang3.StringUtils;
import org.ghost.springboot.demo.util.DateUtil;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class String2ArraysConverter implements Converter<String, Date[]> {
    @Override
    public Date[] convert(String source) {
        if (StringUtils.isNotBlank(source)) {
            String[] data = source.split("\\,");
            if (data != null && data.length > 0) {
                List<Date> list = new ArrayList<>();
                for (String item : data) {
                    try {
                        //先判断是否时间戳
                        list.add(new Date(Long.valueOf(source)));
                    } catch (Exception e) {
                        list.add(DateUtil.toDate(source));
                    }
                }
                Date[] result = new Date[list.size()];
                list.toArray(result);
                return result;
            }
        }

        return null;
    }
}