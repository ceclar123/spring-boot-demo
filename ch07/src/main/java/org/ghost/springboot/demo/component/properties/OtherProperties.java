package org.ghost.springboot.demo.component.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.Serializable;


@Component
@ConfigurationProperties(prefix = "person")
@PropertySource(value = {"classpath:person.properties"}, encoding = "UTF-8")
public class OtherProperties implements Serializable {
    private static final long serialVersionUID = 6307700552480983194L;

    private String name;
    private Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "OtherProperties{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
