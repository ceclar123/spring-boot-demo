package org.ghost.springboot.demo.dto;

import java.io.Serializable;

public class TraceDTO implements Serializable {
    private static final long serialVersionUID = 5248252968982952441L;

    private String reqId;
    private String reqStartTime;
    private String reqEndTime;
    private String provider;
    private String consumer;

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public String getReqStartTime() {
        return reqStartTime;
    }

    public void setReqStartTime(String reqStartTime) {
        this.reqStartTime = reqStartTime;
    }

    public String getReqEndTime() {
        return reqEndTime;
    }

    public void setReqEndTime(String reqEndTime) {
        this.reqEndTime = reqEndTime;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getConsumer() {
        return consumer;
    }

    public void setConsumer(String consumer) {
        this.consumer = consumer;
    }

    @Override
    public String toString() {
        return "TraceDTO{" +
                "reqId='" + reqId + '\'' +
                ", reqStartTime='" + reqStartTime + '\'' +
                ", reqEndTime='" + reqEndTime + '\'' +
                ", provider='" + provider + '\'' +
                ", consumer='" + consumer + '\'' +
                '}';
    }
}
