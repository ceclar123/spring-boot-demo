package org.ghost.springboot.demo.common.filter;

import org.ghost.springboot.demo.common.component.RequestContext;
import org.ghost.springboot.demo.util.HttpRequestUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;

public class RequestUUIDFilter implements Filter {

    public static final String REQUEST_HEADER_NAME = "X-REQUEST-UUID";

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,
            ServletException {

        // uuid埋点
        uuidBuried((HttpServletRequest) req);

        // ip
        RequestContext.getContext().setClientIp(HttpRequestUtil.getIpAddr((HttpServletRequest) req));

        // req startTime
        RequestContext.getContext().setBeginTime(System.currentTimeMillis());

        chain.doFilter(req, res);
    }

    /**
     * uuid埋点
     *
     * @param request
     */
    private void uuidBuried(HttpServletRequest request) {
        HttpServletRequest httpServletRequest = request;
        String id = httpServletRequest.getHeader(REQUEST_HEADER_NAME);

        if (id == null || "".equals(id)) {
            id = UUID.randomUUID().toString();
        }

        RequestContext.getContext().setId(id);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }
}
