package org.ghost.springboot.demo.controller;

import org.ghost.springboot.demo.common.component.TestComponent;
import org.ghost.springboot.demo.component.properties.DbProperties;
import org.ghost.springboot.demo.component.properties.OtherProperties;
import org.ghost.springboot.demo.component.util.SpringUtil;
import org.ghost.springboot.demo.dto.RspDTO;
import org.ghost.springboot.demo.util.MapParaSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "user")
public class UserController extends BaseController {
    @Value("${datasource.url}")
    private String dbUrl;

    @Value("${redis.redisHost}")
    private String redisHost;

    @Autowired
    private DbProperties dbProperties;

    @Autowired
    private OtherProperties otherProperties;

    @Autowired
    private TestComponent testComponent;

    @RequestMapping(value = "config1", method = RequestMethod.GET)
    public RspDTO config1() {
        return this.render(new MapParaSource("dbUrl", dbUrl).addValue("redisHost", redisHost).getValues());
    }

    @RequestMapping(value = "config2", method = RequestMethod.GET)
    public RspDTO config2() {
        return this.render(dbProperties);
    }

    @RequestMapping(value = "config3", method = RequestMethod.GET)
    public RspDTO config3() {
        return this.render(new MapParaSource("dbUrl", SpringUtil.getValue("datasource.url")).addValue("redisHost", SpringUtil.getValue("redis.redisHost")).getValues());
    }

    @RequestMapping(value = "config4", method = RequestMethod.GET)
    public RspDTO config4() {
        return this.render(otherProperties);
    }

    @RequestMapping(value = "config5", method = RequestMethod.GET)
    public RspDTO config5() {
        return this.render(testComponent.getName());
    }
}

