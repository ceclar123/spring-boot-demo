package org.ghost.springboot.demo.common.component;

public class TestComponent {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void init() {
        System.out.println("*****init*****");
    }

    public void destory() {
        System.out.println("*****init*****");
    }
}
