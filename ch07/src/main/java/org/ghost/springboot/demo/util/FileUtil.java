package org.ghost.springboot.demo.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;


public class FileUtil {
    private final Logger logger = LoggerFactory.getLogger(FileUtil.class);

    private FileUtil() {
    }

    public static byte[] getBytes(InputStream inputStream) throws IOException {
        return org.aspectj.util.FileUtil.readAsByteArray(inputStream);
    }

    public static byte[] getBytes(File file) throws IOException {
        return org.aspectj.util.FileUtil.readAsByteArray(file);
    }

    public static String getSuffixName(String fileName) {
        if (StringUtils.isNotBlank(fileName)) {
            int index = fileName.lastIndexOf('.');
            if (index < fileName.length() - 1) {
                return fileName.substring(index + 1);
            }
            return "";
        }

        return "";
    }
}
