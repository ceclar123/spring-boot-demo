package org.ghost.springboot.demo.common.exception;

import org.ghost.springboot.demo.common.constant.SystemErrorCodeEnum;

import java.io.Serializable;

public class ServiceClientIOException extends BaseException implements Serializable {
    public ServiceClientIOException(String message) {
        this(SystemErrorCodeEnum.SERVICE_CLIENT_IO_ERROR.getCode(), message);
    }

    public ServiceClientIOException(String code, String message) {
        super(code, message);
    }
}
