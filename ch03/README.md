# spring-boot-demo

## 介绍Spring常用接口使用
### 1、InstantiationAwareBeanPostProcessor接口
- Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName)
- boolean postProcessAfterInstantiation(Object bean, String beanName)
- PropertyValues postProcessProperties(PropertyValues pvs, Object bean, String beanName)

### 2、BeanPostProcessor接口
- Object postProcessBeforeInitialization(Object bean, String beanName)
- Object postProcessAfterInitialization(Object bean, String beanName)

### 3、BeanNameAware接口
- void setBeanName(String name)

### 4、BeanFactoryAware接口
- void setBeanFactory(BeanFactory beanFactory)

### 5、InitializingBean接口
- void afterPropertiesSet()

### 6、DisposableBean接口
- void destroy()