package org.ghost.springboot2.demo;

import org.ghost.springboot2.demo.component.HelloUtil;
import org.ghost.springboot2.demo.config.MainConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        System.out.println("*****start*****");

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MainConfig.class);
        HelloUtil helloUtil = context.getBean(HelloUtil.class);
        System.out.println(helloUtil.getName());
        context.close();

        System.out.println("*****complete*****");
    }
}
