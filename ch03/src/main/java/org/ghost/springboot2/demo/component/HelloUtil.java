package org.ghost.springboot2.demo.component;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class HelloUtil implements BeanNameAware, BeanFactoryAware, InitializingBean, DisposableBean {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        System.out.println("*****set方法");
        this.name = name;
    }

    public HelloUtil() {
        System.out.println("*****默认构造方法");
    }

    @PostConstruct
    public void init() {
        System.out.println("*****PostConstruct方法");
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("*****PreDestroy方法");
    }


    @Override
    public void setBeanName(String name) {
        System.out.println("*****BeanNameAware.setBeanName方法");
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("*****BeanFactoryAware.setBeanFactory方法");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("*****DisposableBean.destroy方法");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("*****InitializingBean.afterPropertiesSet方法");
    }
}
