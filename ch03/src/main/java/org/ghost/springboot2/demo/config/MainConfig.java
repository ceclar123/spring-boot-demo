package org.ghost.springboot2.demo.config;

import org.ghost.springboot2.demo.component.HelloUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
@ComponentScan(basePackages = ("org.ghost.springboot2.demo"))
public class MainConfig {
    @Lazy
    @Bean
    public HelloUtil getHelloUtil() {
        HelloUtil helloUtil = new HelloUtil();
        helloUtil.setName("Hello World");
        return helloUtil;
    }
}
