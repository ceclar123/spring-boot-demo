package org.ghost.springboot2.demo.component;

import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class HelloInstantiationAwareBeanPostProcessor implements InstantiationAwareBeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof HelloUtil) {
            System.out.println("*****InstantiationAwareBeanPostProcessor.postProcessBeforeInitialization方法");
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof HelloUtil) {
            System.out.println("*****InstantiationAwareBeanPostProcessor.postProcessAfterInitialization方法");
        }
        return bean;
    }

    @Override
    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        if (beanClass == HelloUtil.class) {
            System.out.println("*****InstantiationAwareBeanPostProcessor.postProcessBeforeInstantiation方法");
        }
        return null;
    }

    @Override
    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
        if (bean instanceof HelloUtil) {
            System.out.println("*****InstantiationAwareBeanPostProcessor.postProcessAfterInstantiation方法");
        }
        return true;
    }

    @Override
    public PropertyValues postProcessProperties(PropertyValues pvs, Object bean, String beanName) throws BeansException {
        if (bean instanceof HelloUtil) {
            System.out.println("*****InstantiationAwareBeanPostProcessor.postProcessProperties方法");
        }
        return pvs;
    }
}
