# spring-boot-demo
## RocketMQ使用
### 一、MessageModel说明
- 1、MessageModel.CLUSTERING，默认模式
    - 每一条消息都只会被分发到一台机器上处理
    - 不保证消息的每一次失败重投等逻辑都能路由到同一台机器上
- 2、MessageModel.BROADCASTING，集群模式
    - 顺序消息暂不支持广播消费模式
    - MQ保证每条消息至少被每台客户端消费一次，但是并不会对消费失败的消息进行失败重投，因此业务方需要关注消费失败的情况
    - 广播模式下，第一次启动时默认从最新消息消费，客户端的消费进度是被持久化在客户端本地的隐藏文件中，因此不建议删除该隐藏文件，否则会丢失部分消息
    - 广播模式下，每条消息都会被大量的客户端重复处理，因此推荐尽可能使用集群模式
    - 广播模式下服务端不维护消费进度，所以 MQ 控制台不支持消息堆积查询和堆积报警功能

```Java
consumer.setMessageModel(MessageModel.BROADCASTING);
```

### ConsumeFromWhere说明
- 1、CONSUME_FROM_LAST_OFFSET
> 一个新的订阅组第一次启动从队列的最后位置开始消费,后续再启动接着上次消费的进度开始消费
- 2、CONSUME_FROM_FIRST_OFFSET
> 一个新的订阅组第一次启动从队列的最前位置开始消费,后续再启动接着上次消费的进度开始消费
- 3、CONSUME_FROM_TIMESTAMP
> 一个新的订阅组第一次启动从指定时间点开始消费,后续再启动接着上次消费的进度开始消费；时间点设置参见DefaultMQPushConsumer.consumeTimestamp参数