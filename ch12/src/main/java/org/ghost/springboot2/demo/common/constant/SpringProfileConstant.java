package org.ghost.springboot2.demo.common.constant;

/**
 * @author Administrator
 */
public interface SpringProfileConstant {
    /**
     * 消费者consumer_push_concurrent
     */
    String PROFILE_CONSUMER_PUSH_CONCURRENT = "consumer_push_concurrent";
    /**
     * 消费者consumer_push_orderly
     */
    String PROFILE_CONSUMER_PUSH_ORDERLY = "consumer_push_orderly";

    /**
     * 消费者consumer_push_broadcast
     * MessageModel.BROADCASTING，MQ会将每条消息推送给集群内所有注册过的客户端，保证消息至少被每台机器消费一次
     */
    String PROFILE_CONSUMER_PUSH_BROADCAST = "consumer_push_broadcast";
    /**
     * 消费者consumer_push_scheduled
     */
    String PROFILE_CONSUMER_PUSH_SCHEDULED = "consumer_push_scheduled";


}
