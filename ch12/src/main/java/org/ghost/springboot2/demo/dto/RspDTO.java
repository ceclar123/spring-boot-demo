package org.ghost.springboot2.demo.dto;


import java.io.Serializable;


/**
 * @author Administrator
 */
public class RspDTO implements Serializable {
    private static final long serialVersionUID = 3774308086534688634L;

    private static final Tuple2<String, String> SUCCESS = new Tuple2<String, String>("E20000", "操作成功");
    private static final Tuple2<String, String> SYSTEM_ERROR = new Tuple2<String, String>("E50000", "系统错误");


    /**
     * 是否成功
     */
    private Boolean success;

    /**
     * 错误码
     */
    private String code;

    /**
     * 错误信息
     */
    private String message;

    /**
     * 返回数据
     */
    private Object data;

    /**
     * 使用错误码和错误信息构造返回DTO
     *
     * @param data 返回数据
     */
    public RspDTO(Object data) {
        this(SUCCESS.getFirst(), SUCCESS.getSecond(), data);
    }

    /**
     * 使用错误码和错误信息构造返回DTO
     *
     * @param code    错误码
     * @param message 错误信息
     */
    public RspDTO(String code, String message) {
        this(code, message, null);
    }

    /**
     * @param code    错误码
     * @param message 错误信息
     * @param data    数据
     */
    public RspDTO(String code, String message, Object data) {
        this.code = code != null ? code : SUCCESS.getFirst();
        this.message = message != null ? message : SUCCESS.getSecond();
        this.data = data;
        this.success = SUCCESS.getFirst().equals(this.code);
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    /**
     * 获取错误码
     *
     * @return 错误码
     */
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取错误信息
     *
     * @return 错误信息
     */
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 获取数据
     *
     * @return T
     */
    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RspDTO{" +
                "success=" + success +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
