package org.ghost.springboot2.demo.common.constant;

/**
 * @author Administrator
 */
public interface RabbitMqConstant {

    /**
     * rabbitmq前缀名
     */
    String RABBITMQ_PREFIX = "ghost_";
    /**
     * rabbit注解一些布尔值采用了字符串所以定义了常量以免弄错
     */
    String TRUE_CONSTANT = "true";
    String FALSE_CONSTANT = "false";
    /**
     * 默认交换器名字
     */
    String DEFAULT_EXCHANGE = RABBITMQ_PREFIX + "exchange.default";
    String DEFAULT_QUEUE = RABBITMQ_PREFIX + "queue.default";
    String DEFAULT_KEY = RABBITMQ_PREFIX + "key.default";

    /**
     * 需要消费者确认的队列
     */
    String CONFIRM_EXCHANGE = RABBITMQ_PREFIX + "exchange.confirm";
    String CONFIRM_QUEUE = RABBITMQ_PREFIX + "queue.confirm";
    String CONFIRM_KEY = RABBITMQ_PREFIX + "key.confirm";

    /**
     * 自动确认的队列
     */
    String NONE_EXCHANGE = RABBITMQ_PREFIX + "exchange.none";
    String NONE_QUEUE = RABBITMQ_PREFIX + "queue.none";
    String NONE_KEY = RABBITMQ_PREFIX + "key.none";
    /**
     * 死信队列
     */
    String DEAD_EXCHANGE = RABBITMQ_PREFIX + "exchange.dead";
    String DEAD_QUEUE = RABBITMQ_PREFIX + "queue.dead";
    String DEAD_KEY = RABBITMQ_PREFIX + "key.dead";

    /**
     * 显示声明的队列名字
     */
    String PROGRAMMATICALLY_QUEUE = RABBITMQ_PREFIX + "queue.programmatically";
    String PROGRAMMATICALLY_EXCHANGE = RABBITMQ_PREFIX + "exchange.programmatically";
    String PROGRAMMATICALLY_KEY = RABBITMQ_PREFIX + "key.programmatically";
    /**
     * 多个监听器
     */
    String MULTIPART_HANDLE_EXCHANGE = RABBITMQ_PREFIX + "exchange.multipart";
    String MULTIPART_HANDLE_QUEUE = RABBITMQ_PREFIX + "queue.multipart";
    String MULTIPART_HANDLE_KEY = RABBITMQ_PREFIX + "key.multipart";

    String MANDATORY_KEY = RABBITMQ_PREFIX + "key.mandatory";
    String MULTIPART_QUEUE = RABBITMQ_PREFIX + "queue.multipart";
    /**
     * 测试direct容器
     */
    String DIRECT_QUEUE = RABBITMQ_PREFIX + "queue.direct";
    String DIRECT_EXCHANGE = RABBITMQ_PREFIX + "exchange.direct";
    String DIRECT_KEY = RABBITMQ_PREFIX + "key.direct";

    /**
     * 测试消费者reply
     */
    String REPLY_EXCHANGE = RABBITMQ_PREFIX + "exchange.reply";
    String REPLY_QUEUE = RABBITMQ_PREFIX + "queue.reply";
    String REPLY_KEY = RABBITMQ_PREFIX + "key.reply";
    /**
     * RPC模式
     */
    String RPC_EXCHANGE = RABBITMQ_PREFIX + "exchange.rpc";
    String RPC_QUEUE = RABBITMQ_PREFIX + "queue.rpc";
    String RPC_KEY = RABBITMQ_PREFIX + "key.rpc";

    /**
     * container
     */
    String CONTAINER_DEFAULT = RABBITMQ_PREFIX + "container";
    String CONTAINER_WITH_CONFIRM = RABBITMQ_PREFIX + "containerWithConfirm";
    String CONTAINER_WITH_NONE= RABBITMQ_PREFIX+"containerWithNone";
}
