package org.ghost.springboot2.demo.producer;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.ghost.springboot2.demo.config.RocketMqProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author Administrator
 */
@Slf4j
@Component
public class DefaultProducer {
    @Autowired
    private RocketMqProperties rocketMqProperties;

    private DefaultMQProducer defaultProducer;

    public DefaultProducer() {
        defaultProducer = new DefaultMQProducer();
    }

    @PostConstruct
    public void init() throws MQClientException {
        defaultProducer.setProducerGroup(rocketMqProperties.getGroupName());
        defaultProducer.setNamesrvAddr(rocketMqProperties.getNameServerAddr());
        defaultProducer.setVipChannelEnabled(rocketMqProperties.getProducer().isVipChannelEnabled());
        defaultProducer.start();
    }

    @PreDestroy
    public void destroy() {
        defaultProducer.shutdown();
    }

    public DefaultMQProducer getDefaultProducer() {
        return defaultProducer;
    }
}
