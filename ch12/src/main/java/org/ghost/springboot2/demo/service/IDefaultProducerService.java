package org.ghost.springboot2.demo.service;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.remoting.exception.RemotingException;

/**
 * @author Administrator
 */
public interface IDefaultProducerService {
    /**
     * 同步发送(主要运用在比较重要一点消息传递、通知等业务)
     *
     * @param topic topic
     * @param tags  tags
     * @param msg   msg
     * @return
     * @throws InterruptedException
     * @throws RemotingException
     * @throws MQClientException
     * @throws MQBrokerException
     */
    SendResult syncSend(String topic, String tags, String msg) throws InterruptedException, RemotingException, MQClientException, MQBrokerException;

    /**
     * 同步发送(主要运用在比较重要一点消息传递、通知等业务)
     *
     * @param topic   topic
     * @param tags    tags
     * @param msg     msg
     * @param timeout timeout
     * @return
     * @throws InterruptedException
     * @throws RemotingException
     * @throws MQClientException
     * @throws MQBrokerException
     */
    SendResult syncSend(String topic, String tags, String msg, long timeout) throws InterruptedException, RemotingException, MQClientException, MQBrokerException;

    /**
     * 异步发送(通常用于对发送消息响应时间要求更高、更快的场景)
     *
     * @param topic    topic
     * @param tags     tags
     * @param msg      msg
     * @param callback callback
     * @throws RemotingException
     * @throws MQClientException
     * @throws InterruptedException
     */
    void asyncSend(String topic, String tags, String msg, SendCallback callback) throws RemotingException, MQClientException, InterruptedException;

    /**
     * 异步发送(通常用于对发送消息响应时间要求更高、更快的场景)
     *
     * @param topic    topic
     * @param tags     tags
     * @param msg      msg
     * @param callback callback
     * @param timeout  timeout
     * @throws RemotingException
     * @throws MQClientException
     * @throws InterruptedException
     */
    void asyncSend(String topic, String tags, String msg, SendCallback callback, long timeout) throws RemotingException, MQClientException, InterruptedException;

    /**
     * 单向发送(运用可靠性不是很高的场景，例如日志收集；只发送消息，不等待服务器响应，只发送请求不等待应答,没有回调)
     *
     * @param topic topic
     * @param tags  tags
     * @param msg   msg
     * @throws RemotingException
     * @throws MQClientException
     * @throws InterruptedException
     */
    void sendOneWay(String topic, String tags, String msg) throws RemotingException, MQClientException, InterruptedException;

    SendResult syncSend(String topic, String tags, String msg, MessageQueueSelector selector, Object arg) throws InterruptedException, RemotingException, MQClientException, MQBrokerException;

    SendResult syncSend(String topic, String tags, String msg, MessageQueueSelector selector, Object arg, long timeout) throws InterruptedException, RemotingException, MQClientException, MQBrokerException;

    void asyncSend(String topic, String tags, String msg, MessageQueueSelector selector, Object arg, SendCallback sendCallback) throws RemotingException, MQClientException, InterruptedException;

    void asyncSend(String topic, String tags, String msg, MessageQueueSelector selector, Object arg, SendCallback sendCallback, long timeout) throws RemotingException, MQClientException, InterruptedException;
}
