package org.ghost.springboot2.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Administrator
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderMessageDTO implements Serializable {
    private static final long serialVersionUID = 4940297415712781949L;

    private Long orderId;
    private String buyer;
    private Integer num;
}
