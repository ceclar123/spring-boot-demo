package org.ghost.springboot2.demo.producer;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.TransactionMQProducer;
import org.ghost.springboot2.demo.config.RocketMqProperties;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author Administrator
 */
@Slf4j
//@Component
public class TransactionProducer {
    @Autowired
    private RocketMqProperties rocketMqProperties;

    private TransactionMQProducer transactionProducer;

    public TransactionProducer() {
        transactionProducer = new TransactionMQProducer();
    }

    @PostConstruct
    public void init() throws MQClientException {
        transactionProducer.setProducerGroup(rocketMqProperties.getGroupName());
        transactionProducer.setNamesrvAddr(rocketMqProperties.getNameServerAddr());
        transactionProducer.setVipChannelEnabled(rocketMqProperties.getProducer().isVipChannelEnabled());
        transactionProducer.start();
    }

    @PreDestroy
    public void destroy() {
        transactionProducer.shutdown();
    }

    public TransactionMQProducer getTransactionProducer() {
        return transactionProducer;
    }
}
