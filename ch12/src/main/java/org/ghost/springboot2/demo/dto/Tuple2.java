package org.ghost.springboot2.demo.dto;

import java.io.Serializable;

/**
 * @author Administrator
 */
public class Tuple2<A, B> implements Serializable {
    private static final long serialVersionUID = -6669426008982428358L;

    private A first;
    private B second;

    public Tuple2(A first, B second) {
        this.first = first;
        this.second = second;
    }

    public A getFirst() {
        return first;
    }

    public void setFirst(A first) {
        this.first = first;
    }

    public B getSecond() {
        return second;
    }

    public void setSecond(B second) {
        this.second = second;
    }

    @Override
    public String toString() {
        return "Tuple2{" +
                "first=" + first +
                ", second=" + second +
                '}';
    }
}
