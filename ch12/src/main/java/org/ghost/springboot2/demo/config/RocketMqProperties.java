package org.ghost.springboot2.demo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @author Administrator
 */
@Data
@Component
@ConfigurationProperties(prefix = "rocketmq")
@PropertySource(value = {"classpath:rocketmq.properties"}, encoding = "UTF-8")
public class RocketMqProperties implements Serializable {
    private static final long serialVersionUID = -6401951325070202677L;

    private boolean enabled = false;
    private String nameServerAddr = "localhost:9876";
    private String groupName = "default";
    private ProducerProp producer;
    private ConsumerProp consumer;


    @Data
    public static class ProducerProp implements Serializable {
        private static final long serialVersionUID = 396111516816767598L;

        public ProducerProp() {
        }

        private int maxMessageSize = 1024;
        private int sendMsgTimeout = 2000;
        private int retryTimesWhenSendFailed = 2;
        private boolean vipChannelEnabled = false;
    }

    @Data
    public static class ConsumerProp implements Serializable {
        private static final long serialVersionUID = -7728993663235229561L;

        public ConsumerProp() {
        }

        private int consumeThreadMin = 5;
        private int consumeThreadMax = 30;
        private int consumeMessageBatchMaxSize = 1;
    }
}
