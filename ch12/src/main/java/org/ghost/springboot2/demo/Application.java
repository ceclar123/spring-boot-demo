package org.ghost.springboot2.demo;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Administrator
 */
@EnableScheduling
@SpringBootApplication(scanBasePackages = {"org.ghost.springboot2.demo"})
public class Application {
    public static void main(String[] args) {
        System.out.println("*****starting*****");

        //SpringApplication.run(Application.class, args);
        SpringApplication springApplication = new SpringApplication(Application.class);
        springApplication.setBannerMode(Banner.Mode.CONSOLE);
        springApplication.run(args);

        System.out.println("*****complete*****");
    }
}
