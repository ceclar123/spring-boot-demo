package org.ghost.springboot2.demo.test;

import org.ghost.springboot2.demo.Application;
import org.ghost.springboot2.demo.dto.HttpRspDTO;
import org.ghost.springboot2.demo.test.service.ITestHelloService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@EnableFeignClients(basePackages = {"org.ghost.springboot2.demo.test.service"})
public class FeignTest {
    @Autowired
    private ITestHelloService testHelloService;

    @Test
    public void test1() {
        long begin = System.currentTimeMillis();

        HttpRspDTO<String> rspDTO = testHelloService.getTestHello("这是一条测试数据");

        long end = System.currentTimeMillis();
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
        System.out.println(rspDTO);
    }

    @Test
    public void test2() {
        long begin = System.currentTimeMillis();

        Map<String, Object> paraMap = new HashMap<String, Object>();
        paraMap.put("msg", "这是一条测试数据");
        HttpRspDTO<String> rspDTO = testHelloService.postTestHello(paraMap);

        long end = System.currentTimeMillis();
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
        System.out.println(rspDTO);
    }
}
