package org.ghost.springboot2.demo.test;

import org.ghost.springboot2.demo.common.http.HttpClientFactory;
import org.ghost.springboot2.demo.dto.HttpRspDTO;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class HttpProxyTest {
    @Test
    public void test1() {
        long begin = System.currentTimeMillis();

        IHelloService testService = HttpClientFactory.getBean(IHelloService.class);

        long end = System.currentTimeMillis();
        System.out.println("*****构造对象消耗时间(秒):" + (end - begin) / 1000.0);
        begin = System.currentTimeMillis();

        HttpRspDTO<String> rspDTO = testService.demoHello("张三");

        end = System.currentTimeMillis();
        System.out.println("*****查询数据消耗时间(秒):" + (end - begin) / 1000.0);
        System.out.println(rspDTO);

        begin = System.currentTimeMillis();
        Map<String, Object> paraMap = new HashMap<String, Object>();
        paraMap.put("msg", "这是一条测试数据");

        rspDTO = testService.demoHello(paraMap);
        end = System.currentTimeMillis();
        System.out.println("*****查询数据消耗时间(秒):" + (end - begin) / 1000.0);
        System.out.println(rspDTO);
    }
}
