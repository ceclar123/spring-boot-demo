package org.ghost.springboot2.demo.test;

import org.ghost.springboot2.demo.dto.HttpRspDTO;
import org.junit.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import java.lang.reflect.Type;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class RestTemplateTest {

    @Test
    public void test1() {
        long begin = System.currentTimeMillis();

        Map<String, Object> paraMap = new HashMap<String, Object>();
        paraMap.put("msg", "这是一条测试数据");

        ResponseEntity<String> responseEntity = new RestTemplate().getForEntity("http://localhost:9999/demo/hello?msg={msg}", String.class, paraMap);
        System.out.println(responseEntity.getStatusCode());
        System.out.println(responseEntity.getBody());

        String rtn = new RestTemplate().getForObject("http://localhost:9999/demo/hello?msg={msg}", String.class, paraMap);
        System.out.println(rtn);


        responseEntity = new RestTemplate().postForEntity("http://localhost:9999/demo/hello", paraMap, String.class);
        System.out.println(responseEntity.getStatusCode());
        System.out.println(responseEntity.getBody());

        rtn = new RestTemplate().postForObject("http://localhost:9999/demo/hello", paraMap, String.class);
        System.out.println(rtn);

        long end = System.currentTimeMillis();
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
    }

    @Test
    public void test2() {
        long begin = System.currentTimeMillis();

        ParameterizedTypeReference<HttpRspDTO<String>> parameterizedTypeReference = new ParameterizedTypeReference<HttpRspDTO<String>>() {
            @Override
            public Type getType() {
                return ParameterizedTypeImpl.make(HttpRspDTO.class, new Type[]{String.class}, null);
            }
        };

        ResponseEntity<HttpRspDTO<String>> responseEntity = new RestTemplate().exchange(URI.create("http://localhost:9999/demo/hello"), HttpMethod.GET, null, parameterizedTypeReference);

        System.out.println(responseEntity.getStatusCode());
        System.out.println(responseEntity.getBody());

        Map<String, Object> paraMap = new HashMap<String, Object>();
        paraMap.put("msg", "这是一条测试数据");
        responseEntity = new RestTemplate().exchange("http://localhost:9999/demo/hello?msg={msg}", HttpMethod.GET, null, parameterizedTypeReference, paraMap);

        System.out.println(responseEntity.getStatusCode());
        System.out.println(responseEntity.getBody());

        long end = System.currentTimeMillis();
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
    }
}


