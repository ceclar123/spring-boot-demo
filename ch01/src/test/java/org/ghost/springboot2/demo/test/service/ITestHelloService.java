package org.ghost.springboot2.demo.test.service;

import org.ghost.springboot2.demo.config.feign.HelloServiceApiConfig;
import org.ghost.springboot2.demo.dto.HttpRspDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(name = "helloApi", url = "http://localhost:9999", configuration = HelloServiceApiConfig.class)
public interface ITestHelloService {
    @RequestMapping(value = "demo/hello", method = RequestMethod.GET)
    HttpRspDTO<String> getTestHello(@RequestParam(value = " msg") String msg);


    @RequestMapping(value = "demo/hello", method = RequestMethod.POST)
    HttpRspDTO<String> postTestHello(@RequestBody Map<String, Object> reqBody);
}
