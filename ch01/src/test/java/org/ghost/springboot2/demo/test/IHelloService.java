package org.ghost.springboot2.demo.test;

import org.ghost.springboot2.demo.common.http.annotation.HttpClientEx;
import org.ghost.springboot2.demo.common.http.annotation.RequestMappingEx;
import org.ghost.springboot2.demo.dto.HttpRspDTO;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@HttpClientEx(hostName = "http://localhost:9999")
public interface IHelloService {
    @RequestMappingEx(name = "demo/hello", method = HttpMethod.GET, headers = {"xxx=123"})
    HttpRspDTO<String> demoHello(@RequestParam(value = "msg") String msg);

    @RequestMappingEx(name = "demo/hello", method = HttpMethod.POST, headers = {"xxx=123"})
    HttpRspDTO<String> demoHello(@RequestBody Map<String, Object> reqMap);
}
