package org.ghost.springboot2.demo.test;

import org.ghost.springboot2.demo.dto.HttpRspDTO;
import org.junit.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import java.lang.reflect.Type;
import java.net.URI;

public class WebClientTest {
    @Test
    public void test1() {
        long begin = System.currentTimeMillis();

        // GET返回简单类型
        Mono<String> responseEntity = WebClient.create().get()
                .uri(URI.create("http://localhost:9999/demo/hello"))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
                .retrieve().bodyToMono(String.class);
        System.out.println(responseEntity.block());

        // GET带参数返回简单类型
        //Map<String, Object> paraMap = new HashMap<String, Object>();
        //paraMap.put("msg", "这是一条测试数据");
        responseEntity = WebClient.create().get()
                //.uri("http://localhost:9999/demo/hello?msg={msg}", "这是一条测试数据")
                //.uri("http://localhost:9999/demo/hello?msg={msg}", paraMap)
                .uri((uriBuilder) -> uriBuilder
                        .scheme("http")
                        .host("localhost")
                        .port("9999")
                        .path("demo/hello")
                        .queryParam("msg", "这是一条测试数据")
                        .build())
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
                .retrieve().bodyToMono(String.class);
        System.out.println(responseEntity.block());

        long end = System.currentTimeMillis();
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
    }


    @Test
    public void test3() {
        long begin = System.currentTimeMillis();

        ParameterizedTypeReference<HttpRspDTO<String>> parameterizedTypeReference = new ParameterizedTypeReference<HttpRspDTO<String>>() {
            @Override
            public Type getType() {
                return ParameterizedTypeImpl.make(HttpRspDTO.class, new Type[]{String.class}, null);
            }
        };

        Mono<HttpRspDTO<String>> responseEntity = WebClient.create().get()
                .uri(URI.create("http://localhost:9999/demo/hello"))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
                .retrieve().bodyToMono(parameterizedTypeReference);

        System.out.println(responseEntity.block());

        long end = System.currentTimeMillis();
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
    }
}
