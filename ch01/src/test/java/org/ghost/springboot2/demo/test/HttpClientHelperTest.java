package org.ghost.springboot2.demo.test;

import org.ghost.springboot2.demo.Application;
import org.ghost.springboot2.demo.common.http.HttpClientHelper;
import org.ghost.springboot2.demo.common.http.HttpRequestBuilder;
import org.ghost.springboot2.demo.config.TaskExecutorConfig;
import org.ghost.springboot2.demo.dto.HttpRspDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class HttpClientHelperTest {
    @Autowired
    private TaskExecutorConfig taskExecutorConfig;

    /**
     * HttpClient
     */
    @Test
    public void test1() {
        long begin = System.currentTimeMillis();

        HttpRequestBuilder httpRequestBuilder = new HttpRequestBuilder()
                .build(HttpMethod.GET, "http://localhost:9999/demo/hello")
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);

        HttpRspDTO<String> rspDTO = HttpClientHelper.invoke(httpRequestBuilder, HttpRspDTO.class, null, String.class);

        long end = System.currentTimeMillis();
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
        System.out.println(rspDTO);
    }

    /**
     * 自定义线程池 + HttpClient
     */
    @Test
    public void test2() {
        long begin = System.currentTimeMillis();
        List<Future<HttpRspDTO<String>>> futureList = new ArrayList<Future<HttpRspDTO<String>>>();
        for (int i = 0; i < 100; i++) {
            HttpRequestBuilder httpRequestBuilder = new HttpRequestBuilder()
                    .build(HttpMethod.GET, "http://localhost:9999/demo/hello")
                    .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);

            Future<HttpRspDTO<String>> future = taskExecutorConfig.getAsyncTaskExecutor().submit(new Callable<HttpRspDTO<String>>() {
                @Override
                public HttpRspDTO<String> call() throws Exception {
                    return HttpClientHelper.invoke(httpRequestBuilder, HttpRspDTO.class, null, String.class);
                }
            });
            futureList.add(future);
        }

        List<String> resultList = new ArrayList<String>();
        for (Future<HttpRspDTO<String>> future : futureList) {
            try {
                HttpRspDTO<String> rspDTO = future.get();
                if (rspDTO != null && Objects.equals(Boolean.TRUE, rspDTO.getSuccess())) {
                    resultList.add(rspDTO.getData());
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        long end = System.currentTimeMillis();
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
        System.out.println(resultList.size());
    }

    /**
     * ParallelStream + HttpClient
     */
    @Test
    public void test3() {
        long begin = System.currentTimeMillis();

        List<String> resultList = IntStream.range(0, 100)
                .parallel()
                .mapToObj(it -> {
                    HttpRequestBuilder httpRequestBuilder = new HttpRequestBuilder()
                            .build(HttpMethod.GET, "http://localhost:9999/demo/hello")
                            .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
                            .addUrlPara("msg", String.valueOf(it));
                    HttpRspDTO<String> rspDTO = HttpClientHelper.invoke(httpRequestBuilder, HttpRspDTO.class, null, String.class);
                    if (rspDTO != null && Objects.equals(Boolean.TRUE, rspDTO.getSuccess())) {
                        return rspDTO.getData();
                    } else {
                        return null;
                    }
                }).filter(Objects::nonNull).collect(Collectors.toList());

        long end = System.currentTimeMillis();
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
        System.out.println(resultList.size());
    }
}
