package org.ghost.springboot2.demo.test;

import org.apache.http.concurrent.FutureCallback;
import org.ghost.springboot2.demo.Application;
import org.ghost.springboot2.demo.common.http.HttpAsyncClientHelper;
import org.ghost.springboot2.demo.common.http.HttpRequestBuilder;
import org.ghost.springboot2.demo.common.http.HttpResultCallback;
import org.ghost.springboot2.demo.dto.HttpRspDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class HttpAsyncClientHelperTest {
    /**
     * HttpAsyncClient单条Future
     */
    @Test
    public void test1() {
        long begin = System.currentTimeMillis();

        HttpRequestBuilder httpRequestBuilder = new HttpRequestBuilder()
                .build(HttpMethod.GET, "http://localhost:9999/demo/hello")
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);

        HttpRspDTO<String> rspDTO = HttpAsyncClientHelper.invoke(httpRequestBuilder, HttpRspDTO.class, null, String.class);

        long end = System.currentTimeMillis();
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
        System.out.println(rspDTO);
    }

    /**
     * HttpAsyncClient单条Callback
     */
    @Test
    public void test2() throws ExecutionException, InterruptedException, TimeoutException {
        long begin = System.currentTimeMillis();

        HttpRequestBuilder httpRequestBuilder = new HttpRequestBuilder()
                .build(HttpMethod.GET, "http://localhost:9999/demo/hello")
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);

        //List<HttpRspDTO<String>> rspDTO = HttpAsyncClientHelper.invoke(Collections.singletonList(httpRequestBuilder), HttpRspDTO.class, null, String.class);

        HttpAsyncClientHelper.invoke(httpRequestBuilder, HttpRspDTO.class, null, new Type[]{String.class}, new FutureCallback<HttpRspDTO<String>>() {
            @Override
            public void completed(HttpRspDTO<String> result) {
                long end = System.currentTimeMillis();
                System.out.println("*****completed消耗时间(秒):" + (end - begin) / 1000.0);
                System.out.println(result);
            }

            @Override
            public void failed(Exception ex) {
                long end = System.currentTimeMillis();
                System.out.println("*****failed消耗时间(秒):" + (end - begin) / 1000.0);
            }

            @Override
            public void cancelled() {
                long end = System.currentTimeMillis();
                System.out.println("*****cancelled消耗时间(秒):" + (end - begin) / 1000.0);
            }
        });

        long end = System.currentTimeMillis();
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
    }

    /**
     * HttpAsyncClient批量Callback
     */
    @Test
    public void test3() {
        long begin = System.currentTimeMillis();
        List<HttpRequestBuilder> requestBuilderList = new ArrayList<HttpRequestBuilder>();
        for (int i = 0; i < 100; i++) {
            HttpRequestBuilder httpRequestBuilder = new HttpRequestBuilder()
                    .build(HttpMethod.GET, "http://localhost:9999/demo/hello")
                    .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
            requestBuilderList.add(httpRequestBuilder);
        }

        List<String> resultList = new ArrayList<String>();
        HttpAsyncClientHelper.invoke(requestBuilderList, HttpRspDTO.class, null, new Type[]{String.class}, new HttpResultCallback<HttpRspDTO<String>>() {
            @Override
            public void handle(HttpRspDTO<String> result) {
                //System.out.println("#####返回结果:" + result);
                if (result != null && Objects.equals(Boolean.TRUE, result.getSuccess())) {
                    resultList.add(result.getData());
                }
            }
        });

        long end = System.currentTimeMillis();
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
        System.out.println(resultList.size());
    }

    /**
     * HttpAsyncClient批量Future
     */
    @Test
    public void test4() {
        long begin = System.currentTimeMillis();
        List<HttpRequestBuilder> requestBuilderList = new ArrayList<HttpRequestBuilder>();
        for (int i = 0; i < 100; i++) {
            HttpRequestBuilder httpRequestBuilder = new HttpRequestBuilder()
                    .build(HttpMethod.GET, "http://localhost:9999/demo/hello")
                    .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
            requestBuilderList.add(httpRequestBuilder);
        }

        List<HttpRspDTO<String>> rspDTOList = HttpAsyncClientHelper.invoke(requestBuilderList, HttpRspDTO.class, null, String.class);

        long end = System.currentTimeMillis();
        System.out.println("*****消耗时间(秒):" + (end - begin) / 1000.0);
        System.out.println(rspDTOList != null ? rspDTOList.stream().filter(Objects::nonNull).filter(it -> Objects.equals(Boolean.TRUE, it.getSuccess())).count() : 0);
    }
}
