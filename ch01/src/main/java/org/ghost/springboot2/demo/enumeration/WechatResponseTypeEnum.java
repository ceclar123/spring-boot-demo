package org.ghost.springboot2.demo.enumeration;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

public enum WechatResponseTypeEnum {
    /**
     * 默认code
     */
    CODE("code", "默认code");

    WechatResponseTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static boolean exists(String code) {
        if (StringUtils.isNotBlank(code)) {
            for (WechatResponseTypeEnum type : WechatResponseTypeEnum.values()) {
                if (Objects.equals(type.getCode(), code)) {
                    return true;
                }
            }
        }
        return false;
    }
}
