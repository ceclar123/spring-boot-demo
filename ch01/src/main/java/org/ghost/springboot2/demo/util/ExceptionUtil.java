package org.ghost.springboot2.demo.util;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;

/**
 * 异常工具
 */
final public class ExceptionUtil {
    public static void throwRuntimeException(String method, String message, Logger logger) throws RuntimeException {
        if (logger != null && logger.isWarnEnabled()) {
            logger.warn("{} 业务异常: {}", method, message);
        }

        throw new RuntimeException(method + message);
    }

    public static String getExceptionStack(Throwable e) {
        StringBuilder builder = new StringBuilder(e.toString() + e.getMessage() + "\n");
        StackTraceElement[] stackTraceElements = e.getStackTrace();
        if (ArrayUtils.isNotEmpty(stackTraceElements)) {
            for (int index = stackTraceElements.length - 1; index >= 0; --index) {
                builder.append("at [").append(stackTraceElements[index].getClassName()).append(",");
                builder.append(stackTraceElements[index].getFileName()).append(",");
                builder.append(stackTraceElements[index].getMethodName()).append(",");
                builder.append(stackTraceElements[index].getLineNumber()).append("]\n");
            }
        }

        if (e.getCause() != null) {
            builder.append(getExceptionStack(e.getCause()));
        }

        return builder.toString();
    }
}
