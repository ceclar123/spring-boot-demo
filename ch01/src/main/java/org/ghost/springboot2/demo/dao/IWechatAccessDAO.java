package org.ghost.springboot2.demo.dao;

import org.ghost.springboot2.demo.common.http.annotation.HttpClientEx;
import org.ghost.springboot2.demo.common.http.annotation.RequestMappingEx;
import org.ghost.springboot2.demo.dto.WechatAccessTokenRspDTO;
import org.ghost.springboot2.demo.dto.WechatErrorRspDTO;
import org.ghost.springboot2.demo.dto.WechatOpenIdRspDTO;
import org.ghost.springboot2.demo.dto.WechatUserInfoRspDTO;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 01
 * https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html
 */
@HttpClientEx(hostName = "https://api.weixin.qq.com")
public interface IWechatAccessDAO {
    /**
     * 通过code获取openId
     *
     * @param appId     公众号的唯一标识
     * @param secret    公众号的appsecret
     * @param code      认证返回的code
     * @param grantType 填写为authorization_code
     * @return WechatOpenIdRspDTO
     */
    @RequestMappingEx(name = "sns/oauth2/access_token", method = HttpMethod.GET)
    WechatOpenIdRspDTO getOpenId(@RequestParam(value = "appid") String appId,
                                 @RequestParam(value = "secret") String secret,
                                 @RequestParam(value = "code") String code,
                                 @RequestParam(value = "grant_type") String grantType);

    /**
     * 通过refresh_token刷新access_token
     *
     * @param appId        公众号的唯一标识
     * @param grantType    填写为refresh_token
     * @param refreshToken 填写通过access_token获取到的refresh_token参数
     * @return
     */
    @RequestMappingEx(name = "sns/oauth2/refresh_token", method = HttpMethod.GET)
    WechatOpenIdRspDTO getAccessToken(@RequestParam(value = "appid") String appId,
                                      @RequestParam(value = "grant_type") String grantType,
                                      @RequestParam(value = "refresh_token") String refreshToken);

    /**
     * 检验授权凭证（access_token）是否有效
     *
     * @param accessToken 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
     * @param openId      用户的唯一标识
     * @return WechatErrorRspDTO
     */
    @RequestMappingEx(name = "sns/auth", method = HttpMethod.GET)
    WechatErrorRspDTO checkAccessToken(@RequestParam(value = "access_token") String accessToken,
                                       @RequestParam(value = "openid") String openId);

    /**
     * 拉取用户信息(网页授权需scope为 snsapi_userinfo)
     *
     * @param accessToken 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
     * @param openId      用户的唯一标识
     * @param lang        返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语
     * @return WechatUserInfoRspDTO
     */
    @RequestMappingEx(name = "sns/userinfo", method = HttpMethod.GET)
    WechatUserInfoRspDTO getUserInfo(@RequestParam(value = "access_token") String accessToken,
                                     @RequestParam(value = "openid") String openId,
                                     @RequestParam(value = "lang") String lang);

    /**
     * 获取其他接口调用需要的access_token
     * https://developers.weixin.qq.com/doc/offiaccount/Basic_Information/Get_access_token.html
     *
     * @param appId     第三方用户唯一凭证
     * @param secret    第三方用户唯一凭证密钥，即appsecret
     * @param grantType 获取access_token填写client_credential
     * @return
     */
    @RequestMappingEx(name = "cgi-bin/token", method = HttpMethod.GET)
    WechatAccessTokenRspDTO getToken(@RequestParam(value = "appid") String appId,
                                     @RequestParam(value = "secret") String secret,
                                     @RequestParam(value = "grant_type") String grantType);

}
