package org.ghost.springboot2.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;

/**
 * @author 01
 */
public class WechatMsgTemplateDTO implements Serializable {
    private static final long serialVersionUID = 4449702118986737851L;

    /**
     * 接收方的openId
     */
    private String toUser;
    /**
     * 消息模板ID
     */
    private String templateId;
    /**
     * 点击消息跳转的链接
     */
    private String url;
    /**
     * 小程序配置，没有就不传
     */
    private WechatMsgTemplateDTO.MiniProgram miniProgram;
    /**
     * 消息内容
     */
    private Map<String, MsgDataItem> data;


    @JsonProperty(value = "touser")

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    @JsonProperty(value = "template_id")
    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty(value = "miniprogram")
    public MiniProgram getMiniProgram() {
        return miniProgram;
    }

    public void setMiniProgram(MiniProgram miniProgram) {
        this.miniProgram = miniProgram;
    }

    public Map<String, MsgDataItem> getData() {
        return data;
    }

    public void setData(Map<String, MsgDataItem> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "WechatMsgTemplateDTO{" +
                "toUser='" + toUser + '\'' +
                ", templateId='" + templateId + '\'' +
                ", url='" + url + '\'' +
                ", miniProgram=" + miniProgram +
                ", data=" + data +
                '}';
    }

    public static class MiniProgram implements Serializable {
        private static final long serialVersionUID = -3853753646949156847L;

        private String appId;

        private String pagePath;

        @JsonProperty(value = "appid")
        public String getAppId() {
            return appId;
        }

        public void setAppId(String appId) {
            this.appId = appId;
        }

        @JsonProperty(value = "pagepath")
        public String getPagePath() {
            return pagePath;
        }

        public void setPagePath(String pagePath) {
            this.pagePath = pagePath;
        }

        @Override
        public String toString() {
            return "MiniProgram{" +
                    "appId='" + appId + '\'' +
                    ", pagePath='" + pagePath + '\'' +
                    '}';
        }
    }

    public static class MsgDataItem implements Serializable {
        private static final long serialVersionUID = 2098744199550366493L;
        private String value;
        private String color = "#173177";

        public MsgDataItem() {
        }

        public MsgDataItem(String value) {
            this.value = value;
        }

        public MsgDataItem(String value, String color) {
            this.value = value;
            this.color = color;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        @Override
        public String toString() {
            return "MsgDataItem{" +
                    "value='" + value + '\'' +
                    ", color='" + color + '\'' +
                    '}';
        }
    }
}
