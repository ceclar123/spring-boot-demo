package org.ghost.springboot2.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author 01
 */
public class WechatAccessTokenRspDTO extends WechatErrorRspDTO {
    private static final long serialVersionUID = 1505840124741732221L;

    /**
     * 获取到的凭证
     */
    private String accessToken;

    /**
     * 凭证有效时间，单位：秒
     */
    private Integer expiresIn;

    public String getAccessToken() {
        return accessToken;
    }

    @JsonProperty(value = "access_token")
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    @JsonProperty(value = "expires_in")
    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }

    @Override
    public String toString() {
        return "WechatAccessTokenRspDTO{" +
                "accessToken='" + accessToken + '\'' +
                ", expiresIn=" + expiresIn +
                ", errCode=" + errCode +
                ", errMsg='" + errMsg + '\'' +
                '}';
    }
}
