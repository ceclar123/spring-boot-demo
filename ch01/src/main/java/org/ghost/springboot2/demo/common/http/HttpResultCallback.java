package org.ghost.springboot2.demo.common.http;

public interface HttpResultCallback<T> {
    /**
     * 结果处理
     *
     * @param result
     */
    void handle(T result);
}
