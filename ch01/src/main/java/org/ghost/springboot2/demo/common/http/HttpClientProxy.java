package org.ghost.springboot2.demo.common.http;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.ghost.springboot2.demo.common.SpringUtil;
import org.ghost.springboot2.demo.common.constant.MarkConstant;
import org.ghost.springboot2.demo.common.http.annotation.HttpClientEx;
import org.ghost.springboot2.demo.common.http.annotation.RequestMappingEx;
import org.ghost.springboot2.demo.util.UriUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public final class HttpClientProxy<T> implements InvocationHandler {
    private static final Logger logger = LoggerFactory.getLogger(HttpClientProxy.class);

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Class clz = method.getDeclaringClass();
        if (clz != null) {
            HttpClientEx typeAnno = (HttpClientEx) (clz.getAnnotation(HttpClientEx.class));
            RequestMappingEx methodAnno = method.getAnnotation(RequestMappingEx.class);
            if (typeAnno != null && methodAnno != null) {
                List<ParameterObj> parameterObjList = this.getParas(method, args);
                HttpRequestBuilder httpRequestBuilder = this.constructHttpRequestBuilder(typeAnno, methodAnno, parameterObjList);
                if (httpRequestBuilder != null) {
                    if (method.getGenericReturnType() instanceof ParameterizedType) {
                        return HttpClientHelper.invoke(httpRequestBuilder, (ParameterizedType) method.getGenericReturnType());
                    } else if (void.class != method.getReturnType()) {
                        return HttpClientHelper.invoke(httpRequestBuilder, method.getReturnType());
                    } else {
                        HttpClientHelper.invoke(httpRequestBuilder);
                    }
                }
            }
        }

        return null;
    }

    private HttpRequestBuilder constructHttpRequestBuilder(HttpClientEx typeAnno, RequestMappingEx methodAnno, List<ParameterObj> parameterObjList) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final Class<HttpRequestBuilder> configClz = typeAnno.config();
        Constructor<HttpRequestBuilder> constructor = configClz.getConstructor(HttpMethod.class, String.class);
        if (constructor != null) {
            final Environment environment = SpringUtil.getEnvironment();
            //处理url参数
            final String fullUrl = this.getFullUrl(typeAnno, methodAnno, environment, parameterObjList);
            HttpRequestBuilder httpRequestBuilder = constructor.newInstance(methodAnno.method(), fullUrl);

            //header处理
            if (StringUtils.isNotBlank(methodAnno.contentType())) {
                httpRequestBuilder.addHeader(HttpHeaders.CONTENT_TYPE, methodAnno.contentType());
            } else {
                httpRequestBuilder.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
            }
            if (ArrayUtils.isNotEmpty(methodAnno.headers())) {
                for (String item : methodAnno.headers()) {
                    if (StringUtils.isNotBlank(item)) {
                        String[] array = this.getFormatString(environment, item).split("=");
                        if (ArrayUtils.isNotEmpty(array) && array.length == 2) {
                            String key = array[0];
                            String value = array[1];
                            if (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(value)) {
                                httpRequestBuilder.addHeader(key, value);
                            }
                        }
                    }
                }
            }

            //RequestBody处理
            if (CollectionUtils.isNotEmpty(parameterObjList)) {
                parameterObjList.stream()
                        .filter(Objects::nonNull)
                        .filter(it -> ArrayUtils.isNotEmpty(it.getAnnotations()) && Stream.of(it.getAnnotations()).anyMatch(f -> f instanceof RequestBody))
                        .findFirst()
                        .ifPresent((val) -> httpRequestBuilder.addBody(val.getValue()));
            }

            return httpRequestBuilder;
        }

        return null;
    }

    private String getFullUrl(HttpClientEx typeAnno, RequestMappingEx methodAnno, Environment environment) {
        if (typeAnno != null && methodAnno != null) {
            String fullUrl = this.getFormatString(environment, typeAnno.hostName() + "/" + methodAnno.name());
            //处理注解中的参数
            if (ArrayUtils.isNotEmpty(methodAnno.params())) {
                String annoParaUrl = Stream.of(methodAnno.params())
                        .filter(StringUtils::isNotBlank)
                        .map(it -> getFormatString(environment, it))
                        .reduce((a, b) -> {
                            if (StringUtils.isNotBlank(a)) {
                                return a + "&" + b;
                            } else {
                                return b;
                            }
                        }).orElse(null);
                if (StringUtils.isNotBlank(annoParaUrl)) {
                    if (fullUrl.contains(MarkConstant.QUESTION_MARK)) {
                        fullUrl = fullUrl + "&" + annoParaUrl;
                    } else {
                        fullUrl = fullUrl + MarkConstant.QUESTION_MARK + annoParaUrl;
                    }
                }
            }

            return fullUrl;
        }

        return "";
    }

    private String getFullUrl(String url, List<ParameterObj> parameterObjList) {
        if (StringUtils.isNotBlank(url) && CollectionUtils.isNotEmpty(parameterObjList)) {
            //处理PathVariable
            for (ParameterObj parameterObj : parameterObjList) {
                if (parameterObj != null && ArrayUtils.isNotEmpty(parameterObj.getAnnotations())) {
                    String name = Stream.of(parameterObj.getAnnotations()).filter(Objects::nonNull).filter(it -> it instanceof PathVariable).map(it -> ((PathVariable) it).name()).findFirst().orElse(null);
                    if (StringUtils.isNotBlank(name)) {
                        url = url.replace("{" + name + MarkConstant.RIGHT_BRACES_MARK, String.valueOf(parameterObj.getValue()));
                    }
                }
            }

            //处理RequestParam
            String paraUrl = parameterObjList.stream()
                    .filter(Objects::nonNull)
                    .filter(it -> ArrayUtils.isNotEmpty(it.getAnnotations()))
                    .map(it -> Stream.of(it.getAnnotations())
                            .filter(Objects::nonNull)
                            .filter(f -> f instanceof RequestParam)
                            .map(f -> {
                                RequestParam requestParam = (RequestParam) f;
                                String key = requestParam.name();
                                if (StringUtils.isBlank(key)) {
                                    key = requestParam.value();
                                }
                                if (it.getValue() == null || hasBaseType(it.getValue())) {
                                    if (StringUtils.isNotBlank(key)) {
                                        try {
                                            return key + "=" + URLEncoder.encode(String.valueOf(it.getValue()), "UTF-8");
                                        } catch (UnsupportedEncodingException e) {
                                            logger.warn("*****HttpClientProxy.getFullUrl编码失败, 错误信息: {}, {}", e.getMessage(), e);
                                        }
                                    }
                                    return "";
                                } else if (it.getValue() instanceof List) {
                                    if (StringUtils.isNotBlank(key)) {
                                        List<Object> list = (List<Object>) (it.getValue());
                                        String value = list.stream().filter(Objects::nonNull).map(Object::toString).reduce("", (a, b) -> StringUtils.isBlank(a) ? b : a + "," + b);
                                        return key + "=" + value;
                                    }
                                    return "";
                                } else if (it.getValue().getClass().isArray()) {
                                    if (StringUtils.isNotBlank(key)) {
                                        Object[] array = (Object[]) (it.getValue());
                                        String value = Stream.of(array).filter(Objects::nonNull).map(Object::toString).reduce("", (a, b) -> StringUtils.isBlank(a) ? b : a + "," + b);
                                        return key + "=" + value;
                                    }
                                    return "";
                                } else {
                                    String value = UriUtil.urlParaEncode(it.getValue());
                                    if (StringUtils.isNotBlank(value) && !value.contains(MarkConstant.EQUAL_MARK)) {
                                        return key + "=" + value;
                                    }
                                    return value;
                                }
                            })
                            .filter(StringUtils::isNotBlank)
                            .reduce((a, b) -> {
                                if (StringUtils.isNotBlank(a)) {
                                    return a + "&" + b;
                                } else {
                                    return b;
                                }
                            }).orElse(null))
                    .filter(StringUtils::isNotBlank)
                    .reduce((a, b) -> {
                        if (StringUtils.isNotBlank(a)) {
                            return a + "&" + b;
                        } else {
                            return b;
                        }
                    }).orElse(null);
            if (StringUtils.isNotBlank(paraUrl)) {
                if (url.contains(MarkConstant.QUESTION_MARK)) {
                    url = url + "&" + paraUrl;
                } else {
                    url = url + MarkConstant.QUESTION_MARK + paraUrl;
                }
            }

            //处理没有任何注解的参数
            String noAnnoUrl = parameterObjList.stream()
                    .filter(Objects::nonNull)
                    .filter(it -> ArrayUtils.isEmpty(it.getAnnotations()))
                    .filter(it -> it.getValue() != null)
                    .filter(it -> !hasBaseType(it.getValue()))
                    .map(UriUtil::urlParaEncode)
                    .filter(StringUtils::isNotBlank)
                    .reduce((a, b) -> {
                        if (StringUtils.isNotBlank(a)) {
                            return a + "&" + b;
                        } else {
                            return b;
                        }
                    }).orElse(null);
            if (StringUtils.isNotBlank(noAnnoUrl)) {
                if (url.contains(MarkConstant.QUESTION_MARK)) {
                    url = url + "&" + noAnnoUrl;
                } else {
                    url = url + MarkConstant.QUESTION_MARK + noAnnoUrl;
                }
            }
        }

        return url;
    }

    private String getFullUrl(HttpClientEx typeAnno, RequestMappingEx methodAnno, Environment environment, List<ParameterObj> parameterObjList) {
        String url = this.getFullUrl(typeAnno, methodAnno, environment);
        return this.getFullUrl(url, parameterObjList);
    }

    private List<ParameterObj> getParas(Method method, Object[] args) {
        if (ArrayUtils.isNotEmpty(args)) {
            Parameter[] parameters = method.getParameters();
            if (ArrayUtils.isNotEmpty(parameters) && ArrayUtils.isNotEmpty(args) && parameters.length == args.length) {
                List<ParameterObj> rtnList = new ArrayList<ParameterObj>();
                for (int i = 0; i < parameters.length; i++) {
                    rtnList.add(new ParameterObj(parameters[i].getName(), args[i], parameters[i].getAnnotations()));
                }

                return rtnList;
            }
        }

        return null;
    }

    private boolean hasBaseType(Object object) {
        if (object != null) {
            return hasBaseType(object.getClass());
        }

        return true;
    }

    private <T extends Object> boolean hasBaseType(Class<T> clz) {
        if (clz == Byte.class || clz == Character.class || clz == Boolean.class
                || clz == Short.class || clz == Integer.class || clz == Long.class
                || clz == Float.class || clz == Double.class || clz == String.class) {
            return true;
        }

        return false;
    }

    /**
     * {a.b.c}  => XXX
     *
     * @param environment
     * @param input
     * @return
     */
    private String getFormatString(Environment environment, String input) {
        if (environment != null && StringUtils.isNotBlank(input)) {
            if (input.contains(MarkConstant.DOLLAR_LEFT_BRACES_MARK) && input.contains(MarkConstant.RIGHT_BRACES_MARK)) {
                //第一个匹配的
                int begin = input.indexOf(MarkConstant.DOLLAR_LEFT_BRACES_MARK);
                int end = input.indexOf(MarkConstant.RIGHT_BRACES_MARK, begin);
                String oldVal = input.substring(begin + 2, end);
                String newVal = environment.getProperty(oldVal, "");

                //继续查找后面的
                return getFormatString(environment, input.replace(MarkConstant.DOLLAR_LEFT_BRACES_MARK + oldVal + MarkConstant.RIGHT_BRACES_MARK, newVal));
            }
        }

        return input;
    }


    class ParameterObj {
        /**
         * 参数名称
         */
        private String name;
        /**
         * 参数值
         */
        private Object value;
        /**
         * 参数注解
         */
        private Annotation[] annotations;

        public ParameterObj() {
        }

        ParameterObj(String name, Object value, Annotation[] annotations) {
            this.name = name;
            this.value = value;
            this.annotations = annotations;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public Annotation[] getAnnotations() {
            return annotations;
        }

        public void setAnnotations(Annotation[] annotations) {
            this.annotations = annotations;
        }
    }
}
