package org.ghost.springboot2.demo.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ObjectUtil {
    private static final Logger logger = LoggerFactory.getLogger(ObjectUtil.class);

    public static Object getFieldValue(Object obj, String field) {
        Class<?> clazz = obj.getClass();
        Field f = null;
        Object fieldValue = null;
        try {
            f = clazz.getDeclaredField(field);
            f.setAccessible(true);
            fieldValue = f.get(obj);
        } catch (SecurityException | NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return fieldValue;
    }

    public static <T> boolean hasBaseType(Class<T> clz) {
        if (clz == Byte.class || clz == Character.class || clz == Boolean.class
                || clz == Short.class || clz == Integer.class || clz == Long.class
                || clz == Float.class || clz == Double.class || clz == String.class) {
            return true;
        }

        return false;
    }

    public static <T> boolean hasBaseType(Object obj) {
        if (obj instanceof Byte || obj instanceof Character || obj instanceof Boolean
                || obj instanceof Short || obj instanceof Integer || obj instanceof Long
                || obj instanceof Float || obj instanceof Double || obj instanceof String) {
            return true;
        }

        return false;
    }

    /**
     * 几种基本类型处理
     *
     * @param value
     * @param clz
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T getBaseTypeValue(String value, Class<T> clz) {
        if (StringUtils.isNotBlank(value)) {
            if (clz == Character.class) {
                char ch = value.toCharArray()[0];
                try {
                    Method method = clz.getMethod("valueOf", char.class);
                    if (method != null) {
                        return (T) method.invoke(clz.getClass(), ch);
                    }
                } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                    logger.error("*****ObjectUtil.getBaseTypeValue出现错误:" + e.getMessage(), e);
                }
            } else if (clz == Byte.class || clz == Boolean.class
                    || clz == Short.class || clz == Integer.class || clz == Long.class
                    || clz == Float.class || clz == Double.class) {
                try {
                    Method method = clz.getMethod("valueOf", String.class);
                    if (method != null) {
                        return (T) method.invoke(clz.getClass(), value);
                    }
                } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                    logger.error("*****ObjectUtil.getBaseTypeValue出现错误:" + e.getMessage(), e);
                }
            } else if (clz == String.class) {
                return (T) value;
            }
        }

        return null;
    }
}