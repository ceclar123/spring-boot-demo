package org.ghost.springboot2.demo.controller;

import org.ghost.springboot2.demo.dto.RspDTO;


abstract public class BaseController {
    protected RspDTO render() {
        return new RspDTO(true);
    }

    protected RspDTO render(Object object) {
        return new RspDTO(object);
    }

    protected RspDTO render(String code, String message) {
        return new RspDTO(code, message);
    }
}
