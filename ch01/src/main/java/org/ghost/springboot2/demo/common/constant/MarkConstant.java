package org.ghost.springboot2.demo.common.constant;

/**
 * 符号常量信息
 */
public class MarkConstant {
    /**
     * 美元符号
     */
    public static final String DOLLAR_MARK = "$";
    /**
     * 美元符号左大括号
     */
    public static final String DOLLAR_LEFT_BRACES_MARK = "${";

    /**
     * 左大括号
     */
    public static final String LEFT_BRACES_MARK = "{";
    /**
     * 右大括号
     */
    public static final String RIGHT_BRACES_MARK = "}";

    /**
     * 问号
     */
    public static final String QUESTION_MARK = "?";
    /**
     * 等于符号
     */
    public static final String EQUAL_MARK = "=";
    /**
     * https
     */
    public static final String HTTPS_MARK = "https";
    /**
     * http
     */
    public static final String HTTP_MARK = "http";
}
