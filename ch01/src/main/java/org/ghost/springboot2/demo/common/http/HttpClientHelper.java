package org.ghost.springboot2.demo.common.http;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.auth.AuthScope;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;
import org.ghost.springboot2.demo.common.SpringUtil;
import org.ghost.springboot2.demo.common.constant.EnvEnum;
import org.ghost.springboot2.demo.dto.Tuple2;
import org.ghost.springboot2.demo.util.FileUtil;
import org.ghost.springboot2.demo.util.JacksonUtil;
import org.ghost.springboot2.demo.util.MimeTypeUtil;
import org.ghost.springboot2.demo.util.ObjectUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.StreamUtils;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.*;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

public class HttpClientHelper {

    private static final Logger logger = LoggerFactory.getLogger(HttpClientHelper.class);
    private static final String NEW_LINE = "\r\n";
    private static final String BOUNDARY_PREFIX = "--";

    /**
     * 连接超时
     */
    private static int CONNECT_TIMEOUT = 3000;
    /**
     * 连接请求超时
     */
    private static int CONNECT_REQUEST_TIMEOUT = 2000;
    /**
     * 设置等待数据超时时间20秒钟(接口太慢可以设置大一点)
     */
    private static int SOCKET_TIMEOUT = 20000;
    /**
     * 超时配置
     */
    public static RequestConfig REQUEST_CONFIG = RequestConfig.custom()
            .setConnectTimeout(CONNECT_TIMEOUT)
            .setConnectionRequestTimeout(CONNECT_REQUEST_TIMEOUT)
            .setSocketTimeout(Objects.equals(EnvEnum.PROD, SpringUtil.getEnvEnum()) ? SOCKET_TIMEOUT : 2 * SOCKET_TIMEOUT).build();


    /**
     * 接口调用时间异常阈值
     */
    private static final long THRESHOLD_THREE_SECOND_FOR_MILLISECOND = 3000;
    private static TrustManager X509TrustManagerEx = new X509TrustManager() {

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType)
                throws CertificateException {
        }

        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType)
                throws CertificateException {
        }
    };

    /**
     * 数据分隔线
     *
     * @return
     */
    private static String getBoundary() {
        String boundary = "----WebKitFormBoundary8RPsnNWd31e38HLk" + System.currentTimeMillis();
        return boundary;
    }

    public static CloseableHttpClient createSSLClientDefault() {
        try {
            SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
                //信任所有
                @Override
                public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    return true;
                }
            }).build();
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext);
            return HttpClients.custom().setSSLSocketFactory(sslsf).build();
        } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
            logger.error("*****createSSLClientDefault出现异常:" + e.getMessage(), e);
        }

        return HttpClients.createDefault();
    }

    public static byte[] downloadFile(String url) {
        int byteRead = 0;
        HttpURLConnection conn = createHttpURLConnection(url);
        if (conn != null) {
            try (ByteArrayOutputStream fs = new ByteArrayOutputStream(2048);
                 InputStream inStream = conn.getInputStream();) {
                byte[] buffer = new byte[1204];
                while ((byteRead = inStream.read(buffer)) != -1) {
                    fs.write(buffer, 0, byteRead);
                }
                return fs.toByteArray();
            } catch (Exception e) {
                logger.error("*****HttpClientHelper.downloadFile下载文件出现异常:" + e.getMessage(), e);
            }
        }

        return null;
    }

    /**
     * 模拟表单文件上传
     *
     * @param url
     * @param params
     * @param file
     * @param t
     * @param <T>
     * @return
     */
    public static <T> T doPostFile(String url, Map<String, Object> params, File file, Class<T> t) throws IOException {
        HttpURLConnection httpConn = createHttpURLConnection(url);
        return doPostFile(httpConn, params, file, t);
    }

    /**
     * 模拟表单文件上传
     *
     * @param url
     * @param params
     * @param fileName
     * @param data
     * @param t
     * @param <T>
     * @return
     */
    public static <T> T doPostFile(String url, Map<String, Object> params, String fileName, byte[] data, Class<T> t) {
        HttpURLConnection httpConn = createHttpURLConnection(url);
        return doPostFile(httpConn, params, fileName, data, t);
    }

    private static HttpURLConnection createHttpURLConnection(String url) {
        HttpURLConnection httpConn = null;
        if (StringUtils.isBlank(url)) {
            return null;
        }

        if (url.toLowerCase().startsWith("https")) {
            try {
                //设置SSLContext
                SSLContext sslContext = SSLContext.getInstance("TLS");
                sslContext.init(null, new TrustManager[]{X509TrustManagerEx}, null);

                URL requestUrl = new URL(url);
                HttpsURLConnection conn = (HttpsURLConnection) requestUrl.openConnection();

                //设置套接工厂
                conn.setSSLSocketFactory(sslContext.getSocketFactory());

                httpConn = conn;
                httpConn.setConnectTimeout(CONNECT_TIMEOUT);
                httpConn.setReadTimeout(SOCKET_TIMEOUT);
            } catch (Exception e) {
                logger.error("*****HttpClientHelper.doPostFile创建HttpsURLConnection出现异常:" + e.getMessage(), e);
            }
        } else {
            try {
                URL requestUrl = new URL(url);
                httpConn = (HttpURLConnection) requestUrl.openConnection();
                httpConn.setConnectTimeout(CONNECT_TIMEOUT);
                httpConn.setReadTimeout(SOCKET_TIMEOUT);
            } catch (Exception e) {
                logger.error("*****HttpClientHelper.doPostFile创建HttpURLConnection出现异常:" + e.getMessage(), e);
            }
        }

        return httpConn;
    }

    private static <T> T doPostFile(HttpURLConnection conn, Map<String, Object> params, File file, Class<T> t) throws
            IOException {
        String fileName = file.getName();
        byte[] data = org.aspectj.util.FileUtil.readAsByteArray(file);
        return doPostFile(conn, params, fileName, data, t);
    }

    private static <T> T doPostFile(HttpURLConnection conn, Map<String, Object> params, String fileName, byte[] data,
                                    Class<T> t) {
        try {
            // 定义数据分隔线
            String boundary = getBoundary();
            StringBuilder stringBuffer = new StringBuilder();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);

            // 设置请求头参数
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; zh-CN; rv:1.9.2.6)");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("Charsert", "UTF-8");
            conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

            OutputStream out = new DataOutputStream(conn.getOutputStream());
            //其他参数
            if (params != null) {
                StringBuilder strBuf = new StringBuilder();
                for (Map.Entry<String, Object> item : params.entrySet()) {
                    String inputName = item.getKey();
                    String inputValue = item.getValue() == null ? "" : item.getValue().toString();
                    if (inputValue == null) {
                        continue;
                    }
                    strBuf.append(NEW_LINE).append("--").append(boundary).append(NEW_LINE);
                    strBuf.append("Content-Disposition: form-data; name=\"").append(inputName).append("\"").append(NEW_LINE).append(NEW_LINE);
                    strBuf.append(inputValue);
                }
                stringBuffer.append(strBuf.toString());
                out.write(strBuf.toString().getBytes());
            }
            // 文件参数
            if (data != null) {
                //没有传入文件类型，同时根据文件获取不到类型，默认采用application/octet-stream
                String suffixName = FileUtil.getSuffixName(fileName);
                String contentType = MimeTypeUtil.getMimeType(suffixName);
                if (StringUtils.isBlank(contentType)) {
                    contentType = "application/octet-stream";
                }
                StringBuilder strBuf = new StringBuilder();
                strBuf.append(NEW_LINE).append("--").append(boundary).append(NEW_LINE);
                strBuf.append("Content-Disposition: form-data; name=\"file\"; filename=\"").append(fileName).append("\"").append(NEW_LINE);
                strBuf.append("Content-Type:").append(contentType).append(NEW_LINE).append(NEW_LINE);
                stringBuffer.append(strBuf.toString());
                out.write(strBuf.toString().getBytes());
                stringBuffer.append(new String(data));
                out.write(data, 0, data.length);
            }

            // 定义最后数据分隔线，即--加上BOUNDARY再加上--。
            byte[] endData = (NEW_LINE + BOUNDARY_PREFIX + boundary + BOUNDARY_PREFIX + NEW_LINE).getBytes();
            // 写上结尾标识
            stringBuffer.append(NEW_LINE + BOUNDARY_PREFIX).append(boundary).append(BOUNDARY_PREFIX).append(NEW_LINE);
            out.write(endData);
            out.flush();
            out.close();

            //读取返回数据
            String res = org.apache.commons.io.IOUtils.toString(conn.getInputStream(), "UTF-8");
            conn.disconnect();

            logger.info("*****HttpClientHelper.doPostFile请求url:{},request:{},response:{}", conn.getURL().toString(), JacksonUtil.useDefaultMapper().toJson(params), res);
            return JacksonUtil.nonEmptyMapper().fromJson(res, t);
        } catch (Exception e) {
            logger.error("*****HttpClientHelper.doPostFile出现异常,请求url:{},request:{},错误信息:{},{}", conn.getURL().toString(), JacksonUtil.useDefaultMapper().toJson(params), e.getMessage(), e);
        }

        return null;
    }

    public static Map doPost(String url, Map<String, Object> params) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = postForm(url, params);
        String res = invoke(httpClient, httpPost);
        return parseType(res, Map.class);
    }

    public static <T> T doPost(String url, Map<String, Object> params, Class<T> t) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = postForm(url, params);
        String res = invoke(httpClient, httpPost);
        return JacksonUtil.nonEmptyMapper().fromJson(res, t);
    }

    public static <T> T doPostWithJson(String url, Object params, Class<T> t) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = postJson(url, params);
        String res = invoke(httpClient, httpPost);
        return JacksonUtil.nonEmptyMapper().fromJson(res, t);
    }

    /**
     * doPostWithJson
     *
     * @param url
     * @param params
     * @param typeReference TypeReference<T> typeReference = new TypeReference<T>() {};
     * @param <T>
     * @return
     */
    public static <T> T doPostWithJson(String url, Object params, TypeReference<T> typeReference) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = postJson(url, params);
        String res = invoke(httpClient, httpPost);
        return JacksonUtil.nonEmptyMapper().fromJson(res, typeReference);
    }

    /**
     * doPostWithJson
     *
     * @param url
     * @param params
     * @param parameterizedType 例如:ParameterizedType parameterizedType = ParameterizedTypeImpl.make(HttpRspDTO.class, new Type[]{CustomerKeyInfoRspDTO.class}, null);
     * @param <T>
     * @return
     */
    public static <T> T doPostWithJson(String url, Object params, ParameterizedType parameterizedType) {
        TypeReference<T> typeReference = new TypeReference<T>() {
            @Override
            public Type getType() {
                return parameterizedType;
            }
        };
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = postJson(url, params);
        String res = invoke(httpClient, httpPost);
        return JacksonUtil.nonEmptyMapper().fromJson(res, typeReference);
    }

    /**
     * doPostWithJson
     *
     * @param url
     * @param params
     * @param rawType   ParameterizedTypeImpl.make 第一个参数
     * @param ownerType ParameterizedTypeImpl.make 第三个参数
     * @param argType   ParameterizedTypeImpl.make 第二个参数
     * @param <T>
     * @return
     */
    public static <T> T doPostWithJson(String url, Object params, Class rawType, Type ownerType, Type... argType) {
        ParameterizedType parameterizedType = ParameterizedTypeImpl.make(rawType, argType, ownerType);
        TypeReference<T> typeReference = new TypeReference<T>() {
            @Override
            public Type getType() {
                return parameterizedType;
            }
        };

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = postJson(url, params);
        String res = invoke(httpClient, httpPost);
        return JacksonUtil.nonEmptyMapper().fromJson(res, typeReference);
    }

    public static <T> T doGet(String url, Map<String, String> params, Class<T> t) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = getForm(url, params);
        String res = invoke(httpClient, httpGet);
        return JacksonUtil.nonEmptyMapper().fromJson(res, t);
    }

    public static Map doGet(String url, Map<String, String> params) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = getForm(url, params);
        String res = invoke(httpClient, httpGet);
        return JacksonUtil.nonEmptyMapper().fromJson(res, Map.class);
    }

    /**
     * doGet
     *
     * @param url
     * @param params
     * @param typeReference TypeReference<T> typeReference = new TypeReference<T>() {};
     * @param <T>
     * @return
     */
    public static <T> T doGet(String url, Map<String, String> params, TypeReference<T> typeReference) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = getForm(url, params);
        String res = invoke(httpClient, httpGet);
        return JacksonUtil.nonEmptyMapper().fromJson(res, typeReference);
    }

    /**
     * doGet
     *
     * @param url
     * @param params
     * @param parameterizedType 例如:ParameterizedType parameterizedType = ParameterizedTypeImpl.make(HttpRspDTO.class, new Type[]{CustomerKeyInfoRspDTO.class}, null);
     * @param <T>
     * @return
     */
    public static <T> T doGet(String url, Map<String, String> params, ParameterizedType parameterizedType) {
        TypeReference<T> typeReference = new TypeReference<T>() {
            @Override
            public Type getType() {
                return parameterizedType;
            }
        };
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = getForm(url, params);
        String res = invoke(httpClient, httpGet);
        return JacksonUtil.nonEmptyMapper().fromJson(res, typeReference);
    }

    /**
     * doGet
     *
     * @param url
     * @param params
     * @param rawType   ParameterizedTypeImpl.make 第一个参数
     * @param ownerType ParameterizedTypeImpl.make 第三个参数
     * @param argType   ParameterizedTypeImpl.make 第二个参数
     * @param <T>
     * @return
     */
    public static <T> T doGet(String url, Map<String, String> params, Class rawType, Type ownerType, Type... argType) {
        ParameterizedType parameterizedType = ParameterizedTypeImpl.make(rawType, argType, ownerType);
        TypeReference<T> typeReference = new TypeReference<T>() {
            @Override
            public Type getType() {
                return parameterizedType;
            }
        };

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = getForm(url, params);
        String res = invoke(httpClient, httpGet);
        return JacksonUtil.nonEmptyMapper().fromJson(res, typeReference);
    }

    private static HttpPost postForm(String url, Map<String, Object> params) {
        HttpPost httpPost = new HttpPost(url);
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();

        Set<String> keySet = params.keySet();
        for (String key : keySet) {
            Object obj = params.get(key);
            if (obj != null) {
                nvps.add(new BasicNameValuePair(key, obj.toString()));
            }
        }

        try {
            httpPost.setConfig(REQUEST_CONFIG);
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
        } catch (Exception e) {
            logger.error("*****HttpClientHelper.postForm出现错误:" + e.getMessage(), e);
        }

        return httpPost;
    }

    private static HttpGet getForm(String url, Map<String, String> params) {
        StringBuilder sb = new StringBuilder();
        if (MapUtils.isNotEmpty(params)) {
            try {
                for (Map.Entry<String, String> item : params.entrySet()) {
                    String key = URLEncoder.encode(item.getKey(), "utf-8");
                    String value = URLEncoder.encode(item.getValue(), "utf-8");
                    if (StringUtils.isNotBlank(key)) {
                        sb.append("&").append(key).append("=").append(value);
                    }
                }
            } catch (Exception e) {
                logger.error("*****getForm出现错误:" + e.getMessage(), e);
            }
        }
        if (sb.length() > 0) {
            if (url.indexOf('?') > 0) {
                url += sb.toString();
            } else {
                url += "?" + sb.substring(1);
            }
        }
        HttpGet httpGet = new HttpGet(url);
        httpGet.setConfig(REQUEST_CONFIG);

        return httpGet;
    }

    private static HttpPost postJson(String url, Object obj) {
        HttpPost httpPost = new HttpPost(url);
        try {
            httpPost.setConfig(REQUEST_CONFIG);
            httpPost.setHeader(org.springframework.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
            ObjectMapper objectMapper = new ObjectMapper();
            StringEntity se = new StringEntity(objectMapper.writeValueAsString(obj), Charset.forName("UTF-8"));
            httpPost.setEntity(se);
        } catch (Exception e) {
            logger.error("*****postJson执行IO异常,错误信息: {}, {}", e.getMessage(), e);
        }

        return httpPost;
    }

    private static HttpPut putJson(String url, Object obj) {
        HttpPut httpPut = new HttpPut(url);
        try {
            httpPut.setConfig(REQUEST_CONFIG);
            httpPut.setHeader(org.springframework.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
            ObjectMapper objectMapper = new ObjectMapper();
            StringEntity se = new StringEntity(objectMapper.writeValueAsString(obj));
            httpPut.setEntity(se);
        } catch (Exception e) {
            logger.error("*****putJson执行IO异常,错误信息: {}, {}", e.getMessage(), e);
        }

        return httpPut;
    }

    private static String invoke(CloseableHttpClient httpClient, HttpUriRequest httpPost) {
        long begin = System.currentTimeMillis();

        HttpResponse response = sendRequest(httpClient, httpPost);
        String body = parseResponse(response);

        long duration = System.currentTimeMillis() - begin;

        if (httpPost instanceof HttpPost) {
            HttpPost temp = (HttpPost) httpPost;
            try {
                String requestBody = "";
                if (temp.getEntity() != null && temp.getEntity().getContent() != null) {
                    requestBody = StreamUtils.copyToString(temp.getEntity().getContent(), Charset.forName("UTF-8"));
                }
                logger.info("*****method:{},url:{},time:{},request:{},response:{}", httpPost.getMethod(), httpPost.getURI().toString(), duration / 1000.0, requestBody, body);
                if (duration >= THRESHOLD_THREE_SECOND_FOR_MILLISECOND) {
                    logger.info("*****HttpClientHelper接口太慢method:{},url:{},time:{},request:{},response:{}", httpPost.getMethod(), httpPost.getURI().toString(), duration / 1000.0, requestBody, "");
                    writePoint(httpPost.getURI().toString(), duration, httpPost.getMethod());
                }
            } catch (IOException e) {
                logger.error("*****invoke出现错误,错误信息: {}, {}", e.getMessage(), e);
            }
        } else {
            logger.info("*****method:{},url:{},time:{},response:{}", httpPost.getMethod(), httpPost.getURI().toString(), duration / 1000.0, body);
            if (duration >= THRESHOLD_THREE_SECOND_FOR_MILLISECOND) {
                logger.info("*****HttpClientHelper接口太慢method:{},url:{},time:{}", httpPost.getMethod(), httpPost.getURI().toString(), duration / 1000.0);
                writePoint(httpPost.getURI().toString(), duration, httpPost.getMethod());
            }
        }

        return body;
    }

    private static HttpResponse sendRequest(CloseableHttpClient httpClient, HttpUriRequest httpPost) {
        HttpResponse response = null;
        try {
            response = httpClient.execute(httpPost);
        } catch (IOException e) {
            logger.error("*****HTTP 执行IO异常,错误信息: {}, {}", e.getMessage(), e);
            parseException(e, httpPost.getURI().toString(), httpPost.getMethod());
        }

        return response;
    }

    public static String parseResponse(HttpResponse response) {
        String body = null;
        try {
            body = EntityUtils.toString(response.getEntity());
        } catch (ParseException | IOException e) {
            logger.error("*****HttpClientHelper.parseResponse出现错误:" + e.getMessage(), e);
        }

        return body;
    }

    public static <T> T parseType(String mess, Class<T> t) {
        try {
            return JacksonUtil.useDefaultMapper().fromJson(mess, t);
        } catch (Exception e) {
            logger.error("*****HttpClientHelper.parseType出现错误:" + e.getMessage(), e);
        }
        return null;
    }

    private static CloseableHttpClient getHttpClient(HttpRequestBuilder builder) {
        if (builder.getCredentials() != null) {
            HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
            CredentialsProvider provider = new BasicCredentialsProvider();
            provider.setCredentials(AuthScope.ANY, builder.getCredentials());
            httpClientBuilder.setDefaultCredentialsProvider(provider);
            httpClientBuilder.setDefaultRequestConfig(REQUEST_CONFIG);

            return httpClientBuilder.build();
        } else {
            return HttpClients.createDefault();
        }
    }

    /**
     * invoke
     *
     * @param builder
     * @return
     */
    public static void invoke(HttpRequestBuilder builder) {
        long begin = System.currentTimeMillis();
        Tuple2<Integer, String> tupleRsp = new Tuple2<Integer, String>(HttpStatus.OK.value(), "");
        try (CloseableHttpClient httpClient = getHttpClient(builder)) {
            try (CloseableHttpResponse response = httpClient.execute(builder.getHttpRequestBase())) {
                tupleRsp = getResponse(builder, response);
            }
        } catch (IOException e) {
            logger.error("*****HttpClientHelper.invoke出现错误:" + e.getMessage(), e);
            parseException(e, builder.getHttpRequestBase());
        } finally {
            writeLog(builder.getHttpRequestBase(), begin, tupleRsp);
        }
    }

    /**
     * invoke
     *
     * @param builder
     * @param cls
     * @param <T>
     * @return
     */
    public static <T> T invoke(HttpRequestBuilder builder, Class<T> cls) {
        long begin = System.currentTimeMillis();
        Tuple2<Integer, String> tupleRsp = new Tuple2<Integer, String>(HttpStatus.OK.value(), "");
        try (CloseableHttpClient httpClient = getHttpClient(builder)) {
            try (CloseableHttpResponse response = httpClient.execute(builder.getHttpRequestBase())) {
                tupleRsp = getResponse(builder, response);
                if (StringUtils.isNotBlank(tupleRsp.getSecond())) {
                    if (ObjectUtil.hasBaseType(cls)) {
                        return ObjectUtil.getBaseTypeValue(tupleRsp.getSecond(), cls);
                    } else {
                        return JacksonUtil.nonEmptyMapper().fromJson(tupleRsp.getSecond(), cls);
                    }
                }
            }
        } catch (IOException e) {
            logger.error("*****HttpClientHelper.invoke出现错误:" + e.getMessage(), e);
            parseException(e, builder.getHttpRequestBase());
        } finally {
            writeLog(builder.getHttpRequestBase(), begin, tupleRsp);
        }

        return null;
    }


    /**
     * invoke
     *
     * @param builder
     * @param typeReference TypeReference<T> typeReference = new TypeReference<T>() {};
     * @param <T>
     * @return
     */
    public static <T> T invoke(HttpRequestBuilder builder, TypeReference<T> typeReference) {
        long begin = System.currentTimeMillis();
        Tuple2<Integer, String> tupleRsp = new Tuple2<Integer, String>(HttpStatus.OK.value(), "");
        try (CloseableHttpClient httpClient = getHttpClient(builder)) {
            try (CloseableHttpResponse response = httpClient.execute(builder.getHttpRequestBase())) {
                tupleRsp = getResponse(builder, response);
                if (StringUtils.isNotBlank(tupleRsp.getSecond())) {
                    return JacksonUtil.nonEmptyMapper().fromJson(tupleRsp.getSecond(), typeReference);
                }
            }
        } catch (IOException e) {
            logger.error("*****HttpClientHelper.invoke出现错误:" + e.getMessage(), e);
            parseException(e, builder.getHttpRequestBase());
        } finally {
            writeLog(builder.getHttpRequestBase(), begin, tupleRsp);
        }

        return null;
    }

    /**
     * invoke
     *
     * @param builder
     * @param parameterizedType 例如:ParameterizedType parameterizedType = ParameterizedTypeImpl.make(HttpRspDTO.class, new Type[]{CustomerKeyInfoRspDTO.class}, null);
     * @param <T>
     * @return
     */
    public static <T> T invoke(HttpRequestBuilder builder, ParameterizedType parameterizedType) {
        TypeReference<T> typeReference = new TypeReference<T>() {
            @Override
            public Type getType() {
                return parameterizedType;
            }
        };

        long begin = System.currentTimeMillis();
        Tuple2<Integer, String> tupleRsp = new Tuple2<Integer, String>(HttpStatus.OK.value(), "");
        try (CloseableHttpClient httpClient = getHttpClient(builder)) {
            try (CloseableHttpResponse response = httpClient.execute(builder.getHttpRequestBase())) {
                tupleRsp = getResponse(builder, response);
                if (StringUtils.isNotBlank(tupleRsp.getSecond())) {
                    return JacksonUtil.nonEmptyMapper().fromJson(tupleRsp.getSecond(), typeReference);
                }
            }
        } catch (IOException e) {
            logger.error("*****HttpClientHelper.invoke出现错误:" + e.getMessage(), e);
            parseException(e, builder.getHttpRequestBase());
        } finally {
            writeLog(builder.getHttpRequestBase(), begin, tupleRsp);
        }

        return null;
    }

    /**
     * invoke
     *
     * @param builder
     * @param rawType   ParameterizedTypeImpl.make 第一个参数
     * @param ownerType ParameterizedTypeImpl.make 第三个参数
     * @param argType   ParameterizedTypeImpl.make 第二个参数
     * @param <T>
     * @return
     */
    public static <T> T invoke(HttpRequestBuilder builder, Class rawType, Type ownerType, Type... argType) {
        ParameterizedType parameterizedType = ParameterizedTypeImpl.make(rawType, argType, ownerType);
        TypeReference<T> typeReference = new TypeReference<T>() {
            @Override
            public Type getType() {
                return parameterizedType;
            }
        };

        long begin = System.currentTimeMillis();
        Tuple2<Integer, String> tupleRsp = new Tuple2<Integer, String>(HttpStatus.OK.value(), "");
        try (CloseableHttpClient httpClient = getHttpClient(builder)) {
            try (CloseableHttpResponse response = httpClient.execute(builder.getHttpRequestBase())) {
                tupleRsp = getResponse(builder, response);
                if (StringUtils.isNotBlank(tupleRsp.getSecond())) {
                    return JacksonUtil.nonEmptyMapper().fromJson(tupleRsp.getSecond(), typeReference);
                }
            }
        } catch (IOException e) {
            logger.error("*****HttpClientHelper.invoke出现错误:" + e.getMessage(), e);
            parseException(e, builder.getHttpRequestBase());
        } finally {
            writeLog(builder.getHttpRequestBase(), begin, tupleRsp);
        }

        return null;
    }

    private static Tuple2<Integer, String> getResponse(HttpRequestBuilder builder, CloseableHttpResponse response) {
        int status = HttpStatus.INTERNAL_SERVER_ERROR.value();
        if (response != null && response.getStatusLine() != null) {
            status = response.getStatusLine().getStatusCode();
            //if (HttpStatus.OK.value() == status) {
            //TODO 有些接口返回204,206，这里只要返回2XX都去获取一遍返回结果
            if (status >= HttpStatus.OK.value() && status < HttpStatus.MULTIPLE_CHOICES.value()) {
                try {
                    return new Tuple2<Integer, String>(status, EntityUtils.toString(response.getEntity()));
                } catch (IOException e) {
                    logger.error("*****HttpClientHelper.getResponse出现错误:" + e.getMessage(), e);
                    parseException(e, builder.getHttpRequestBase());
                }
            }
        }

        return new Tuple2<Integer, String>(status, null);
    }

    private static void writeLog(HttpRequestBase httpRequestBase, Long begin, Tuple2<Integer, String> tupleRsp) {
        long duration = System.currentTimeMillis() - begin;
        String rspContent = ("" + tupleRsp.getSecond()).replace("\n", "");
        if (httpRequestBase instanceof HttpEntityEnclosingRequestBase) {
            HttpEntityEnclosingRequestBase temp = (HttpEntityEnclosingRequestBase) httpRequestBase;
            try {
                String requestBody = "";
                if (temp.getEntity() != null && temp.getEntity().getContent() != null) {
                    requestBody = StreamUtils.copyToString(temp.getEntity().getContent(), Charset.forName("UTF-8"));
                }
                logger.info("*****method:{},url:{},time:{},status:{},request:{},header:{}", httpRequestBase.getMethod(), httpRequestBase.getURI().toString(), duration / 1000.0, tupleRsp.getFirst(), requestBody, getHeader(httpRequestBase));
                logger.info("*****method:{},url:{},time:{},status:{},request:{},response:{}", httpRequestBase.getMethod(), httpRequestBase.getURI().toString(), duration / 1000.0, tupleRsp.getFirst(), requestBody, rspContent);
                if (duration >= THRESHOLD_THREE_SECOND_FOR_MILLISECOND) {
                    logger.warn("*****HttpClientHelper接口太慢method:{},url:{},time:{},status:{},request:{},header:{}", httpRequestBase.getMethod(), httpRequestBase.getURI().toString(), duration / 1000.0, tupleRsp.getFirst(), requestBody, getHeader(httpRequestBase));
                    writePoint(httpRequestBase.getURI().toString(), duration, httpRequestBase.getMethod());
                }
            } catch (IOException e) {
                logger.error("*****HttpClientHelper.writeLog出现错误,错误信息: {}, {}", e.getMessage(), e);
            }
        } else {
            logger.info("*****method:{},url:{},time:{},status:{},header:{}", httpRequestBase.getMethod(), httpRequestBase.getURI().toString(), duration / 1000.0, tupleRsp.getFirst(), getHeader(httpRequestBase));
            logger.info("*****method:{},url:{},time:{},status:{},response:{}", httpRequestBase.getMethod(), httpRequestBase.getURI().toString(), duration / 1000.0, tupleRsp.getFirst(), rspContent);
            if (duration >= THRESHOLD_THREE_SECOND_FOR_MILLISECOND) {
                logger.warn("*****HttpClientHelper接口太慢method:{},url:{},time:{},status:{},header:{}", httpRequestBase.getMethod(), httpRequestBase.getURI().toString(), duration / 1000.0, tupleRsp.getFirst(), getHeader(httpRequestBase));
                writePoint(httpRequestBase.getURI().toString(), duration, httpRequestBase.getMethod());
            }
        }
    }

    private static String getHeader(HttpRequestBase httpRequestBase) {
        Header[] headers = httpRequestBase.getAllHeaders();
        if (ArrayUtils.isNotEmpty(headers)) {
            StringBuilder builder = new StringBuilder("[");
            for (Header header : headers) {
                builder.append(header.getName()).append("=").append(header.getValue()).append(",");
            }
            builder.append("]");

            return builder.toString();
        }

        return "";
    }

    private static void writePoint(String url, long millis, String method) {
        if (StringUtils.isNotBlank(url) && StringUtils.isNotBlank(method) && millis > 0L) {
            logger.error("*****HttpClientHelper.writePoint:url->{},method->{},millis->{}", url, method, millis);
        }
    }

    private static void parseException(Exception e, HttpRequestBase httpRequestBase) {
        if (e != null && httpRequestBase != null) {
            if (e instanceof SocketTimeoutException) {
                writePoint(httpRequestBase.getURI().toString(), SOCKET_TIMEOUT * SOCKET_TIMEOUT, httpRequestBase.getMethod());
            }
        }
    }

    private static void parseException(Exception e, String url, String method) {
        if (e != null && StringUtils.isNotBlank(url) && StringUtils.isNotBlank(method)) {
            writePoint(url, SOCKET_TIMEOUT * SOCKET_TIMEOUT, method);
        }
    }
}