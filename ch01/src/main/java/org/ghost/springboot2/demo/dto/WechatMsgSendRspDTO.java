package org.ghost.springboot2.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author 01
 */
public class WechatMsgSendRspDTO extends WechatErrorRspDTO implements Serializable {
    private static final long serialVersionUID = 5138458163885094582L;

    private Long msgId;

    public Long getMsgId() {
        return msgId;
    }

    @JsonProperty(value = "msgid")
    public void setMsgId(Long msgId) {
        this.msgId = msgId;
    }


    @Override
    public String toString() {
        return "WechatMsgSendRspDTO{" +
                "msgId=" + msgId +
                ", errCode=" + errCode +
                ", errMsg='" + errMsg + '\'' +
                '}';
    }
}
