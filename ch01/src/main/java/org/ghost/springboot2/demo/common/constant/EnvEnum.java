package org.ghost.springboot2.demo.common.constant;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum EnvEnum {
    NONE(0, Collections.emptyList(), "无"),
    DEV(1, Arrays.asList("dev", "develop"), "开发"),
    STAGE(2, Collections.singletonList("stage"), "开发"),
    PRE(3, Arrays.asList("pre", "prodpre"), "开发"),
    PROD(4, Arrays.asList("prod", "prd"), "开发");


    EnvEnum(int code, List<String> nameList, String desc) {
        this.code = code;
        this.nameList = nameList;
        this.desc = desc;
    }

    private int code;
    private List<String> nameList;
    private String desc;

    public int getCode() {
        return code;
    }

    public List<String> getNameList() {
        return nameList;
    }

    public String getDesc() {
        return desc;
    }

    public static EnvEnum getItemByName(String name) {
        if (name != null) {
            for (EnvEnum item : EnvEnum.values()) {
                if (item.nameList.contains(name)) {
                    return item;
                }
            }
        }
        return EnvEnum.NONE;
    }
}
