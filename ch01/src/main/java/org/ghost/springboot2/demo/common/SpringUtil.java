package org.ghost.springboot2.demo.common;

import org.apache.commons.lang3.StringUtils;
import org.ghost.springboot2.demo.common.constant.EnvEnum;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class SpringUtil implements ApplicationContextAware {
    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (SpringUtil.applicationContext == null) {
            SpringUtil.applicationContext = applicationContext;
        }
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static Object getBean(String name) {
        if (getApplicationContext() != null) {
            return getApplicationContext().getBean(name);
        }
        return null;
    }

    public static <T> T getBean(Class<T> clazz) {
        if (getApplicationContext() != null) {
            return getApplicationContext().getBean(clazz);
        }
        return null;
    }

    public static <T> T getBean(String name, Class<T> clazz) {
        if (getApplicationContext() != null) {
            return getApplicationContext().getBean(name, clazz);
        }
        return null;
    }

    public static Environment getEnvironment() {
        if (getApplicationContext() != null) {
            return getApplicationContext().getEnvironment();
        }
        return null;
    }

    public static EnvEnum getEnvEnum() {
        Environment environment = getEnvironment();
        if (environment != null && StringUtils.isNotBlank(environment.getProperty("spring.profiles.active"))) {
            return EnvEnum.getItemByName(environment.getProperty("spring.profiles.active").toLowerCase());
        }
        return EnvEnum.NONE;
    }
}
