package org.ghost.springboot2.demo.config.feign;

import feign.Feign;
import feign.Logger;
import feign.Retryer;
import feign.codec.Decoder;
import feign.codec.Encoder;
import org.ghost.springboot2.demo.util.JacksonUtil;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HelloServiceApiConfig extends FeignClientsConfiguration {
    @Override
    public Feign.Builder feignBuilder(Retryer retryer) {
        Feign.Builder builder = super.feignBuilder(retryer);
        builder.requestInterceptor(new CommonInterceptor());
        return builder;
    }

    @Override
    public Decoder feignDecoder() {
        return new JacksonDecoder(JacksonUtil.useDefaultMapper().getMapper());
    }

    @Override
    public Encoder feignEncoder() {
        return new JacksonEncoder(JacksonUtil.nonEmptyMapper().getMapper());
    }

    @Bean
    Logger.Level feignLogLevel() {
        return Logger.Level.FULL;
    }
}