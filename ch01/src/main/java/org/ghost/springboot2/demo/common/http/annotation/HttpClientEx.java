package org.ghost.springboot2.demo.common.http.annotation;

import org.ghost.springboot2.demo.common.http.HttpRequestBuilder;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;

@Target(TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface HttpClientEx {
    @AliasFor("hostName")
    String value() default "";

    /**
     * 主机名http://www.baidu.com
     *
     * @return
     */
    @AliasFor("value")
    String hostName() default "";

    /**
     * http请求数据组装
     *
     * @return
     */
    Class<HttpRequestBuilder> config() default HttpRequestBuilder.class;
}
