package org.ghost.springboot2.demo.enumeration;

/**
 * @author 01
 */
public enum WechatGrantTypeEnum {
    /**
     * authorization_code
     */
    AUTHORIZATION_CODE("authorization_code", "通过code换取网页授权access_token"),
    REFRESH_TOKEN("refresh_token", "通过refresh_token刷新access_token"),
    CLIENT_CREDENTIAL("client_credential", "获取普通接口调用需要的access_token");

    WechatGrantTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
