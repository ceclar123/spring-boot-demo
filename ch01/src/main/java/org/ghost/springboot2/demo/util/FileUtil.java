package org.ghost.springboot2.demo.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class FileUtil {
    private final Logger logger = LoggerFactory.getLogger(FileUtil.class);

    private FileUtil() {
    }

    public static String getSuffixName(String fileName) {
        if (StringUtils.isNotBlank(fileName)) {
            int index = fileName.lastIndexOf('.');
            if (index < fileName.length() - 1) {
                return fileName.substring(index + 1);
            }
            return "";
        }

        return "";
    }
}
