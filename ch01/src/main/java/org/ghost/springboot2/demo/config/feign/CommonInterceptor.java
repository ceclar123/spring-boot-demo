package org.ghost.springboot2.demo.config.feign;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

public class CommonInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate template) {
        Map<String, Collection<String>> headerMap = template.headers();
        if (MapUtils.isNotEmpty(headerMap) && CollectionUtils.isNotEmpty(headerMap.get(HttpHeaders.CONTENT_TYPE))) {
            //
        } else {
            template.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
        }
        template.header("uuid", UUID.randomUUID().toString());
    }
}
