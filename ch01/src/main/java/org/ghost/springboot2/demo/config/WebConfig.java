package org.ghost.springboot2.demo.config;

import org.ghost.springboot2.demo.common.filter.RequestCrossFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {

    }

    @ConditionalOnProperty(name = "cross.domain")
    @Bean
    public FilterRegistrationBean getRequestCrossFilter() {
        FilterRegistrationBean<RequestCrossFilter> registration = new FilterRegistrationBean<RequestCrossFilter>();
        registration.setFilter(new RequestCrossFilter());
        registration.addUrlPatterns("/*");
        registration.setName("requestCrossFilter");

        return registration;
    }

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.defaultContentType(MediaType.APPLICATION_JSON).favorPathExtension(true);
    }
}
