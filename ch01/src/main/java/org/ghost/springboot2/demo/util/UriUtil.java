package org.ghost.springboot2.demo.util;


import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * java.net.URLDecoder和java.net.URLEncoder
 */
public class UriUtil {
    private static final Logger logger = LoggerFactory.getLogger(UriUtil.class);

    public static final String ALLOWED_CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.!~*'()";

    /**
     * Description:
     *
     * @param input
     * @return
     * @see
     */
    public static String encodeURIComponent(String input) {
        if (null == input || "".equals(input.trim())) {
            return input;
        }

        int l = input.length();
        StringBuilder o = new StringBuilder(l * 3);

        for (int i = 0; i < l; i++) {
            String e = input.substring(i, i + 1);
            if (!ALLOWED_CHARS.contains(e)) {
                byte[] b = e.getBytes(StandardCharsets.UTF_8);
                o.append(getHex(b));
                continue;
            }
            o.append(e);
        }
        return o.toString();
    }

    public static String decodeURIComponent(String encodedURI) {
        char actualChar;

        StringBuffer buffer = new StringBuffer();

        int bytePattern, sumb = 0;

        for (int i = 0, more = -1; i < encodedURI.length(); i++) {
            actualChar = encodedURI.charAt(i);

            switch (actualChar) {
                case '%': {
                    actualChar = encodedURI.charAt(++i);
                    int hb = (Character.isDigit(actualChar) ? actualChar - '0' : 10 + Character.toLowerCase(actualChar) - 'a') & 0xF;
                    actualChar = encodedURI.charAt(++i);
                    int lb = (Character.isDigit(actualChar) ? actualChar - '0' : 10 + Character.toLowerCase(actualChar) - 'a') & 0xF;
                    bytePattern = (hb << 4) | lb;
                    break;
                }
                case '+': {
                    bytePattern = ' ';
                    break;
                }
                default: {
                    bytePattern = actualChar;
                }
            }

            if ((bytePattern & 0xc0) == 0x80) { // 10xxxxxx
                sumb = (sumb << 6) | (bytePattern & 0x3f);
                if (--more == 0) {
                    buffer.append((char) sumb);
                }
            } else if ((bytePattern & 0x80) == 0x00) { // 0xxxxxxx
                buffer.append((char) bytePattern);
            } else if ((bytePattern & 0xe0) == 0xc0) { // 110xxxxx
                sumb = bytePattern & 0x1f;
                more = 1;
            } else if ((bytePattern & 0xf0) == 0xe0) { // 1110xxxx
                sumb = bytePattern & 0x0f;
                more = 2;
            } else if ((bytePattern & 0xf8) == 0xf0) { // 11110xxx
                sumb = bytePattern & 0x07;
                more = 3;
            } else if ((bytePattern & 0xfc) == 0xf8) { // 111110xx
                sumb = bytePattern & 0x03;
                more = 4;
            } else { // 1111110x
                sumb = bytePattern & 0x01;
                more = 5;
            }
        }
        return buffer.toString();
    }


    private static String getHex(byte buf[]) {
        StringBuilder o = new StringBuilder(buf.length * 3);
        for (int i = 0; i < buf.length; i++) {
            int n = (int) buf[i] & 0xff;
            o.append("%");
            if (n < 0x10) {
                o.append("0");
            }
            o.append(Long.toString(n, 16).toUpperCase());
        }
        return o.toString();
    }

    /**
     * 对对象进行urlencode编号
     *
     * @param data         对象
     * @param keyUpperCase 是否将key首字符大写
     * @return String
     */
    public static String urlEncode(Object data, Boolean keyUpperCase) {
        if (data == null) {
            return null;
        }

        StringBuilder stringBuilder = getStringBuilder(data, keyUpperCase);
        if (stringBuilder == null) {
            return null;
        }
        String encodedUrl;
        try {
            encodedUrl = URLEncoder.encode(stringBuilder.substring(1), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            if (logger.isWarnEnabled()) {
                logger.warn("urlencode 编码失败, 错误信息: {}, {}", e.getMessage(), e);
            }
            return null;
        }
        return encodedUrl;
    }

    private static StringBuilder getStringBuilder(Object data, Boolean keyUpperCase) {
        StringBuilder stringBuilder = new StringBuilder();
        PropertyDescriptor[] propertyDescriptors = BeanUtils.getPropertyDescriptors(data.getClass());
        try {
            for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
                String name = propertyDescriptor.getName();
                name = keyUpperCase ? StringUtils.capitalize(name) : name;
                Method method = propertyDescriptor.getReadMethod();
                Object object = method.invoke(data);
                if (object instanceof List) {
                    List mess = (List) object;
                    for (Object lo : mess) {
                        String value = lo != null ? lo.toString() : "";
                        stringBuilder.append('&').append(name).append('=').append(value);
                    }
                } else {
                    String value = object != null ? object.toString() : "";
                    stringBuilder.append('&').append(name).append('=').append(value);
                }
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            if (logger.isWarnEnabled()) {
                logger.warn("urlencode 编码失败, 错误信息: {}, {}", e.getMessage(), e);
            }
            return null;
        }
        return stringBuilder;
    }

    public static String urlParaEncode(Object data) {
        return urlParaEncode(data, false);
    }

    /**
     * 对对象进行urlEncode编号
     *
     * @param data         对象
     * @param keyUpperCase 是否将key首字符大写
     * @return String
     */
    public static String urlParaEncode(Object data, Boolean keyUpperCase) {
        Map<String, String> rtnMap = getResultMap(data, keyUpperCase);
        if (MapUtils.isNotEmpty(rtnMap)) {
            StringBuilder builder = new StringBuilder();
            for (Map.Entry<String, String> item : rtnMap.entrySet()) {
                if (StringUtils.isNotBlank(item.getKey()) && StringUtils.isNotBlank(item.getValue())) {
                    String value = "";
                    try {
                        value = URLEncoder.encode(item.getValue(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        logger.warn("*****UriUtil.urlEncode编码失败, 错误信息: {}, {}", e.getMessage(), e);
                    }
                    if (builder.length() == 0) {
                        builder.append(item.getKey()).append('=').append(value);
                    } else {
                        builder.append('&').append(item.getKey()).append('=').append(value);
                    }
                }
            }

            return builder.toString();
        }

        return "";
    }

    private static Map<String, String> getResultMap(Object data, boolean keyUpperCase) {
        Map<String, String> rtnMap = new HashMap<String, String>();
        if (data != null) {
            Field[] fields = data.getClass().getDeclaredFields();
            for (Field field : fields) {
                String name = field.getName();
                if (keyUpperCase) {
                    name = StringUtils.capitalize(name);
                }

                Object object = null;
                try {
                    PropertyDescriptor pd = new PropertyDescriptor(field.getName(), data.getClass());
                    Method method = pd.getReadMethod();
                    if (method != null) {
                        object = method.invoke(data);
                    }
                } catch (IntrospectionException e) {
                    logger.warn("*****UriUtil.getResultMap中PropertyDescriptor获取失败, 错误信息: {}, {}", e.getMessage(), e);
                } catch (IllegalAccessException e) {
                    logger.warn("*****UriUtil.getResultMap中反射get方法权限问题, 错误信息: {}, {}", e.getMessage(), e);
                } catch (InvocationTargetException e) {
                    logger.warn("*****UriUtil.getResultMap中反射getReadMethod失败, 错误信息: {}, {}", e.getMessage(), e);
                }

                if (Objects.isNull(object)) {
                    //
                } else if (object instanceof List) {
                    List<Object> list = (List<Object>) object;
                    String value = list.stream().filter(Objects::nonNull).map(Object::toString).reduce("", (a, b) -> StringUtils.isBlank(a) ? b : a + "," + b);
                    rtnMap.put(name, value);
                } else if (object.getClass().isArray()) {
                    Object[] array = (Object[]) object;
                    String value = Stream.of(array).filter(Objects::nonNull).map(Object::toString).reduce("", (a, b) -> StringUtils.isBlank(a) ? b : a + "," + b);
                    rtnMap.put(name, value);
                } else {
                    rtnMap.put(name, object.toString());
                }
            }
        }

        return rtnMap;
    }
}
