package org.ghost.springboot2.demo.controller;

import org.ghost.springboot2.demo.dto.RspDTO;
import org.ghost.springboot2.demo.util.JacksonUtil;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "demo")
public class HelloDemoController extends BaseController {
    @RequestMapping(value = "hello", method = RequestMethod.GET)
    public RspDTO helloWorld(@RequestParam(required = false) String msg) {
        try {
            Thread.sleep(3000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this.render("Hello World" + msg);
    }

    @RequestMapping(value = "hello", method = RequestMethod.POST)
    public RspDTO helloWorld(@RequestBody Map<String, Object> reqMap) {
        try {
            Thread.sleep(3000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this.render(JacksonUtil.useDefaultMapper().toJson(reqMap));
    }
}

