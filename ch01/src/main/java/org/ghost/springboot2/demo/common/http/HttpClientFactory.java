package org.ghost.springboot2.demo.common.http;

import java.lang.reflect.Proxy;

public final class HttpClientFactory {
    private HttpClientFactory() {
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBean(Class<T> clz) throws IllegalArgumentException {
        return (T) Proxy.newProxyInstance(clz.getClassLoader(), new Class[]{clz}, new HttpClientProxy<T>());
    }
}
