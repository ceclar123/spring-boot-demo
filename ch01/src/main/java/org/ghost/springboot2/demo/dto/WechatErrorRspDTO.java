package org.ghost.springboot2.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author 01
 */
public class WechatErrorRspDTO implements Serializable {
    private static final long serialVersionUID = 8741550062880973390L;

    protected Integer errCode;

    protected String errMsg;


    public Integer getErrCode() {
        return errCode;
    }

    @JsonProperty(value = "errcode")
    public void setErrCode(Integer errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    @JsonProperty(value = "errmsg")
    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    @Override
    public String toString() {
        return "WechatErrorRspDTO{" +
                "errCode=" + errCode +
                ", errMsg='" + errMsg + '\'' +
                '}';
    }
}
